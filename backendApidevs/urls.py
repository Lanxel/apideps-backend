from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from azulab.API.GoogleLoginAPI import GoogleLoginApi
import azulab.API.TermAPI as termApi
import azulab.API.AcademicUnitAPI as auApi
import azulab.API.UserAPI as userApi
import azulab.API.CourseAPI as courseApi
import azulab.API.ScheduleAPI as scheduleApi
import azulab.API.RoleAPI as roleApi
import azulab.API.LaboratoryAPI as labApi
import azulab.API.ModuleAPI as moduleApi
import azulab.API.PageAPI as pageApi
import azulab.API.SectionAPI as sectionApi
import azulab.API.QuestionAPI as questionApi
import azulab.API.CommentAPI as commentApi
import azulab.API.RichTextBoxAPI as rtbApi
import azulab.API.DocumentAPI as docApi
import azulab.API.AnswerDocumentAPI as asdocApi
import azulab.API.AnswerSheetAPI as asApi
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
schema_view = get_schema_view(openapi.Info(title='Azulab Swagger API',
                                           default_version='v1'))

role_patterns = [
    path('', roleApi.RoleListApi.as_view(),name='list'),
    path('<int:id>/', roleApi.RoleReadApi.as_view(), name='read'),
    path('update/<int:id>/', roleApi.RoleUpdateApi.as_view(), name='update'),
    path('delete/<int:id>/', roleApi.RoleDeleteApi.as_view(), name='delete'),
    path('create/', roleApi.RoleCreateApi.as_view(), name='create'),
]

term_patterns = [
    path('', termApi.TermListApi.as_view(), name='list'),
    path('create/', termApi.TermCreateApi.as_view(), name='create'),
    path('<int:id>/', termApi.TermReadApi.as_view(), name='read'),
    path('update/<int:id>/', termApi.TermUpdateApi.as_view(), name='update'),
    path('delete/<int:id>/', termApi.TermDeleteApi.as_view(), name='delete'),
]

academicUnit_patterns = [
    path('', auApi.AcademicUnitListApi.as_view(),name='list'),
    path('create/', auApi.AcademicUnitCreateApi.as_view(), name='create'),
    path('<int:id>/', auApi.AcademicUnitReadApi.as_view(), name='read'),
    path('update/<int:id>/', auApi.AcademicUnitUpdateApi.as_view(), name='update'),
    path('delete/<int:id>/', auApi.AcademicUnitDeleteApi.as_view(), name='delete'),
    path('<int:id>/courses/',auApi.AcademicUnitCourseListApi.as_view(),name='Courses List')
]

course_patterns = [
    path('', courseApi.CourseListApi.as_view(), name='list'),
    path('create/', courseApi.CourseCreateApi.as_view(), name='create'),
    path('<int:id>/', courseApi.CourseReadApi.as_view(), name='read'),
    path('update/<int:id>/', courseApi.CourseUpdateApi.as_view(), name='update'),
    path('delete/<int:id>/', courseApi.CourseDeleteApi.as_view(), name='delete'),
    path('addUsers/CSV/<int:idCourse>/<int:idTerm>/',courseApi.CourseAddStudentsFromCsvApi.as_view(),name='Add students to Schedule'),
    path('terms/<int:id>', courseApi.CourseListOfTerms.as_view(), name='List terms of a course'),
    path('delete/many/', courseApi.CourseDeleteMultipleApi.as_view(), name='Deletes many courses'),
    path('<int:id>/lastSchedule/',courseApi.CourseAdminViewApi.as_view(),name='Last Schedule')
]

schedule_patterns = [
    path('', scheduleApi.ScheduleListCoursesApi.as_view(), name='list'),
    path('create/', scheduleApi.ScheduleCreateApi.as_view(), name='create'),
    path('<int:id>/',scheduleApi.ScheduleReadApi.as_view(),name='read'),
    path('update/<int:id>/',scheduleApi.ScheduleUpdateApi.as_view(), name='update'),
    path('delete/<int:id>',scheduleApi.ScheduleDeleteApi.as_view(),name='delete'),
    path('delete/many/', scheduleApi.ScheduleDeleteMultipleApi.as_view(), name='Deletes many Schedules'),
    path('<int:id>/users/', scheduleApi.ScheduleListOfUserApi.as_view(), name='list of users'),
    path('<int:id>/details/',scheduleApi.ScheduleStudentsListApi.as_view(),name='list of users 2'),
    path('<int:id>/laboratories/',scheduleApi.ScheduleLabListApi.as_view(),name='List of Laboratories'),
    path('listDate/<int:id>/',scheduleApi.ScheduleListNextLabApi.as_view(),name = 'List date of next lab'),
    path('<int:id>/delete/users/',scheduleApi.ScheduleDeleteUsersApi.as_view(),name='Delete many users from schedule'),
    path('<int:id>/teacher/', scheduleApi.ScheduleTeacherApi.as_view(), name='Teacher from a schedule'),
    path('<int:id>/updateTeacher/', scheduleApi.ScheduleUpdateTeacherApi.as_view(), name='Add teacher to schedule'),
    path('<int:id>/addStudent/',scheduleApi.ScheduleAddStudentApi.as_view(), name='Add student to schedule'),
]

laboratory_patterns = [
    path('<int:idLab>/modules/',labApi.LaboratoryModulesList.as_view(),name='Lab Detail'),
    path('create/', labApi.LaboratoryCreateApi.as_view(), name='create'),
    path('update/<int:id>/', labApi.LaboratoryUpdateApi.as_view(), name='update'),
    path('delete/<int:id>/', labApi.LaboratoryDeleteApi.as_view(), name='delete'),
    path('display/<int:idLab>/',labApi.LaboratoryDisplayApi.as_view(),name='Lab Display'),
    path('<int:idLab>/template/',labApi.LaboratoryTemplateApi.as_view(),name='Lab Template'),
    path('<int:idSchedule>/grades/',labApi.LaboratoryGradeApi.as_view(),name='Lab Grades'),
    path('<int:idLab>/moduleGrades/',labApi.LaboratoryModuleGradeApi.as_view(),name='Lab Module Grades'),
    path('<int:idLab>/live/',labApi.LaboratoryLiveApi.as_view(),name='Lab Live'),
    path('stats/<int:idLab>/<int:idModule>/', labApi.LaboratoryStatisticsApi.as_view(), name='Lab Statistics'),
]

module_patterns = [
    path('create/',moduleApi.ModuleCreateApi.as_view(),name='create'),
    path('<int:id>/',moduleApi.ModuleReadApi.as_view(),name='read'),
    path('update/<int:id>/',moduleApi.ModuleUpdateApi.as_view(),name='update'),
    path('delete/<int:id>/',moduleApi.ModuleDeleteApi.as_view(),name='delete'),
    path('<int:idModule>/pages/', moduleApi.ModulePagesListApi.as_view(), name='Module Detail'),
    path('updateDisplay/',moduleApi.ModuleUpdateDisplayApi.as_view(),name='Module update display')
]

page_patterns = [
    path('create/',pageApi.PageCreateApi.as_view(),name='create'),
    path('<int:idPage>/',pageApi.PageReadApi.as_view(),name='read'),
    path('update/<int:idPage>/',pageApi.PageUpdateApi.as_view(),name='update'),
    path('delete/<int:idPage>/',pageApi.PageDeleteApi.as_view(),name='delete'),
    path('<int:idPage>/sections/', pageApi.PageSectionsListApi.as_view(), name='Page Detail'),
]

section_patterns = [
    path('create/',sectionApi.SectionCreateApi.as_view(),name='create'),
    path('<int:idSection>/',sectionApi.SectionReadApi.as_view(),name='read'),
    path('update/<int:idSection>/',sectionApi.SectionUpdateApi.as_view(),name='update'),
    path('delete/<int:idSection>/',sectionApi.SectionDeleteApi.as_view(),name='delete'),
    #path('<int:idSection>/elements/',sectionApi.SectionElementsList.as_view(),name='Section Detail'),
]

question_patterns = [
    path('<int:idSection>', questionApi.QuestionListSectionApi.as_view(), name='List all question'),
    path('createCheckbox/',questionApi.QuestionCreateCheckboxApi.as_view(),name='createCheckbox'),
    path('createText/',questionApi.QuestionCreateTextApi.as_view(),name='createText'),        
    path('updateText/<int:id>/',questionApi.QuestionUpdateTextApi.as_view(),name='updateText'),
    path('updateCheckbox/<int:id>/',questionApi.QuestionUpdateCheckboxApi.as_view(),name='updateCheckbox'),
    path('deleteCheckbox/<int:id>/',questionApi.QuestionDeleteCheckboxApi.as_view(),name='deleteCheckbox'),
    path('deleteText/<int:id>/',questionApi.QuestionDeleteTextApi.as_view(),name='deleteText'),
]

comment_patterns = [     
    path('listQuestion/',commentApi.CommentLisQuestiontApi.as_view(),name='List all comments for Questions'),
    path('create/',commentApi.CommentCreateApi.as_view(),name='create'),
    path('update/<int:id>/',commentApi.CommentUpdateApi.as_view(),name='update'),
    path('delete/<int:id>/',commentApi.CommentDeleteApi.as_view(),name='delete'),

]

richtextbox_patterns = [
    path('create/', rtbApi.RTBCreateApi.as_view(), name='create'),
    path('<int:idRTB>/', rtbApi.RTBReadApi.as_view(), name='read'),
    path('update/<int:idRTB>/',rtbApi.RTBUpdateApi.as_view(),name='update'),
    path('delete/<int:idRTB>/',rtbApi.RTBDeleteApi.as_view(),name='delete'),
]

document_patterns = [
    path('upload/', docApi.DocumentUploadApi.as_view(), name='upload File'),
    path('download/<int:idDoc>/', docApi.DocumentDownloadApi.as_view(), name='download File'),
    path('replace/<int:idDoc>/', docApi.DocumentReplaceFileApi.as_view(), name='replace File'),
    path('delete/<int:idDoc>/',docApi.DocumentDeleteApi.as_view(),name='delete File')
]

answerSheet_patterns = [
    path('create/', asApi.AnswerSheetCreateUpdateApi.as_view(), name='create AnswerSheet'),
    path('<int:idLab>/<int:idUser>/', asApi.AnswerSheetReadApi.as_view(), name='Get AnswerSheet'),
    path('module/<int:idAnswerSheet>/<int:idModule>/',asApi.AnswerSheetGetModuleApi.as_view(),name='Get AnswerSheet details'),
    path('grading/<int:id>/', asApi.AnswerSheetGradingApi.as_view(), name='Calificar'),
    path('grades/<int:idLab>/',asApi.AnswerSheetGradesApi.as_view(),name='Notas del lab'),
    path('gradeCB/<int:idAnswerSheet>/',asApi.AnswerSheetGradeCBApi.as_view(),name='Calificar checkbox')
]

answerDocument_patterns = [
    path('upload/', asdocApi.AnswerDocumentUploadApi.as_view(), name='upload File'),
    path('download/<int:idAsDoc>/', asdocApi.AnswerDocumentDownloadApi.as_view(), name='download File'),
    path('delete/<int:idAsDoc>/',asdocApi.AnswerDocumentDeleteApi.as_view(),name='delete File')
]

users_patterns = [
    path('', userApi.UserListApi.as_view(), name='List all users'),
    path('verify/',userApi.UserVerifyApi.as_view(),name='Verify user'),
    path('<int:id>/', userApi.UserReadApi.as_view(), name='Get user details'),
    path('roles/<int:id>/', userApi.UserReadRolesApi.as_view(), name='Get user roles'),
    path('update/<int:id>/', userApi.UserUpdateApi.as_view(), name='Updates a User'),
    path('delete/<int:id>/', userApi.UserDeleteApi.as_view(), name='Deletes a User'),
    path('delete/many/', userApi.UserDeleteMultipleApi.as_view(), name='Deletes many users'),
    path('landing/', userApi.UserLanding.as_view(), name='Display Roles'),
    path('create/', userApi.UserCreateApi.as_view(), name='Creates one user'),
    path('create/one/', userApi.UserCreateOneApi.as_view(), name='Creates one user'),
    path('create/csv/', userApi.UserCreateFromCSV.as_view(), name='Creates users from csv'),
    path('schedules/', termApi.TermCoursesListOfStudent.as_view(), name='Schedules from a user'),
    path('<int:idRole>/schedules/', termApi.TermCurrentCoursesListOfStudent.as_view(), name='Current Schedules from User'),
]

#####################################################################3
urlpatterns = [
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('accounts/', include('allauth.urls')),
    path('rest-auth/google/', GoogleLoginApi.as_view(), name='google_login'),

    ######   MY SITES  #########

    path('terms/', include((term_patterns, 'terms'))),
    path('roles/', include((role_patterns, 'roles'))),
    path('academicUnits/', include((academicUnit_patterns, 'academicUnits'))),
    path('users/', include((users_patterns, 'users'))),
    path('courses/', include((course_patterns, 'courses'))),
    path('schedules/', include((schedule_patterns, 'schedules'))),
    path('laboratories/',include((laboratory_patterns, 'laboratories'))),
    path('modules/',include((module_patterns, 'modules'))),
    path('pages/',include((page_patterns, 'pages'))),
    path('sections/',include((section_patterns, 'sections'))),
    path('questions/', include((question_patterns, 'questions'))),
    path('comments/', include((comment_patterns, 'comments'))),
    path('richtextboxs/', include((richtextbox_patterns, 'rich text boxs'))),
    path('documents/', include((document_patterns, 'documents'))),
    path('answerDocuments/', include((answerDocument_patterns, 'answerDocuments'))),
    path('answerSheets/', include((answerSheet_patterns, 'answerSheets'))),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]

if settings.DEBUG:
  urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
