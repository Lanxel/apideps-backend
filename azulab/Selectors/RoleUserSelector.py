from ..models.RoleUser import RoleUser
from azulab.Services.UserScheduleServices import userSchedule_students,userSchedule_teachers
from .RoleSelector import role_read_by_name
from .UserSelector import user_check_by_email
from .UserSelector import user_get_profile
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist


def roleuser_list(*,idSchedule,search=None,type):
    roleUsers = []
    if type == 'Alumno':
        us = userSchedule_students(idSchedule=idSchedule)
    else:
        us = userSchedule_teachers(idSchedule=idSchedule)
    #roleUsers = RoleUser.objects.filter(schedules__in=[idSchedule],deleted=False).order_by("user__code")
    if search is not None:
        us = us.filter(Q(roleUser__user__user__first_name__unaccent__icontains=search) |
                       Q(roleUser__user__user__last_name__unaccent__icontains=search) |
                       Q(roleUser__user__user__email__unaccent__icontains=search) |
                       Q(roleUser__user__code=search)).order_by("roleUser__user__code")

    for x in us:
        roleUsers.append(x.roleUser)
    return roleUsers



def roleuser_read(*,user,type):
    role = role_read_by_name(name=type)
    userProfile = user_get_profile(user=user)
    try:
        roleUser = RoleUser.objects.get(user=userProfile, role=role,deleted=False)
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist
    else:
        return roleUser


def roleUser_verifyIfExists(*,email,code=None):
    user = user_check_by_email(email=email,code=code)
    return user if user is not None else None


def roleUser_get_academicUnit(*,user,type):
    roleUser = RoleUser.objects.filter(user=user, role__name=type).first()
    au = roleUser.academicUnits.first()
    return au


def roleUser_get_current_roles(*,userId):
    roleUsers = RoleUser.objects.filter(user_id=userId,deleted=False)
    roles=[]
    for item in roleUsers:
        roles.append(item.role)
    return roles

def roleUser_verify_by_code(*,code,idRole):
    try:
        roleUser = RoleUser.objects.get(user__code=code,role_id=idRole)
    except ObjectDoesNotExist:
        return None
    else:
        return roleUser.user


