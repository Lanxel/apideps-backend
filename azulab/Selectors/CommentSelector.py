from ..models.Comment import Comment
from ..models.Section import Section
from ..models.Question import Question
from django.core.exceptions import ObjectDoesNotExist

def comment_list_question(*,idList,idAnswerSheet):
    comments = []
    for id in idList:
        comment = Comment.objects.filter(question_id=id,
                                         answerSheet_id=idAnswerSheet,
                                         deleted=False).order_by('date').first()
        if comment is not None:
            comments.append(comment)
    return comments


def comment_read(*, id):
    try:
       obj = Comment.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este comentario no existe.'}
    else:
        return obj

