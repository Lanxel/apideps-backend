from azulab.models.RichTextBox import RichTextBox
from django.core.exceptions import ObjectDoesNotExist


def rtb_read(*,id):
    try:
        rtb = RichTextBox.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este Rich Text Box no existe.'}
    else:
        return rtb