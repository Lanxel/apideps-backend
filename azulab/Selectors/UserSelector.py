from ..models.User import UserProfile, User
from django.core.exceptions import ObjectDoesNotExist
from ..Selectors.RoleSelector import role_read_by_name
from ..models.RoleUser import RoleUser
from ..models.Role import Role
from django.db.models import Q


def user_list(*, search=None, type, idAu):
    users = []

    idAu = int(idAu)
    role = Role.objects.get(name=type)
    if idAu != -1:
        roleUsers = RoleUser.objects.filter(role_id=role.id, academicUnits__in=[idAu],
                                            deleted=False, user__user__is_active=True).order_by("user__code")
    else:
        roleUsers = RoleUser.objects.filter(role_id=role.id, deleted=False,
                                            user__user__is_active=True).order_by("user__code")

    if search is not None:
        roleUsers = roleUsers.filter(Q(user__user__first_name__unaccent__icontains=search) |
                                     Q(user__user__last_name__unaccent__icontains=search) |
                                     Q(user__user__email__unaccent__icontains=search) |
                                     Q(user__code__icontains=search)).order_by("user__code")

    for item in roleUsers:
        users.append(item.user)
    return users


def user_read(*, id):
    try:
        userProfile = UserProfile.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este usuario no existe.'}
    else:
        return userProfile


def user_get_email(*, id):
    user = User.objects.get(pk=id)
    return user


def user_check_by_email(*, email, code=None):
    try:
        if code is None:
            userProfile = UserProfile.objects.get(user__email=email)
        else:
            userProfile = UserProfile.objects.get(user__email=email, code=code)
    except ObjectDoesNotExist:
        return None
    else:
        return userProfile


def user_get_roles(*, user):
    roles = []
    for roleUser in user.roleUsers.filter(deleted=False):
        if roleUser.role_id in [1, 2]:
            for schedule in roleUser.schedules.filter(deleted=False):
                role = {}
                role['userType'] = {'id': roleUser.role_id, 'name': roleUser.role.name}
                role['academicUnit'] = {'id': schedule.course.academicUnit.id,
                                        'name': schedule.course.academicUnit.name}
                role['course'] = {'id': schedule.course.id, 'name': schedule.course.name}
                role['schedule'] = {'id': schedule.id, 'number': schedule.number}
                role['term'] = {'id': schedule.term.id, 'term': schedule.term.term}
                roles.append(role)
        elif roleUser.role_id == 3:
            role = {}
            role['userType'] = {'id': roleUser.role_id, 'name': roleUser.role.name}
            for au in roleUser.academicUnits.filter(deleted=False):
                role['academicUnit'] = {'id': au.id, 'name': au.name}
                role['course'] = {'id': None, 'name': None}
                role['schedule'] = {'id': None, 'number': None}
                role['term'] = {'id': None, 'term': None}
            roles.append(role)
        else:
            role = {}
            role['userType'] = {'id': roleUser.role_id, 'name': roleUser.role.name}
            role['academicUnit'] = {'id': None, 'name': None}
            role['course'] = {'id': None, 'name': None}
            role['schedule'] = {'id': None, 'number': None}
            role['term'] = {'id': None, 'term': None}
            roles.append(role)
    return sorted(roles, key=lambda i: (i['term']['term'] is not None, i['term']['term']), reverse=True)


def user_list_by_role(*, type):
    role = role_read_by_name(name=type)
    roleUsers = RoleUser.objects.filter(role=role, deleted=False)
    users = []
    for item in roleUsers:
        users.append(item.user)
    return users


def user_get_profile(*, user):
    return UserProfile.objects.get(pk=user.id)


def user_verify(*, email=None, code=None):
    try:
        if email:
            userProfile = UserProfile.objects.get(user__email=email)
        else:
            if code is not None and code.isnumeric() and len(code) == 8:
                userProfile = UserProfile.objects.get(code=code)
            else:
                return 'Codígo debe ser numérico y de 8 dígitos'
    except ObjectDoesNotExist:
        return False
    else:
        return userProfile
