from ..models.Question import Question
from ..models.Text import Text
from ..models.Checkbox import Checkbox
from django.core.exceptions import ObjectDoesNotExist


def question_list_section(*,section):
    lista = []
    try:
        objC = Checkbox.objects.filter(deleted=False,section_id=section)
        objT = Text.objects.filter(deleted=False,section_id=section)
    except ObjectDoesNotExist:
        return {'error': 'Esta seccion no existe.'}
    else:
        for item in objC:
            lista.append(item)
        for item in objT:
            lista.append(item)
        orderedList = sorted(lista, key=lambda x: x.position)
        return orderedList
    

def question_read(*, id):
    try:
        obj = Question.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta no existe.'}
    else:
        return obj

def question_read_by_name(*,statement):
    try:
        obj = Question.objects.get(statement=statement)
    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta no existe.'}
    else:
        return obj

def question_get_module(*,id):
    try:
        question = Question.objects.get(pk=id)
        sectionId = question.section_id
        pageId = question.section.page_id
        moduleId = question.section.page.module_id
    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta no existe.'}
    else:
        return moduleId, pageId, sectionId
