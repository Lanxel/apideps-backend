from ..models.UserSchedule import UserSchedule
from ..Selectors.UserSelector import user_read
from ..Selectors.RoleSelector import role_read
from ..Selectors.RoleUserSelector import roleuser_read
from ..models.Schedule import Schedule


def userSchedule_list_courses_order_by_term(*,user):
    roleUser = roleuser_read(user=user, type='Estudiante')
    courses = UserSchedule.objects.filter(roleUser=roleUser,deleted=False)
    #ordenar
    ordered = courses.order_by('-schedule__term__term')
    return ordered

def userSchedule_read_by_roleuser(*,roleUser):
    userSchedule = UserSchedule.objects.filter(roleUser=roleUser,deleted=False)
    return userSchedule