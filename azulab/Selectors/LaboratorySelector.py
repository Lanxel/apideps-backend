from ..models import Laboratory
from ..models import Section
from ..models import Page
from ..models import Module
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist


def laboratory_list():
    laboratories = Laboratory.objects.all().filter(deleted=False)
    return laboratories


def laboratory_read(*, id):
    try:
        lab = Laboratory.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio no existe.'}
    else:
        return lab


def get_laboratory_question(*,question):
    sec = Section.objects.get(pk=question.section.id)
    pag = Page.objects.get(pk=sec.page.id)
    mod = Module.objects.get(pk=pag.module.id)
    lab = Laboratory.objects.get(pk=mod.laboratory.id)
    return lab


def laboratory_current_module(*,id):
    now = timezone.now()
    lab = Laboratory.objects.get(pk=id)
    modules = lab.modules.filter(deleted=False).order_by('initDate')
    if modules.exists():
        module = modules.filter(initDate__lte=now,endDate__gte=now)
        if module.exists():
            return module.first()
        else:
            module = modules.filter(initDate__gte=now)
            return module.first()
    else:
        return None