from ..models.Page import Page
from django.core.exceptions import ObjectDoesNotExist


def page_read(*,id):
    try:
        page = Page.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Esta página no existe.'}
    else:
        return page