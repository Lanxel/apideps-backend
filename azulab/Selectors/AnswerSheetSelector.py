from azulab.models.UserSchedule import UserSchedule
from azulab.models.Laboratory import Laboratory
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.models.AnswerSheet import AnswerSheet
from django.db.models import Q
import statistics


def answerSheet_read(*,id):
    try:
        answerSheet = AnswerSheet.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error':'Esta hoja de respuesta no existe.'}
    else:
        return answerSheet


def answerSheet_get(*,labId,userId):
    try:
        lab = Laboratory.objects.get(pk=labId)
        scheduleId = lab.schedule.id
        userSchedule = UserSchedule.objects.get(schedule_id=scheduleId, roleUser__user_id=userId)
        answerSheet = AnswerSheet.objects.get(laboratory_id=labId, userSchedule_id=userSchedule.id)
    except ValidationError as e:
        return e.message_dict
    except ObjectDoesNotExist:
        return {'error':'No existe hoja de respuesta para este usuario en este laboratorio.'}
    else:
        return answerSheet


def answerSheet_get_module(*,id,idModule):
    try:
        answerSheet = AnswerSheet.objects.get(pk=id)
        modules = answerSheet.answers['answers']
        module = None
        for item in modules:
            if ("id" in item) and (item['id'] == idModule):
                module = item
                break
    except ObjectDoesNotExist:
        return None
    else:
        return module


def answerSheet_get_from_lab(*,labId):
    return AnswerSheet.objects.filter(laboratory_id=labId,deleted=False).order_by('userSchedule__roleUser__user__code')


def answerSheet_total_grade(*, idLab, user):
    try:
        answerSheet = AnswerSheet.objects.get(laboratory_id=idLab, userSchedule__roleUser__user__user_id=user.id)
        totalGrade = 0
        for maxim in answerSheet.maxModuleGrade['maxModuleGrade']:
            totalGrade += maxim['max']
        finalGrade = answerSheet.grade
        if finalGrade is None:
            finalGrade = 0
        else:
            finalGrade = round(finalGrade*20/totalGrade)
    except ObjectDoesNotExist:
        return {'error': 'Esta hoja de respuesta no existe.'}
    else:
        return finalGrade


def answerSheet_delete_module(*,idLab,idModule):
    answerSheets = AnswerSheet.objects.filter(laboratory_id=idLab,deleted=False)
    for answerSheet in answerSheets:
        modules = answerSheet.answers['answers']
        cant = 0
        delete = []
        for module in modules:
            if module['id'] == idModule:
                delete.append(cant)
                break
            cant += 1
        for index in delete:
            modules.pop(index)
        answerSheet.answers['answers'] = modules
        answerSheet.save()


def answerSheet_module_maxGrade(*,module):
    maxGrade = 0
    for page in module['pages']:
        for section in page['sections']:
            for element in section['elements']:
                if 'maxScore' in element:
                    maxGrade += element['maxScore']
    return maxGrade


def answerSheet_get_percent(*, data, idModule):
    total = 0
    actual = 0
    for item in data:
        if ("id" in item) and (item['id'] == idModule):
            for page in item['pages']:
                for section in page['sections']:
                    for element in section['elements']:
                        if element['elementType'] in ['text-question','checkbox-question']:
                            total += 1
                            if 'answer' in element:
                                if element['answer'] is None:
                                    element['answer'] = ""
                                if element['answer'] == "<p><br></p>":
                                    element['answer'] = ""
                                if element['answer'] != "":
                                    actual += 1
                            if 'options' in element:
                                for option in element['options']['options']:
                                    if option['selected']:
                                        actual += 1
                                        break

    if total == 0:
        return 0
    return round(actual/total,2)*100


def answerSheet_lab_progress(*,idLab,idModule,search=None):
    response = []
    answers = AnswerSheet.objects.filter(laboratory_id=idLab, deleted=False)
    if search is not None:
        answers = answers.filter(Q(userSchedule__roleUser__user__user__first_name__unaccent__icontains=search) |
                                 Q(userSchedule__roleUser__user__user__last_name__unaccent__icontains=search) |
                                 Q(userSchedule__roleUser__user__user__email__unaccent__icontains=search) |
                                 Q(userSchedule__roleUser__user__code__icontains=search)).order_by('userSchedule__roleUser__user__code')
    for answer in answers:
        user = answer.userSchedule.roleUser.user
        name = user.user.first_name + ' ' + user.user.last_name
        data = {'idAnswerSheet':answer.id,'idModule':idModule,'idUser':user.id,'code':user.code,'name':name}
        percent = answerSheet_get_percent(data=answer.answers['answers'], idModule=idModule)
        data['percent'] = str(percent) + '%'
        response.append(data)
    return response


def answerSheet_verify_documents(*,idQuestion,idAnswerSheet, passed=False,delete=False):
    answerSheet = AnswerSheet.objects.get(pk=idAnswerSheet)
    modules = answerSheet.answers['answers']
    for module in modules:
        for page in module['pages']:
            for section in page['sections']:
                for element in section['elements']:
                    if element['elementType'] == 'text-question':
                        if element['id'] == idQuestion:
                            if not delete:
                                if int(element['cantDocuments']) == 0:
                                    return False
                                else:
                                    if passed:
                                        element['cantDocuments'] = int(element['cantDocuments']) - 1
                                        answerSheet.save()
                                    return True
                            else:
                                element['cantDocuments'] = int(element['cantDocuments']) + 1
                                answerSheet.save()
                                return True
    return False


def answerSheet_get_all_module_grades(*,idLab,idModule):
    answerSheets = AnswerSheet.objects.filter(laboratory_id=idLab,deleted=False)
    data = {}
    maxModuleGrade = 0
    grades = []
    maxModules = answerSheets[0].maxModuleGrade['maxModuleGrade']
    for module in maxModules:
        if module['id'] == idModule:
            maxModuleGrade = int(module['max'])
            break

    for answerSheet in answerSheets:
        for moduleGrade in answerSheet.moduleGrade['moduleGrade']:
            if moduleGrade['id'] == idModule:
                if maxModuleGrade == 0:
                    grades.append(0)
                else:
                    grade = round(int(moduleGrade['grade'])*20/maxModuleGrade)
                    grades.append(grade)

    data['count'] = len(grades)
    data['approved'] = 0
    data['disapproved'] = 0
    for grade in grades:
        if grade < 10.5:
            data['disapproved'] += 1
        else:
            data['approved'] += 1
    data['max'] = max(grades)
    data['min'] = min(grades)
    avg = sum(grades)/len(grades)
    data['avg'] = round(avg,2)
    sd = statistics.stdev(grades)
    data['sd'] = round(sd,2)
    return data


def answerSheet_get_all_module_grades_by_question(*,idLab,idModule):
    answerSheets = AnswerSheet.objects.filter(laboratory_id=idLab, deleted=False)
    grades = []
    questions = []
    for answerSheet in answerSheets:
        for module in answerSheet.answers['answers']:
            if module['id'] == idModule:
                for page in module['pages']:
                    for section in page['sections']:
                        for element in section['elements']:
                            if 'maxScore' in element:
                                gradesQ = {'id': element['id'], 'number': element['number']}
                                scores = {'id':element['id'],'scoreObtained':element['scoreObtained']}
                                if gradesQ not in grades:
                                    grades.append(gradesQ)
                                questions.append(scores)

    for grade in grades:
        scoreObtained = []
        for question in questions:
            if question['id'] == grade['id']:
                if question['scoreObtained'] is None:
                    scoreObtained.append(0)
                else:
                    scoreObtained.append(question['scoreObtained'])
        grade['max'] = max(scoreObtained)
        grade['min'] = min(scoreObtained)
        avg = sum(scoreObtained) / len(scoreObtained)
        grade['avg'] = round(avg, 2)
    return sorted(grades, key=lambda i: i['number'])
