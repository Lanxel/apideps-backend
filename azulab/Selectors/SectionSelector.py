from ..models.Section import Section
from django.core.exceptions import ObjectDoesNotExist


def section_read(*,id):
    try:
        section = Section.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Esta seccion no existe.'}
    else:
        return section