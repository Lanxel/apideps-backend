from ..models.Role import Role
from django.core.exceptions import ObjectDoesNotExist


def role_verifyName(*, name):
    names = Role.objects.all().values_list('name', flat=True)
    if names is None:
        return False
    if name in names:
        return True
    return False


def role_list():
    roles = Role.objects.all().filter(deleted=False)
    return roles


def role_read(*, id):
    try:
        role = Role.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este rol no existe.'}
    else:
        return role


def role_read_by_name(*, name):
    try:
        role = Role.objects.get(name=name)
    except ObjectDoesNotExist:
        return {'error': 'Este rol no existe.'}
    else:
        return role
