from ..models.Schedule import Schedule
from ..models.Course import Course
from ..models.UserSchedule import UserSchedule
from ..models.Term import Term
from .RoleUserSelector import roleuser_read
from .UserScheduleSelector import userSchedule_read_by_roleuser
from ..Services.UserScheduleServices import userSchedule_students, userSchedule_teachers
from django.core.exceptions import ObjectDoesNotExist


def schedule_list(*, codeCourse=None, term=None):
    schedules = Schedule.objects.order_by("number")
    if codeCourse is not None:

        try:
            _course = Course.objects.get(pk=codeCourse)
        except ObjectDoesNotExist:
            return {'error': 'Este curso no existe.'}

    if term is not None:

        try:
            _term = Term.objects.get(pk=term)
        except ObjectDoesNotExist:
            return {'error': 'Este ciclo no existe'}

    if (_term is not None) and (_course is not None):
        return schedules.filter(deleted=False, course=_course, term=_term)
    if _term is not None and _course is None:
        return schedules.filter(deleted=False, term=_term)
    if _term is None and _course is not None:
        return schedules.filter(deleted=False, course=_course)
    else:
        return schedules


def schedule_read(*, id):
    try:
        schedule = Schedule.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este horario no existe.'}
    else:
        return schedule


def schedule_by_name_course(*, courseId, number, termId):
    try:
        course = Course.objects.get(pk=courseId)
        scheduleObj = Schedule.objects.filter(course_id=courseId, term_id=termId,
                                              number__icontains=number, deleted=False)
        if scheduleObj.exists():
            schedule = scheduleObj[0]
            name = schedule.number
            if not name.startswith('H-0'):
                if len(number) == 3:
                    schedule.number = 'H-0' + number
                else:
                    schedule.number = 'H-' + number
            schedule.save()
        else:
            if len(number) == 3:
                number = 'H-0' + number
            else:
                number = 'H-' + number
            schedule, created = Schedule.objects.get_or_create(course_id=courseId, term_id=termId, number=number)
            if not created:
                schedule.deleted = False
                schedule.save()

    except ObjectDoesNotExist:
        raise ObjectDoesNotExist
    else:
        return schedule


# USAN USER 

def schedule_list_by_user(*, user, term, type):
    try:
        roleUser = roleuser_read(user=user, type=type)
    except ObjectDoesNotExist:
        return {'error': 'No existe el rol del usuario.'}

    # buscar todos los horarios en los que este presente el roleUser

    lista = userSchedule_read_by_roleuser(roleUser=roleUser)
    schedules = Schedule.objects.filter(userSchedules__in=lista, term=term, deleted=False).order_by('course__name')
    return schedules


def schedule_count(*, schedule):
    cant = UserSchedule.objects.filter(main=False, deleted=False, schedule_id=schedule.id).count()
    return cant


def schedule_teacher(*, schedule):
    userSchedule = UserSchedule.objects.filter(schedule_id=schedule.id, main=True, deleted=False)
    if userSchedule.exists():
        return [x.roleUser for x in userSchedule]
    else:
        return None


def schedule_users(*, idSchedule, type):
    if type == 'Docente':
        us = userSchedule_teachers(idSchedule=idSchedule)
    else:
        us = userSchedule_students(idSchedule=idSchedule)
    roleUsers = [x.roleUser for x in us]
    return roleUsers


def schedule_terms(*, idCourse):
    schedules = Schedule.objects.filter(course_id=idCourse, deleted=False).order_by('-term__term', 'number')
    if schedules.exists():
        schedule = schedules.first()
        term = schedule.term
        return schedule, term
    else:
        return None, None
