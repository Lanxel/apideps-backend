from ..models.Module import Module
from django.core.exceptions import ValidationError, ObjectDoesNotExist


def module_read(*,id):
    try:
        module = Module.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este modulo no existe.'}
    else:
        return module

def module_answerSheet(*,id):
    try:
        from azulab.API.ModuleAPI import ModulePagesListApi
        module = Module.objects.get(pk=id)
        result = ModulePagesListApi.OutputSerializer(module)
    except ObjectDoesNotExist:
        return {'error': 'Este modulo no existe.'}
    else:
        return result.data