from azulab.models.AnswerDocument import AnswerDocument
from django.core.exceptions import ObjectDoesNotExist
import mimetypes
import os
from django.http import FileResponse
from wsgiref.util import FileWrapper

def document_download(*,id):
    try:
        doc = AnswerDocument.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error':'Este documento no existe.'}
    else:
        wrapper = FileWrapper(open(doc.document.path, 'rb'))
        content_type = mimetypes.guess_type(doc.document.path)[0]
        response = FileResponse(wrapper, content_type=content_type)
        response['Content-Length'] = os.path.getsize(doc.document.path)
        file_name = doc.document.path.split('/')[-1]
        response['Content-Disposition'] = f"attachment; filename={file_name}"
        return response