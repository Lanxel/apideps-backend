from ..models.AcademicUnit import AcademicUnit
from django.core.exceptions import ObjectDoesNotExist
from azulab.models.RoleUser import RoleUser


def academicunit_list(*,search=None):
    academicunits = AcademicUnit.objects.filter(deleted=False).order_by("name")
    if search is None:
        return academicunits
    return academicunits.filter(deleted=False, name__unaccent__icontains=search)


def academicunit_read(*, id):
    try:
        au = AcademicUnit.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Esta Unidad Académica no existe.'}
    else:
        return au

def academicunit_read_by_name(*,name):
    name = ' '.join(name.split()).title()
    try:
        academicUnit = AcademicUnit.objects.get(name__unaccent__icontains=name)
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist
    else:
        return academicUnit

