from azulab.models.RichTextBox import RichTextBox
from azulab.models.Document import Document
from django.core.exceptions import ObjectDoesNotExist
from azulab.Selectors.QuestionSelector import question_list_section


def element_list_section(*,sectionId):
    lista = []
    try:
        objR = RichTextBox.objects.all().filter(deleted=False, section_id=sectionId)
        objD = Document.objects.all().filter(deleted=False, section=sectionId)
        objQ = question_list_section(section=sectionId)
    except ObjectDoesNotExist:
        return {'error': 'Esta sección no existe.'}
    else:
        for item in objD:
            lista.append(item)
        for item in objR:
            lista.append(item)
        for item in objQ:
            lista.append(item)
        orderedList = sorted(lista, key=lambda x: x.position)
        return orderedList


