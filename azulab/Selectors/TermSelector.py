from ..models.Term import Term
from ..models.Schedule import Schedule
from azulab.Selectors.ScheduleSelector import schedule_list
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone


def term_list(*, search = None):
    terms = Term.objects.all().order_by('-term').filter(deleted=False)
    if search is None:
        return terms
    return terms.filter(deleted=False, term__icontains=search)


def term_read(*, id):
    try:
        term = Term.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error':'Este ciclo no existe.'}
    else:
        return term

#lsita de ciclos a los que pertenece un ciclo
def term_courses_terms(*,idCourse):
    terms = []
    schedules = Schedule.objects.filter(course_id=idCourse,deleted=False)
    for schedule in schedules:
        term = schedule.term
        terms.append(term)

    termsSingle = list(dict.fromkeys(terms))
    return termsSingle

def term_current():
    now = timezone.now()
    terms = Term.objects.filter(initDate__lte=now,endDate__gte=now,deleted=False)
    return terms.first()


def term_schedules(*,idCourse):
    data = []
    terms = term_list()
    for term in terms:
        result = {}
        schedules = schedule_list(codeCourse=idCourse, term=term.id)
        if schedules.exists():
            result['id'] = term.id
            result['term'] = term.term
            result['schedule'] = []
            for schedule in schedules:
                schData = {'id': schedule.id, 'number': schedule.number}
                result['schedule'].append(schData)
            data.append(result)
    return data