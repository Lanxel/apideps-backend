from ..models.Course import Course
from django.core.exceptions import ObjectDoesNotExist
from azulab.models.RoleUser import RoleUser
from azulab.Selectors.TermSelector import term_read
from azulab.Selectors.AcademicUnitSelector import academicunit_read


def course_list(*, search=None,idTerm=None,idAu=None):

    if idTerm is None and idAu is None:
        courses = Course.objects.filter(deleted=False).distinct().order_by("name")

    if idTerm is not None and idAu is None:
        term = term_read(id=idTerm)
        if isinstance(term,dict):
            return term
        courses = Course.objects.filter(deleted=False, schedules__term_id=term.id).distinct().order_by("name")


    if idTerm is None and idAu is not None:
        au = academicunit_read(id=idAu)
        if isinstance(au, dict):
            return au
        courses = au.courses.filter(deleted=False).distinct()

    if idTerm is not None and idAu is not None:
        term = term_read(id=idTerm)
        if isinstance(term, dict):
            return term
        au = academicunit_read(id=idAu)
        if isinstance(au, dict):
            return au
        courses = au.courses.filter(deleted=False,schedules__term_id=term.id).distinct()

    if search is None:
        return courses
    return courses.filter(name__unaccent__icontains=search)

def course_read(*,id):
    try:
        course = Course.objects.get(pk=id)
    except ObjectDoesNotExist:
        return {'error': 'Este curso no existe.'}
    else:
        return course