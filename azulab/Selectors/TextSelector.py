from ..models.Text import Text
from django.core.exceptions import ObjectDoesNotExist


def text_list_section(*,section):
    try:
        obj = Text.objects.filter(deleted=False,section_id=section)
    except ObjectDoesNotExist:
        return {'error': 'Este texto no existe.'}
    else:
        return obj