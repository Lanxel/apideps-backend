from ..models.Checkbox import Checkbox
from django.core.exceptions import ObjectDoesNotExist

def checkbox_list_section(*,section):
    try:
        obj = Checkbox.objects.filter(deleted=False,section_id=section)
    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta de opción multiple no existe.'}
    else:
        return obj

