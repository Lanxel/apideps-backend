from django.core.exceptions import ValidationError
from azulab.fixtures import *
from azulab.models.Laboratory import Laboratory
from azulab.models.Module import Module
import json
from datetime import datetime
def create_DumpData():

	laboratory = Laboratory.objects.all()
	cant = 8


	initDate =  datetime.fromisoformat('2021-11-04T00:05:23+04:00') 
	endDate = datetime.fromisoformat('2021-11-04T00:07:23+04:00') 

	obj1 = Module.objects.create(name='Prueba Entrada',laboratory=laboratory[1],
									initDate=initDate,endDate=endDate)									
	obj2 = Module.objects.create(name='Prueba Entrada',laboratory=laboratory[2],
									initDate=initDate,endDate=endDate)
	obj3 = Module.objects.create(name='Simulacion',laboratory=laboratory[2],
									initDate=initDate,endDate=endDate)	
	obj4 = Module.objects.create(name='Prueba Entrada',laboratory=laboratory[3],
									initDate=initDate,endDate=endDate)
	obj5 = Module.objects.create(name='Simulacion',laboratory=laboratory[3],
									initDate=initDate,endDate=endDate)
	obj6 = Module.objects.create(name='Prueba Extra',laboratory=laboratory[3],
									initDate=initDate,endDate=endDate)
	obj7 = Module.objects.create(name='Prueba Entrada',laboratory=laboratory[0],
									initDate=initDate,endDate=endDate)
	obj8 = Module.objects.create(name='Prueba Salida',laboratory=laboratory[0],
									initDate=initDate,endDate=endDate)

	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)
	list_object.append(obj5)
	list_object.append(obj6)
	list_object.append(obj7)
	list_object.append(obj8)

	if len(Module.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []