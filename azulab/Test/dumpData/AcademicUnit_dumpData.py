from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json
from azulab.models.AcademicUnit import AcademicUnit

def create_DumpData():
	
	cant = 4

	obj1 = AcademicUnit.objects.create(code='INF',name='Ingenieria Informatica')
	obj2 = AcademicUnit.objects.create(code='IND',name='Ingenieria Industrial')
	obj3 = AcademicUnit.objects.create(code='CIV',name='Ingenieria Civil')
	obj4 = AcademicUnit.objects.create(code='EEI',name='Ingenieria Electornica')


	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)

	if len(AcademicUnit.objects.all())==cant:
		return cant , list_object
	else:
		return -1, []