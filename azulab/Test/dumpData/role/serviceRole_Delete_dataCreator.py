from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json

def dataCreator():
  with open('azulab/fixtures/azulab_DB_testdata_roleModel.json') as f:
    data = json.load(f)
  N_fix = len(data)
  #(testName, fields, error, msgtError, amount, match?)  		
  TEST_DATA = []


  

  #Test validos
  for i in range(len(data)):
    auxFields = []
    auxFields.append(data[i]['pk'])
    for field in data[i]['fields'].items():			
      auxFields.append(field[1])  
    TEST_DATA.append(['create_valid',auxFields,None,None,N_fix-i,False])

  #Test Invalidos
  TEST_DATA.append(['delete_no_exist',[9999999,'',[]],ValidationError,'ID',N_fix,False])
  TEST_DATA.append(['delete_noID',[None,'',[]],ValidationError,'ID',N_fix,False])
  return TEST_DATA
