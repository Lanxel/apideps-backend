from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json

def dataCreator():
  with open('azulab/fixtures/azulab_DB_testdata_roleModel.json') as f:
    data = json.load(f)
  #(testName, fields, error, msgtError, amount, match?)  		
  TEST_DATA = []


  #Test Invalidos
  TEST_DATA.append(['create_no_name',['',[]],ValidationError,'name',0,False])
  

  #Test validos
  for i in range(len(data)):
    auxFields = []
    for field in data[i]['fields'].items():			
      auxFields.append(field[1])
    TEST_DATA.append(['create_valid',auxFields,None,None,i+1,True])

  return TEST_DATA
