from django.core.exceptions import ValidationError

def url_dataCreator():

	DATA_TEST = []
	DATA_URL = ['/terms/']

	for _url in DATA_URL:
		auxList = []
		auxList.append("URL_Valido")
		auxList.append(_url)
		auxList.append(200)
		auxList.append(f"Error en el el url {_url}")
		DATA_TEST.append(auxList)
	return DATA_TEST