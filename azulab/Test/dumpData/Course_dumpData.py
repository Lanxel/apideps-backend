
from django.core.exceptions import ValidationError
import json

from azulab.models.Course import Course
from azulab.models.AcademicUnit import AcademicUnit

def create_DumpData():
		
	cant = 4

	AUs = AcademicUnit.objects.all()


	obj1=Course.objects.create(code='1INF32',name='Lenguaje de Programacion',credits=5,academicUnit=AUs[0])
	obj2=Course.objects.create(code='1INF20',name='Sistemas Operativos',credits=4,academicUnit=AUs[0])
	obj3=Course.objects.create(code='2IEE10',name='Electronica Digital',credits=5,academicUnit=AUs[3])
	obj4=Course.objects.create(code='2IND10',name='Control y Gestion Industrial',credits=3,academicUnit=AUs[1])



	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)

	if len(Course.objects.all())==cant:
		return cant , list_object
	else:
		return -1, []