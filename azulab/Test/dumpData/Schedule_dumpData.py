from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json
from azulab.models.Schedule import Schedule
from azulab.models.Course import Course
from azulab.models.Term import Term

def create_DumpData():
	courses = Course.objects.all()
	terms = Term.objects.all()

	cant = 8

	obj1 = Schedule.objects.create(number='H0801',course=courses[0],term=terms[0])
	obj2 = Schedule.objects.create(number='H0802',course=courses[0],term=terms[0])
	obj3 = Schedule.objects.create(number='H0803',course=courses[0],term=terms[0])
	obj4 = Schedule.objects.create(number='H0804',course=courses[0],term=terms[0])

	obj5 = Schedule.objects.create(number='H0801',course=courses[0],term=terms[1])

	obj6 = Schedule.objects.create(number='H0801',course=courses[1],term=terms[0])
	obj7 = Schedule.objects.create(number='H0802',course=courses[1],term=terms[0])
	
	obj8 = Schedule.objects.create(number='H0801',course=courses[1],term=terms[1])

	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)
	list_object.append(obj5)
	list_object.append(obj6)
	list_object.append(obj7)
	list_object.append(obj8)

	if len(Schedule.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []