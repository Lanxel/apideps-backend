from django.core.exceptions import ValidationError
from azulab.fixtures import *
from azulab.models.Question import Question
from azulab.models.Section import Section
import json
from datetime import datetime
def create_DumpData():
	section = Section.objects.all()
	cant = 4

# number = models.IntegerField()
#     statement = models.TextField()
#     maxScore = models.FloatField()
#     scoreObtained = models.FloatField(null=True, blank=True)

#    section = models.ForeignKey(Section, on_delete=models.CASCADE)
#     position = models.IntegerField()
#     deleted = models.BooleanField(default=False)

	obj1 = Question.objects.create(number=1,statement='Primera Pregunta de LP',
									maxScore=float(20.0),scoreObtained=float(15.0),
									section=section[0],position=1)	

	obj2 = Question.objects.create(number=2,statement='Segunda Pregunta de LP',
									maxScore=float(20.0),scoreObtained=float(16.0),
									section=section[0],position=2)	

	obj3 = Question.objects.create(number=1,statement='Primera Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(17.0),
									section=section[2],position=1)	

	obj4 = Question.objects.create(number=2,statement='Segunda Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(18.0),
									section=section[1],position=2)	

	obj5 = Question.objects.create(number=3,statement='Tercera Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(19.0),
									section=section[1],position=3)	


	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)
	list_object.append(obj5)

	if len(Question.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []