from django.core.exceptions import ValidationError

def termDelete_DataCreator():
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)
  	FixAmount = len(data)
  	#Agregamos casos especiales, que puede fallar
	TEST_DATA = []
	TEST_DATA.append(['delete_noID', [None,data[0]['fields']['term']], ValidationError, 'Especifique ID',True,FixAmount])
	TEST_DATA.append(['delete_noID', ['',data[0]['fields']['term']], ValidationError, 'Especifique ID',True,FixAmount])
	TEST_DATA.append(['delete_ID_noFound',[999999,data[0]['fields']['term']], ValidationError, 'No se encuentra ID del term',True,FixAmount])



	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		auxFields.append(data[i]['pk'])
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA.append(['update_term_valido',auxFields,None,None,False,FixAmount-i-1])



	#Agregamos casos especiales extras


	return TEST_DATA





