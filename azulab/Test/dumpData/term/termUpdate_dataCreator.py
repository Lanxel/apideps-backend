from django.core.exceptions import ValidationError

def termUpdate_DataCreator():
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)

  	#Agregamos casos especiales, que puede fallar
	TEST_DATA = []
	TEST_DATA.append(['update_noID', [None,'2056-2'], ValidationError, 'Especifique ID',False])
	TEST_DATA.append(['update_noID', ['','2056-2'], ValidationError, 'Especifique ID',False])
	TEST_DATA.append(['update_no_existe_id', ['9999999','2056-2'], ValidationError, 'ID invalido',False])
	TEST_DATA.append(['update_no_existe_id', ['-99','2056-2'], ValidationError, 'ID invalido',False])
	
	

	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		auxFields.append(data[i]['pk'])
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA.append(['update_term_valido',auxFields,None,None,True])


	#Agregamos casos especiales, que puede fallar	
	TEST_DATA.append(['update_term_none',[data[1]['pk'],None],ValidationError,'No puede ser vacio el Term',False])
	TEST_DATA.append(['update_term_existe',[None],ValidationError,'Ya existe el term',True])

	return TEST_DATA

