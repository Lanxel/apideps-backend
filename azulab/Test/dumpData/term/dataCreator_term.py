from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json



def term_datalistCreator():	
	#COPIAMOS DATA DEL JSON (dump data)
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)
	jsonNum = len(data)	
  	#Agregamos casos especiales, que puede fallar
	TEST_DATA_LIST = []
	TEST_DATA_LIST.append(['list1_NULO',None,0])

	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA_LIST.append(['list1_valido',auxFields,i+1])

	return TEST_DATA_LIST


def term_datareadCreator():
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)
	jsonNum = len(data)	
  	#Agregamos casos especiales, que puede fallar
	TEST_DATA_READ = []
	TEST_DATA_READ.append(['read_diferente_ID',[data[0]['fields']['term']],2,False])
	TEST_DATA_READ.append(['read_diferente_term',[data[1]['fields']['term']],1,False])

	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA_READ.append(['list1_valido',auxFields,i+1,True])

	return TEST_DATA_READ

def term_dataverifyCreator():
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)
	jsonNum = len(data)	
  	#Agregamos casos especiales, que puede fallar
	TEST_DATA_VERIFY = []
	TEST_DATA_VERIFY.append(['verify_no_term',['3000-8'],False])
	

	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA_VERIFY.append(['verify_valido',auxFields,True])

	return TEST_DATA_VERIFY

def termCreate_DataCreator():
	with open('azulab/fixtures/azulab_DB_testdata_termModel.json') as f:
  		data = json.load(f)

  	#Agregamos casos especiales, que puede fallar
	TEST_DATA_CREATE = []
	TEST_DATA_CREATE.append(['Term_vacio',[''],ValidationError,'term'])
	TEST_DATA_CREATE.append(['Term_vacio',[None],ValidationError,'term'])
	

	#Agregamos casos validos
	for i in range(len(data)):		
		auxFields = []
		for field in data[i]['fields'].items():			
			auxFields.append(field[1])

		TEST_DATA_CREATE.append(['Term_valido',auxFields,None,None])


	#Agregamos casos especiales, que puede fallar	
	TEST_DATA_CREATE.append(['Term_repetido',[data[0]['fields']['term']],ValidationError,'already exists'])

	FixAmount = 0
	for i in range(len(TEST_DATA_CREATE)):
		if TEST_DATA_CREATE[i][2]== None:
			FixAmount=FixAmount+1
		TEST_DATA_CREATE[i].append(FixAmount)

	return TEST_DATA_CREATE

