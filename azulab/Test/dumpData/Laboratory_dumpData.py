from django.core.exceptions import ValidationError
from azulab.fixtures import *
from azulab.models.Laboratory import Laboratory
from azulab.models.Schedule import Schedule
import json
from datetime import datetime
def create_DumpData():
	schedule = Schedule.objects.all()
	cant = 4

	initDate =  datetime.fromisoformat('2021-11-04T00:05:23+04:00') 
	endDate = datetime.fromisoformat('2021-11-04T00:07:23+04:00') 

	obj1 = Laboratory.objects.create(number='1',title='Primer Laboratorio de LP',
									description="Semestre 2020-2",initDate=initDate,
									endDate=endDate,schedule=schedule[0],
									files=None)									
	obj2 = Laboratory.objects.create(number='2',title='Segundo Laboratorio de LP',
									description="Semestre 2020-2" ,initDate=initDate,
									endDate=endDate,schedule=schedule[0],
									files=None)	
	obj3 = Laboratory.objects.create(number='1',title='Primer Laboratorio de SO',
									description= "Semestre 2020-2",initDate=initDate,
									endDate=endDate,schedule=schedule[5],
									files=None)	
	obj4 = Laboratory.objects.create(number='2',title='Segundo Laboratorio de SO',
									description="Semestere 2020-2" ,initDate=initDate,
									endDate=endDate,schedule=schedule[5],
									files=None)	

	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)

	if len(Laboratory.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []