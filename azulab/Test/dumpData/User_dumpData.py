from azulab.fixtures import *
from django.core.exceptions import ValidationError
import json
from azulab.Services import UserServices as userSer

from azulab.models.Role import Role
from azulab.models.User import User

def create_DumpData():
	roles = Role.objects.all()

	cant = 5

	user1 = userSer.user_create_one_csv(code='20155800',
                                        first_name='Lucas Juan',
                                        last_name='Perez Ramirez',
                                        email='a20155800@pucp.edu.pe',
                                        type= roles[0])

	user2 = userSer.user_create_one_csv(code='20155801',
                                        first_name='Teo Mateo',
                                        last_name='Portal Jimenez',
                                        email='teo.mateo@pucp.edu.pe',
                                        type= roles[1])

	user3 = userSer.user_create_one_csv(code='20155802',
                                        first_name='Marcos Jesus',
                                        last_name='Quizpe Zapata',
                                        email='a20155802@pucp.edu.pe',
                                        type= roles[2])

	user4 = userSer.user_create_one_csv(code='20155803',
                                        first_name='Judas',
                                        last_name='Lopez Rodriguez',
                                        email='a20155803@pucp.edu.pe',
                                        type= roles[3])
	user5 = userSer.user_create_one_csv(code='20155804',
                                        first_name='Simon Noel',
                                        last_name='De la cruz',
                                        email='sim.noel@pucp.edu.pe',
                                        type= roles[1])

	list_object  = []
	list_object.append(user1)
	list_object.append(user2)
	list_object.append(user3)
	list_object.append(user4)
	list_object.append(user5)

	if len(User.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []