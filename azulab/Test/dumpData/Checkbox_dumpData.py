from django.core.exceptions import ValidationError
from azulab.fixtures import *
from azulab.models.Checkbox import Checkbox
from azulab.models.Section import Section
import json
from datetime import datetime
def create_DumpData():
	section = Section.objects.all()
	cant = 5

# number = models.IntegerField()
#     statement = models.TextField()
#     maxScore = models.FloatField()
#     scoreObtained = models.FloatField(null=True, blank=True)

#    section = models.ForeignKey(Section, on_delete=models.CASCADE)
#     position = models.IntegerField()
#     deleted = models.BooleanField(default=False)

   # options = models.JSONField()  #Dictionary to store alternatives and clues/feedback
   #  type = models.CharField(max_length=15)  #Enum of checkbox question type

	chbx1 = {
		"1": {
			"op1": "No es verdadero",
			"op2": "No es falso",
			"op3": "Todas las anteriores"
		}
	}
	chbx2 = {
		"2": {
			"op1": "No es verdadero",
			"op2": "No es falso",
			"op3": "Todas las anteriores"
		}
	}
	chbx3 = {
		"1": {
			"op1": "No es verdadero",
			"op2": "No es falso",
			"op3": "Todas las anteriores"
		}
	}
	chbx4 = {
		"1": {
			"op1": "No es verdadero",
			"op2": "No es falso",
			"op3": "Todas las anteriores"
		}
	}
	chbx5 = {
		"2": {
			"op1": "No es verdadero",
			"op2": "No es falso",
			"op3": "Todas las anteriores"
		}
	}

	obj1 = Checkbox.objects.create(number=1,statement='Primera Pregunta de LP',
									maxScore=float(20.0),scoreObtained=float(15.0),
									section=section[0],position=1,
									options=chbx1,type='MultipleChoice')	

	obj2 = Checkbox.objects.create(number=2,statement='Segunda Pregunta de LP',
									maxScore=float(20.0),scoreObtained=float(16.0),
									section=section[0],position=2,
									options=chbx2,type='MultipleChoice')	

	obj3 = Checkbox.objects.create(number=1,statement='Primera Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(17.0),
									section=section[2],position=1,
									options=chbx3,type='MultipleChoice')	

	obj4 = Checkbox.objects.create(number=2,statement='Segunda Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(18.0),
									section=section[1],position=2,
									options=chbx4,type='MultipleChoice')	

	obj5 = Checkbox.objects.create(number=3,statement='Tercera Pregunta de files',
									maxScore=float(20.0),scoreObtained=float(19.0),
									section=section[1],position=3,
									options=chbx5,type='MultipleChoice')	


	list_object  = []
	list_object.append(obj1)
	list_object.append(obj2)
	list_object.append(obj3)
	list_object.append(obj4)
	list_object.append(obj5)

	if len(Checkbox.objects.all())==cant:
		return cant , list_object
	else:	
		return -1, []