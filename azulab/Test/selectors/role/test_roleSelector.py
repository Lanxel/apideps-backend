import datetime
import unittest
from django.test import TestCase
from azulab.models.Role import Role
from azulab.Selectors import RoleSelector as roleSelector
from azulab.Services import RoleServices as roleService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import Role_dumpData as Roledata

class AzulabRoleModel_test_selectores_list(TestCase):	

	def setUp(self):
		self.cantInicio, self.objInicio = Roledata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Roles')

	def test_Role_selector_list_correcto(self):
		test_data = []
		test_data.append([None,self.cantInicio,'Fallo en el listar , Cantidad no coincide'])
		
		for search,cantEsperada,errorMsg in test_data:
			with self.subTest(name=f'Search : {search}'):
				rolesTest = roleSelector.role_list()
				self.assertEqual(len(rolesTest),cantEsperada,errorMsg)					
		
		print('Test: Role_Selector_List_Correctos...Terminado')





class AzulabRoleModel_test_selectores_verify(TestCase):
	def setUp(self):
		self.cantInicio, self.objInicio = Roledata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Roles')

	def test_Role_selector_verify_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append(['Alumno',True,'Fallo al verify un role existente, Alumno'])
		test_data.append(['Docente',True,'Fallo al verify un role existente, Docente'])
		test_data.append(['Administrador',True,'Fallo al verify un role existente, Administrador'])
		test_data.append(['Super Administrador',True,'Fallo al verify un role existente, Super Administrador'])


		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'Role : {search}'):
				rolesTest = roleSelector.role_verifyName(name=search)
				self.assertEqual(valorEsperada,rolesTest,errorMsg)					
		

		print('Test: Role_Selector_verify_Correctos...Terminado')

	def test_Role_selector_verify_incorrecto(self):
		test_data = [] # id,tipo error, ValorEsperado , nombre test
		test_data.append(['Desarrolador',None,None,'Fallo al leer un role no existente (Desarrolador)'])
		test_data.append(['',ValueError,None,'Fallo al leer un role no existente (Desarrolador)'])
		test_data.append([32,ValueError,None,'Fallo al leer un role con no string (int)'])
		test_data.append([None,ValueError,None,'Fallo al leer un role con None'])

		for search,errorTipo,valor,errorMsg in test_data:
			if errorTipo != None:
				with self.assertRaisesRegex(errorTipo):
					rolesTest = roleSelector.role_verifyName(name=search)					
			else:
				rolesTest = roleSelector.role_verifyName(name=search)
				self.assertEqual(rolesTest,valor)
		print('Test: Role_Selector_verify_Inorrectos...Terminado')




class AzulabRoleModel_test_selectores_read(TestCase):
	def setUp(self):
		self.cantInicio, self.objInicio = Roledata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Roles')

	def test_Role_selector_read_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append([self.objInicio[0].id,self.objInicio[0].name,'Fallo al leer un role existente'])
		test_data.append([self.objInicio[1].id,self.objInicio[1].name,'Fallo al leer un role existente'])

		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'Id : {search}'):
				objTest = roleSelector.role_read(id=search)
				self.assertIsInstance(objTest,Role,'Resultado de read no es un objecto Role')
				self.assertEqual(objTest.name,valorEsperada,errorMsg)					
		
		print('Test: Term_Selector_read_Correctos...Terminado')

	def test_Role_selector_read_incorrecto(self):
		test_data = [] # id,   mensaje
		test_data.append([-1,None,'not exists','Fallo al leer un term con id no existente'])
		test_data.append(['test',ValueError,'id','Fallo al leer un term con id no integer (str)'])

		for search,errorTipo,valor,errorMsg in test_data:
			if errorTipo != None:
				with self.assertRaisesRegex(errorTipo,valor):
					roleTest = termSelector.term_read(id=search)
			else:
				roleTest = termSelector.term_read(id=search)
				self.assertTrue(valor in roleTest['error'].lower())


		print('Test: Term_Selector_read_Inorrectos...Terminado')




