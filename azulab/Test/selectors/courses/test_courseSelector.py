import datetime
import unittest
from django.test import TestCase
from azulab.models.Course import Course
from azulab.Selectors import CourseSelector as courseSelector
from azulab.Services import CourseServices as courseService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import Course_dumpData as Coursedata
from azulab.Test.dumpData import AcademicUnit_dumpData as AcademicUnitdata

class AzulabCourseModel_test_selectores_list(TestCase):	
	def setUp(self):
		self.cantInicioAU, self.objInicioAU = AcademicUnitdata.create_DumpData()
		self.cantInicio, self.objInicio = Coursedata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Courses')

	def test_Course_selector_list_correcto(self):
		test_data = []
		test_data.append([None,4,'Fallo en el listar Course, Cantidad no coincide'])
		test_data.append(['Lenguaje de Programacion',1,'Fallo al hacer listar match con un solo elemento'])
		test_data.append(['a',4,'Fallo al hacer listar match con todos los elemento'])

		for search,cantEsperada,errorMsg in test_data:
			with self.subTest(name=f'Search : {search}'):
				coursesTest = courseSelector.course_list(search=search)				
				self.assertEqual(len(coursesTest),cantEsperada,errorMsg)					
		
		print('Test: Course_Selector_List_Correctos...Finalizado')

	def test_Course_selector_list_incorrecto(self):
		test_data = []
		test_data.append([23,None,None,'Fallo al listar course con search invalido(int)'])
		test_data.append(['9999',ValueError,'id','Fallo al listar course con search invalido(no existe)'])
		
		for search,errorTipo,valor,errorMsg in test_data:		
			if errorTipo != None:
				with self.assertRaisesRegex(errorTipo,valor)
				coursesTest = courseSelector.course_list(search=search)						
			else:
				coursesTest = courseSelector.course_list(search=search)	
				self.assertEqual(coursesTest,valor)
			

		print('Test: Course_Selector_List_Incorrectos...Finalizado')


class AzulabCourseModel_test_selectores_read(TestCase):
	def setUp(self):
		self.cantInicioAU, self.objInicioAU = AcademicUnitdata.create_DumpData()
		self.cantInicio, self.objInicio = Coursedata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Courses')

	def test_Course_selector_read_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append([self.objInicio[0].id,self.objInicio[0].name,'Fallo al leer un course existente, valor incorrecto'])
		test_data.append([self.objInicio[1].id,self.objInicio[1].name,'Fallo al leer un course existente, valor incorrecto'])

		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'Id : {valorEsperada}'):
				coursesTest = courseSelector.course_read(id=search)
				self.assertEqual(coursesTest.name,valorEsperada,errorMsg)					
		
		print('Test: Course_Selector_read_Correctos...Finalizado')

	def test_Course_selector_read_incorrecto(self):
		test_data = [] # id,   mensaje
		test_data.append([-1,None,'not exists','Fallo al leer un course con id no existente'])
		test_data.append(['test',ValueError,'id','Fallo al leer un course con id no integer (str)'])

		for search,errorTipo,valor,errorMsg in test_data:
			if errorTipo != None:
				with self.assertRaise(errorTipo,valor,errorMsg):
					coursesTest = courseSelector.course_read(id=search)					
			else:
				coursesTest = courseSelector.course_read(id=search)					
				self.assertNotIsInstance(coursesTest,Course,errorMsg)
				self.assertTrue( valor in coursesTest['error'].lower(),errorMsg)
				

		print('Test: Course_Selector_read_Incorrectos...Finalizado')



