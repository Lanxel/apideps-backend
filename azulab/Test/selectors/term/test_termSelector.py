import datetime
import unittest
from django.test import TestCase
from azulab.models.Term import Term
from azulab.Selectors import TermSelector as termSelector
from azulab.Services import TermServices as termService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import Term_dumpData as Termdata

class AzulabTermModel_test_selectores_list(TestCase):	
	def setUp(self):
		self.cantInicio, self.objInicio = Termdata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Terms')

	def test_Term_selector_list_correcto(self):
		test_data = []
		test_data.append([None,4,'Fallo en el listar Terms, Cantidad no coincide'])
		test_data.append(['2020-2',1,'Fallo al hacer listar match con un solo elemento'])
		test_data.append(['20',4,'Fallo al hacer listar match con todos los elemento'])

		for search,cantEsperada,errorMsg in test_data:
			with self.subTest(name=f'Search : {search}'):
				termsTest = termSelector.term_list(search=search)
				self.assertEqual(len(termsTest),cantEsperada,errorMsg)					
		
		print('Test: Term_Selector_List_Correctos...Terminado')

	def test_Term_selector_list_incorrecto(self):
		test_data = []
		test_data.append([23,None,'Fallo al listar term con search invalido(int)'])
		test_data.append(['9999',None,'Fallo al listar term con search invalido(no existe)'])
		
		for search,cantEsperada,errorMsg in test_data:
			termsTest = termSelector.term_list(search=search)					
			self.assertEqual(termsTest,cantEsperada)
			

		print('Test: Term_Selector_List_Inorrectos...Terminado')




class AzulabTermModel_test_selectores_read(TestCase):
	def setUp(self):
		self.cantInicio, self.objInicio = Termdata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Terms')

	def test_Term_selector_read_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append([self.objInicio[0].id,self.objInicio[0].term,'Fallo al leer un term existente, valor incorrecto'])
		test_data.append([self.objInicio[1].id,self.objInicio[1].term,'Fallo al leer un term existente, valor incorrecto'])

		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'Id : {search}'):
				termsTest = termSelector.term_read(id=search)
				self.assertEqual(termsTest.term,valorEsperada,errorMsg)					
		
		print('Test: Term_Selector_read_Correctos...Terminado')

	def test_Term_selector_read_incorrecto(self):
		test_data = [] # id,   mensaje
		test_data.append([-1,None,'not exist','Fallo al leer un term con id no existente'])
		test_data.append(['test',ValueError,'id','Fallo al leer un term con id no integer (str)'])

		for search,errorTipo,valor,errorMsg in test_data:
			if errorTipo != None:
				with self.assertRaisesRegex(errorTipo,valor,errorTipo):
					termsTest = termSelector.term_read(id=search)
			else:
				termsTest = termSelector.term_read(id=search)
				self.assertNotIsInstance(termsTest,Term,errorMsg)
				self.assertTrue(valor in termsTest['error'].lower(),errorMsg)
					

		print('Test: Term_Selector_read_Inorrectos...Terminado')



