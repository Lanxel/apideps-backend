import datetime
import unittest
from django.test import TestCase
from azulab.models.RoleUser import RoleUser
from azulab.Selectors import RoleUserSelector as roleuserSelector
from azulab.Services import RoleUserServices as roleuserService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import User_dumpData as Userdata
from azulab.Test.dumpData import Role_dumpData as Roledata


# class AzulabRoleUserModel_test_selectores_list(TestCase):	
# 	def setUp(self):
# 		self.cantInicioRole, self.objInicioRole = Roledata.create_DumpData()
# 		self.cantInicio, self.objInicio = Userdata.create_DumpData()
# 		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de RoleUsers')

# 	def test_RoleUser_selector_list_correcto(self):
# 		test_data = []
# 		test_data.append([None,self.cantInicio,'Fallo en el listar RoleUsers, Cantidad no coincide'])
# 		test_data.append(['Ingenieria Informatica',1,'Fallo al hacer listar match con un solo elemento'])
# 		test_data.append(['Ingenieria',self.cantInicio,'Fallo al hacer listar match con todos los elemento'])

# 		for search,cantEsperada,errorMsg in test_data:
# 			with self.subTest(name=f'Search : {search}'):
# 				roleusersTest = roleuserSelector.roleuser_list(search=search)
# 				self.assertEqual(len(roleusersTest),cantEsperada,errorMsg)					
		
# 		print('Test: RoleUser_Selector_List_Correctos...Finalizado')

# 	def test_RoleUser_selector_list_incorrecto(self):
# 		test_data = []
# 		test_data.append([23,None,'Fallo al listar roleuser con search invalido(int)'])
# 		test_data.append(['9999',None,'Fallo al listar roleuser con search invalido(no existe)'])
		
# 		for search,cantEsperada,errorMsg in test_data:
# 			roleusersTest = -1
# 			try:
# 				with self.assertRaise(msg=errorMsg):
# 					roleusersTest = roleuserSelector.roleuser_list(search=search)										
# 					self.assertEqual(roleusersTest,cantEsperada)
# 			except Exception:
# 				pass	

# 		print('Test: RoleUser_Selector_List_Inorrectos...Finalizado')




# class AzulabRoleUserModel_test_selectores_read(TestCase):
# 	def setUp(self):
# 		self.cantInicioRole, self.objInicioRole = Roledata.create_DumpData()
# 		self.cantInicio, self.objInicio = Userdata.create_DumpData()
# 		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de RoleUsers')

# 	def test_RoleUser_selector_read_correcto(self):
# 		test_data = [] # id,  valorEsperada , mensaje
# 		test_data.append([self.objInicio[0].id,self.objInicio[0].name,'Fallo al leer un roleuser existente, valor incorrecto'])
# 		test_data.append([self.objInicio[1].id,self.objInicio[1].name,'Fallo al leer un roleuser existente, valor incorrecto'])

# 		for search,valorEsperada,errorMsg in test_data:
# 			with self.subTest(name=f'Id : {valorEsperada}'):
# 				roleusersTest = roleuserSelector.roleuser_read(id=search)
# 				self.assertEqual(roleusersTest.name,valorEsperada,errorMsg)					
		
# 		print('Test: RoleUser_Selector_read_Correctos...Finalizado')

# 	def test_RoleUser_selector_read_incorrecto(self):
# 		test_data = [] # id,   mensaje
# 		test_data.append([-1,'Fallo al leer un roleuser con id no existente'])
# 		test_data.append(['test','Fallo al leer un roleuser con id no integer (str)'])

# 		for search,errorMsg in test_data:
# 			try:
# 				with self.assertRaise(msg=errorMsg):
# 					roleusersTest = roleuserSelector.roleuser_read(id=search)					
# 					self.assertNotIsInstance(roleusersTest,RoleUser,errorMsg)
# 			except Exception:
# 				pass	

# 		print('Test: RoleUser_Selector_read_Incorrectos...Finalizado')



# class AzulabRoleUserModel_test_selectores_read_name(TestCase):
# 	def setUp(self):
# 		self.cantInicioRole, self.objInicioRole = Roledata.create_DumpData()
# 		self.cantInicio, self.objInicio = Userdata.create_DumpData()
# 		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de RoleUsers')

# 	def test_RoleUser_selector_read_name_correcto(self):
# 		test_data = [] # id,  valorEsperada , mensaje
# 		test_data.append(['Ingenieria Informatica',True,'Fallo al verify un roleuser existente, Informatica'])
# 		test_data.append(['Ingenieria Industrial',True,'Fallo al verify un roleuser existente, Industrial'])
# 		test_data.append(['Ingenieria Civil',True,'Fallo al verify un roleuser existente, Civil'])
# 		test_data.append(['Ingenieria Electornica',True,'Fallo al verify un roleuser existente, Electornica'])


# 		for search,valorEsperada,errorMsg in test_data:
# 			with self.subTest(name=f'RoleUser : {search}'):
# 				roleusersTest = roleuserSelector.roleuser_read_by_name(name=search)
# 				self.assertEqual(valorEsperada,isinstance(roleusersTest,RoleUser),errorMsg)					
		

# 		print('Test: RoleUser_Selector_read_name_Correctos...Terminado')

# 	def test_RoleUser_selector_read_name_incorrecto(self):
# 		test_data = [] # id,valor   mensaje
# 		test_data.append(['Desarrolador',False,'Fallo al leer un roleuser no existente (Desarrolador)'])
# 		test_data.append(['',False,'Fallo al leer un roleuser no existente ('')'])
# 		test_data.append([32,False,'Fallo al leer un roleuser con no string (int)'])
# 		test_data.append([None,False,'Fallo al leer un roleuser con None'])

# 		for search,valor,errorMsg in test_data:
# 			try:
# 				with self.assertRaise(msg=errorMsg):
# 					self.assertEqual(valorEsperada,isinstance(roleusersTest,RoleUser),errorMsg)										
# 					self.assertEqual(valor,roleusersTest,errorMsg)
# 			except Exception:
# 				pass	

# 		print('Test: RoleUser_Selector_read_name_Inorrectos...Terminado')