import datetime
import unittest
from django.test import TestCase
from azulab.models.Schedule import Schedule
from azulab.Selectors import ScheduleSelector as scheduleSelector
from azulab.Services import ScheduleServices as scheduleService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import Schedule_dumpData as Scheduledata
from azulab.Test.dumpData import Term_dumpData as Termdata
from azulab.Test.dumpData import Course_dumpData as Coursedata
from azulab.Test.dumpData import AcademicUnit_dumpData as AcademicUnitdata
from django.core.exceptions import ObjectDoesNotExist

class AzulabScheduleModel_test_selectores_list(TestCase):	
	def setUp(self):
		self.cantInicioAU, self.objInicioAU = AcademicUnitdata.create_DumpData()
		self.cantInicioTerm, self.objInicioTerm = Termdata.create_DumpData()
		self.cantInicioCourse, self.objInicioCourse = Coursedata.create_DumpData()
		self.cantInicio, self.objInicio = Scheduledata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Schedules')

	# def test_Schedule_selector_list_correcto(self):
	# 	test_data = []
	# 	test_data.append([None,None,self.cantInicio,'Fallo en el listar Schedules, Cantidad no coincide'])
	# 	test_data.append([self.objInicio[0].course.id,None,5,'Fallo al hacer listar match con Course'])		
	# 	test_data.append([None,self.objInicio[0].term.id,6,'Fallo al hacer listar match con Term'])		
	# 	test_data.append([self.objInicio[4].course.id,self.objInicio[4].term.id,1,'Fallo al hacer listar match con Term y Course'])
	# 	test_data.append([self.objInicio[0].course.id,self.objInicio[0].term.id,4,'Fallo al hacer listar match con 4 los elemento'])

	# 	for course,term,cantEsperada,errorMsg in test_data:
	# 		with self.subTest(name=f'Course : {course} -  Term {term}'):
	# 			schedulesTest = scheduleSelector.schedule_list(codeCourse=course,term=term)
	# 			self.assertEqual(len(schedulesTest),cantEsperada,errorMsg)					
		
	# 	print('Test: Schedule_Selector_List_Correctos...Finalizado')

	def test_Schedule_selector_list_incorrecto(self):
		test_data = []
		#test_data.append([None  ,None  ,self.cantInicio,None      ,None    ,'Message','Fallo en el listar Schedules, filtros invalidos'])		
						 # Course, term , Cant,        ,Error,    mensaje,  Test nombre    
		test_data.append(['id=3',None  ,None           ,ValueError,'id'    ,'Fallo al hacer listar match con Course invalido'])		
		test_data.append([-1    ,None  ,None           ,None      ,'course','Fallo al hacer listar match con Course no existe'])
		test_data.append([None  ,-1    ,None           ,None      ,'term'  ,'Fallo al hacer listar match con Term no existe'])
		test_data.append([None  ,'id=3',None           ,ValueError,'id'   ,'Fallo al hacer listar match con Term invalido'])
		test_data.append(['id=3','id=3',None           ,ValueError,'id'   ,'Fallo al hacer listar match con Term y Course invalidos'])
		

		for course, term, cantEsperada, errorTipo, mensajeX, errorMsg in test_data:			
			
			with self.subTest(name=f'Course : {course} - Term {term}'):
				if errorTipo == None:					
						schedulesTest = scheduleSelector.schedule_list(codeCourse=course,term=term)
						print(schedulesTest['error'])
						self.assertEqual(True,mensajeX in schedulesTest['error'].lower())
				else:
					with self.assertRaisesRegex(errorTipo,mensajeX):
						schedulesTest = scheduleSelector.schedule_list(codeCourse=course,term=term)		

		print('Test: Schedule_Selector_List_Inorrectos...Finalizado')




# class AzulabScheduleModel_test_selectores_read(TestCase):
# 	def setUp(self):
# 		self.cantInicioCourse, self.objInicioCourse = Coursedata.create_DumpData()
# 		self.cantInicioTerm, self.objInicioTerm = Termdata.create_DumpData()
# 		self.cantInicio, self.objInicio = Scheduledata.create_DumpData()
# 		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Schedules')

# 	def test_Schedule_selector_read_correcto(self):
# 		test_data = [] # id,  valorEsperada , mensaje
# 		test_data.append([self.objInicio[0].id,self.objInicio[0].name,'Fallo al leer un schedule existente, valor incorrecto'])
# 		test_data.append([self.objInicio[1].id,self.objInicio[1].name,'Fallo al leer un schedule existente, valor incorrecto'])

# 		for search,valorEsperada,errorMsg in test_data:
# 			with self.subTest(name=f'Id : {valorEsperada}'):
# 				schedulesTest = scheduleSelector.schedule_read(id=search)
# 				self.assertEqual(schedulesTest.name,valorEsperada,errorMsg)					
		
# 		print('Test: Schedule_Selector_read_Correctos...Finalizado')

# 	def test_Schedule_selector_read_incorrecto(self):
# 		test_data = [] # id,   mensaje
# 		test_data.append([-1,'Fallo al leer un schedule con id no existente'])
# 		test_data.append(['test','Fallo al leer un schedule con id no integer (str)'])

# 		for search,errorMsg in test_data:
# 			try:
# 				with self.assertRaise(msg=errorMsg):
# 					schedulesTest = scheduleSelector.schedule_read(id=search)					
# 					self.assertNotIsInstance(schedulesTest,Schedule,errorMsg)
# 			except Exception:
# 				pass	

# 		print('Test: Schedule_Selector_read_Incorrectos...Finalizado')



# class AzulabScheduleModel_test_selectores_read_name(TestCase):
# 	def setUp(self):
# 		self.cantInicioCourse, self.objInicioCourse = Coursedata.create_DumpData()
# 		self.cantInicioTerm, self.objInicioTerm = Termdata.create_DumpData()
# 		self.cantInicio, self.objInicio = Scheduledata.create_DumpData()
# 		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de Schedules')

# 	def test_Schedule_selector_read_name_correcto(self):
# 		test_data = [] # id,  valorEsperada , mensaje
# 		test_data.append(['Ingenieria Informatica',True,'Fallo al verify un schedule existente, Informatica'])
# 		test_data.append(['Ingenieria Industrial',True,'Fallo al verify un schedule existente, Industrial'])
# 		test_data.append(['Ingenieria Civil',True,'Fallo al verify un schedule existente, Civil'])
# 		test_data.append(['Ingenieria Electornica',True,'Fallo al verify un schedule existente, Electornica'])


# 		for search,valorEsperada,errorMsg in test_data:
# 			with self.subTest(name=f'Schedule : {search}'):
# 				schedulesTest = scheduleSelector.schedule_read_by_name(name=search)
# 				self.assertEqual(valorEsperada,isinstance(schedulesTest,Schedule),errorMsg)					
		

# 		print('Test: Schedule_Selector_read_name_Correctos...Terminado')

# 	def test_Schedule_selector_read_name_incorrecto(self):
# 		test_data = [] # id,valor   mensaje
# 		test_data.append(['Desarrolador',False,'Fallo al leer un schedule no existente (Desarrolador)'])
# 		test_data.append(['',False,'Fallo al leer un schedule no existente ('')'])
# 		test_data.append([32,False,'Fallo al leer un schedule con no string (int)'])
# 		test_data.append([None,False,'Fallo al leer un schedule con None'])

# 		for search,valor,errorMsg in test_data:
# 			try:
# 				with self.assertRaise(msg=errorMsg):
# 					self.assertEqual(valorEsperada,isinstance(schedulesTest,Schedule),errorMsg)										
# 					self.assertEqual(valor,schedulesTest,errorMsg)
# 			except Exception:
# 				pass	

# 		print('Test: Schedule_Selector_read_name_Inorrectos...Terminado')