import datetime
import unittest
from django.test import TestCase
from azulab.models.AcademicUnit import AcademicUnit
from azulab.Selectors import AcademicUnitSelector as academicunitSelector
from azulab.Services import AcademicUnitServices as academicunitService
from django.core.exceptions import ValidationError
from azulab.Test.dumpData import AcademicUnit_dumpData as AcademicUnitdata

class AzulabAcademicUnitModel_test_selectores_list(TestCase):	
	def setUp(self):
		self.cantInicio, self.objInicio = AcademicUnitdata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de AcademicUnits')

	def test_AcademicUnit_selector_list_correcto(self):
		test_data = []
		test_data.append([None,4,'Fallo en el listar AcademicUnits, Cantidad no coincide'])
		test_data.append(['Ingenieria Informatica',1,'Fallo al hacer listar match con un solo elemento'])
		test_data.append(['Ingenieria',4,'Fallo al hacer listar match con todos los elemento'])

		for search,cantEsperada,errorMsg in test_data:
			with self.subTest(name=f'Search : {search}'):			
				academicunitsTest = academicunitSelector.academicunit_list(search=search)
				self.assertEqual(len(academicunitsTest),cantEsperada,errorMsg)					
	
		print('Test: AcademicUnit_Selector_List_Correctos...Finalizado')

	def test_AcademicUnit_selector_list_incorrecto(self):
		test_data = []
		test_data.append([23,0,None,None,'Fallo al listar academicunit con search invalido(int)'])
		test_data.append(['9999',0,None,None,'Fallo al listar academicunit con search invalido(no existe)'])
	
		for search,cantEsperada,errorTipo,msj,errorMsg in test_data:	
			with self.subTest(name=f'UA : {search}'):				
				if errorTipo == None:					
						academicunitsTest = academicunitSelector.academicunit_list(search=search)						
						self.assertEqual(cantEsperada,len(academicunitsTest))
				else:
					with self.assertRaisesRegex(errorTipo,msj):
						academicunitsTest = academicunitSelector.academicunit_list(search=search)
				


		print('Test: AcademicUnit_Selector_List_Inorrectos...Finalizado')




class AzulabAcademicUnitModel_test_selectores_read(TestCase):
	def setUp(self):
		self.cantInicio, self.objInicio = AcademicUnitdata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de AcademicUnits')

	def test_AcademicUnit_selector_read_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append([self.objInicio[0].id,self.objInicio[0].name,'Fallo al leer un academicunit existente, valor incorrecto'])
		test_data.append([self.objInicio[1].id,self.objInicio[1].name,'Fallo al leer un academicunit existente, valor incorrecto'])

		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'Id : {valorEsperada}'):			
				academicunitsTest = academicunitSelector.academicunit_read(id=search)
				self.assertEqual(academicunitsTest.name,valorEsperada,errorMsg)					
		
		print('Test: AcademicUnit_Selector_read_Correctos...Finalizado')

	def test_AcademicUnit_selector_read_incorrecto(self):
		test_data = [] # id,   mensaje
		test_data.append([-1,None,'id','Fallo al leer un academicunit con id no existente'])
		test_data.append(['test',ValueError,'id','Fallo al leer un academicunit con id no integer (str)'])

		for search,errorTipo,msj,errorMsg in test_data:
			with self.subTest(name=f'Id : {search}'):		
				if errorTipo==None:
					academicunitsTest = academicunitSelector.academicunit_read(id=search)					
					self.assertNotIsInstance(academicunitsTest,AcademicUnit,errorMsg)
					self.assertTrue(msj in academicunitsTest['error'],errorMsg)
				else:
					with self.assertRaisesRegex(errorTipo,msj):
						academicunitsTest = academicunitSelector.academicunit_read(id=search)					

			
		print('Test: AcademicUnit_Selector_read_Incorrectos...Finalizado')



class AzulabAcademicUnitModel_test_selectores_read_name(TestCase):
	def setUp(self):
		self.cantInicio, self.objInicio = AcademicUnitdata.create_DumpData()
		self.assertFalse(-1==self.cantInicio,'Error en la creacion inicial de AcademicUnits')

	def test_AcademicUnit_selector_read_name_correcto(self):
		test_data = [] # id,  valorEsperada , mensaje
		test_data.append(['Ingenieria Informatica',True,'Fallo al verify un academicunit existente, Informatica'])
		test_data.append(['Ingenieria Industrial',True,'Fallo al verify un academicunit existente, Industrial'])
		test_data.append(['Ingenieria Civil',True,'Fallo al verify un academicunit existente, Civil'])
		test_data.append(['Ingenieria Electornica',True,'Fallo al verify un academicunit existente, Electornica'])


		for search,valorEsperada,errorMsg in test_data:
			with self.subTest(name=f'AcademicUnit : {search}'):			
				academicunitsTest = academicunitSelector.academicunit_read_by_name(name=search)
				self.assertEqual(valorEsperada,isinstance(academicunitsTest,AcademicUnit),errorMsg)					
		

		print('Test: AcademicUnit_Selector_read_name_Correctos...Terminado')

	def test_AcademicUnit_selector_read_name_incorrecto(self):
		test_data = [] # id,valor   mensaje
		test_data.append(['Desarrolador','name',TypeError,'id','Fallo al leer un academicunit no existente (Desarrolador)'])
		test_data.append(['','name',TypeError,'id','Fallo al leer un academicunit no existente ('')'])
		test_data.append([32,'name',TypeError,'id','Fallo al leer un academicunit con no string (int)'])
		test_data.append([None,'name',TypeError,'id','Fallo al leer un academicunit con None'])
	

		for search,valorEsperado,errorTipo,msj,errorMsg in test_data:
			with self.subTest(name=f'Id : {search}'):	
				if errorTipo==None:
					academicunitsTest = academicunitSelector.academicunit_read_by_name(id=search)					
					self.assertNotIsInstance(academicunitsTest,AcademicUnit,errorMsg)
					self.assertTrue(valorEsperado in academicunitsTest['error'],lower(),errorMsg)
				else:
					with self.assertRaisesRegex(errorTipo,msj):
						academicunitsTest = academicunitSelector.academicunit_read_by_name(id=search)					


		print('Test: AcademicUnit_Selector_read_name_Inorrectos...Terminado')