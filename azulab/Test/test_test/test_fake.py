
import unittest



from django.test import TestCase,RequestFactory,Client
from django.test import TransactionTestCase 
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group, Permission
from django.urls import reverse

from rest_framework.test import APIRequestFactory
from rest_framework.test import RequestsClient
from rest_framework.test import APIClient
from rest_framework.test import CoreAPIClient


from requests.auth import HTTPBasicAuth
from rest_framework.test import force_authenticate
from rest_framework.compat import coreapi, requests
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated,AllowAny


from azulab.API.UserAPI import UserCreateFromCSV
from azulab.Test.dumpData import  *

from azulab.Services.UserServices import user_create_from_csv,user_create_one_csv,user_update,user_generate_token
from azulab.Services.RoleServices import role_create
from azulab.Services.RoleUserServices import roleUser_create
from azulab.Services import TextServices as txtSer
from azulab.Services import LaboratoryServices as labSer
from azulab.Services import CheckboxServices as chbxSer
from azulab.Services import CommentServices as commentSer

from azulab.Selectors.UserSelector import user_read,user_list
from azulab.Selectors import LaboratorySelector as labSel
from azulab.Selectors import QuestionSelector as questionSel

from azulab.models.Role import Role
from azulab.models.Term import Term
from azulab.models.AcademicUnit import AcademicUnit
from azulab.models.User import UserProfile, User
from azulab.models.Text import Text
from azulab.models.Course import Course
from azulab.models.Schedule import Schedule
from azulab.models.Laboratory import Laboratory
from azulab.models.Module import Module
from azulab.models.Section import Section
from azulab.models.Page import Page
from azulab.models.Element import Element

from azulab.models.Question import Question
from azulab.models.Checkbox import Checkbox
from azulab.models.RichTextBox import RichTextBox
from azulab.models.Comment import Comment


from azulab.Test.dumpData import ALL_dumpData as dp
from azulab.Test.dumpData import AcademicUnit_dumpData as AUdata
from azulab.Test.dumpData import Course_dumpData as Coursedata
from azulab.Test.dumpData import Laboratory_dumpData as Laboratorydata
from azulab.Test.dumpData import Module_dumpData as Moduledata
from azulab.Test.dumpData import Page_dumpData as Pagedata
from azulab.Test.dumpData import Question_dumpData as Questiondata
from azulab.Test.dumpData import Role_dumpData as Roledata
from azulab.Test.dumpData import Schedule_dumpData as Scheduledata
from azulab.Test.dumpData import Section_dumpData as Sectiondata
from azulab.Test.dumpData import Term_dumpData as Termdata
from azulab.Test.dumpData import User_dumpData as Userdata
from azulab.Test.dumpData import Checkbox_dumpData as Checkboxdata



import unittest
from datetime import datetime
import json
from azulab.Test.selectors.academicUnit import test_academicunitSelector as testUA

# suite=unittest.TestLoader().loadTestsFromTestCase(testUA.AzulabAcademicUnitModel_test_selectores_list)
# testResult = unittest.TextTestRunner(verbosity=2).run(suite)
# response_data = {}
# response_data['nrFailed'] = len(testResult.failures)
# response_data['output'] = "{}{}".format('\n'.join([result[1] for result in testResult.errors]),'\n'.join([result[1] for result in testResult.failures]))
# response_data['totalTests'] = testResult.testsRun
# print(response_data)
class test_pruebas(TestCase):
	
	def setUp(self):
		self.cantList = []
		self.cantRole, self.roles = Roledata.create_DumpData()
		self.cantAU, self.AUs = AUdata.create_DumpData()
		self.cantTerm, self.terms = Termdata.create_DumpData()
		self.cantCourse, self.courses = Coursedata.create_DumpData()
		self.cantSch, self.schedules = Scheduledata.create_DumpData()
		self.cantUser, self.users = Userdata.create_DumpData()
		self.cantLab, self.labs = Laboratorydata.create_DumpData()
		self.cantModule, self.Modules = Moduledata.create_DumpData()
		self.cantPage, self.Pages = Pagedata.create_DumpData()
		self.cantSection, self.Sections = Sectiondata.create_DumpData()

		self.cantList.append([self.cantRole,'Role'])
		self.cantList.append([self.cantAU,'AU'])
		self.cantList.append([self.cantTerm,'Term'])
		self.cantList.append([self.cantCourse,'Course'])
		self.cantList.append([self.cantSch,'Schedule'])
		self.cantList.append([self.cantUser,'User'])
		self.cantList.append([self.cantLab,'Laboratory'])
		self.cantList.append([self.cantModule,'Module'])
		self.cantList.append([self.cantPage,'Page'])
		self.cantList.append([self.cantSection,'Section'])



	# def test_index(self):			
	# 	for cant,obj in self.cantList:
	# 		with self.subTest(name=f'Test de creacion {obj}'):
	# 			print(f'comprara {cant}')
	# 			self.assertFalse(-1==cant,msg=f'Error en creacion de {obj}')

	# 	print('Creacion correcta de datos de prueba')

	# def test_selectors_Question(self):

	# 	cant, questions = Checkboxdata.create_DumpData()
	# 	print(f'cant checkbox {cant}')	
	# 	cantQ = Question.objects.all()
	# 	print(f'cant question {cantQ}')	
	# 	q_sec0= questionSel.question_list_section(section=self.Sections[0].id)
	# 	print(f'q in sec 0 {q_sec0}')
	# 	q_sec1= questionSel.question_list_section(section=self.Sections[1].id)
	# 	print(f'q in sec 1 {q_sec1}')
	# 	q_sec2= questionSel.question_list_section(section=self.Sections[2].id)
	# 	print(f'q in sec 2 {q_sec2}')
	# 	q_sec3= questionSel.question_list_section(section=self.Sections[3].id)
	# 	print(f'q in sec 3 {q_sec3}')
	# 	q_sec4= questionSel.question_list_section(section=self.Sections[4].id)
	# 	print(f'q in sec 4 {q_sec4}')
	# 	q_sec5= questionSel.question_list_section(section=self.Sections[5].id)
	# 	print(f'q in sec 5 {q_sec5}')
	# 	q_read_1=questionSel.question_read(id=questions[0].id)
	# 	print(f'q read id = {questions[0]} = {q_read_1}')
	# 	q_read_name=questionSel.question_read_by_name(statement=questions[0].statement)
	# 	print(f'q read name = {questions[0]} = {q_read_name}')
	# 	#self.assertTrue(len(cantQ)==cant)
		
	# 	test_data=[]
	# 	#                 search, valor esperado, errorTipo, msj, test nombre
	# 	test_data.append([''])

	def test_services_Question_checkbox(self):
		chbx1 = {
			"1": {
				"op1": "No es verdadero",
				"op2": "No es falso",
				"op3": "Todas las anteriores"
			}
		}
		chbx2 = {
			"2": {
				"op1": "No es verdadero",
				"op2": "No es falso",
				"op3": "Todas las anteriores"
			}
		}
		chbx3 = {
			"1": {
				"op1": "No es verdadero",
				"op2": "No es falso",
				"op3": "Todas las anteriores"
			}
		}
		chbx4 = {
			"1": {
				"op1": "No es verdadero",
				"op2": "No es falso",
				"op3": "Todas las anteriores"
			}
		}
		chbx5 = {
			"2": {
				"op1": "No es verdadero",
				"op2": "No es falso",
				"op3": "Todas las anteriores"
			}
		}
		

		obj1 = chbxSer.checkbox_create(number=1,statement='Primera Pregunta de LP',
										maxScore=float(20.0),scoreObtained=float(15.0),
										section=self.Sections[0].id,position=1,
										options=chbx1,type='MultipleChoice')	

		obj2 = chbxSer.checkbox_create(number=2,statement='Segunda Pregunta de LP',
										maxScore=float(20.0),scoreObtained=float(16.0),
										section=self.Sections[0].id,position=2,
										options=chbx2,type='MultipleChoice')	

		obj3 = chbxSer.checkbox_create(number=1,statement='Primera Pregunta de files',
										maxScore=float(20.0),scoreObtained=float(17.0),
										section=self.Sections[2].id,position=1,
										options=chbx3,type='MultipleChoice')	

		obj4 = chbxSer.checkbox_create(number=2,statement='Segunda Pregunta de files',
										maxScore=float(20.0),scoreObtained=float(18.0),
										section=self.Sections[1].id,position=2,
										options=chbx4,type='MultipleChoice')	

		obj5 = chbxSer.checkbox_create(number=3,statement='Tercera Pregunta de files',
										maxScore=float(20.0),scoreObtained=float(19.0),
										section=self.Sections[1].id,position=3,
										options=chbx5,type='MultipleChoice')

		questions = Question.objects.all()
		print(f'cant question {questions}')	
		q_sec0= questionSel.question_list_section(section=self.Sections[0].id)
		print(f'q in sec 0 {q_sec0}')
		q_sec1= questionSel.question_list_section(section=self.Sections[1].id)
		print(f'q in sec 1 {q_sec1}')
		q_sec2= questionSel.question_list_section(section=self.Sections[2].id)
		print(f'q in sec 2 {q_sec2}')
		q_sec3= questionSel.question_list_section(section=self.Sections[3].id)
		print(f'q in sec 3 {q_sec3}')
		q_sec4= questionSel.question_list_section(section=self.Sections[4].id)
		print(f'q in sec 4 {q_sec4}')
		q_sec5= questionSel.question_list_section(section=self.Sections[5].id)
		print(f'q in sec 5 {q_sec5}')
		q_read_1=questionSel.question_read(id=questions[0].id)
		print(f'q read id = {questions[0]} = {q_read_1}')
		q_read_name=questionSel.question_read_by_name(statement=questions[0].statement)
		print(f'q read name = {questions[0]} = {q_read_name}')
		#self.assertTrue(len(cantQ)==cant)
		commentDate =  datetime.fromisoformat('2021-11-04T00:05:23+04:00') 
		com1 = commentSer.comment_create(date=commentDate,content='Revise el tema 3 del libro'
										,question=q_read_1.id,user=self.users[0].id)
		print(com1)

		com2 = commentSer.comment_create(date=commentDate,content='Si revise el tema 3 del libro'
										,question=q_read_1.id,user=self.users[0].id)
		print(com2)