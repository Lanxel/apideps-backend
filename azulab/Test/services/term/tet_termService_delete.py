import datetime
import unittest
from django.test import TestCase
from django.test import TransactionTestCase 
from azulab.models.Term import Term
from azulab.Selectors import TermSelector as termSelector
from django.core.exceptions import ValidationError
from azulab.Test.dumpData.term import termDelete_dataCreator as dataCreator

class AzulabTermModel_test_validators(TestCase):
	#Fixture crea N = 4 elementos
	fixtures = ['azulab_DB_testdata_termModel.json']	


	def test_index(self):	
		TEST_DATA = dataCreator.termDelete_DataCreator()
		for name,value,expected,message,amount in TEST_DATA:

			exception_expected = expected is not None and issubclass(expected, Exception)
			with self.subTest(name=name, value=value):
				if exception_expected:
					with self.assertRaisesMessage(expected,message):
						term_1 = termService.term_delete(id=value[0])									
				else:
					term_1 = termService.term_delete(id=value[0])
					
				terms = termSelector.term_list()
				self.assertEqual(result,termSelector.term_verifyTerm(term=v.alue[1]))
				self.assertEqual(amount,len(terms))
					
                	
		
			
