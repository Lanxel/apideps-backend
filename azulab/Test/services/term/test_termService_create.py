import datetime
import unittest
from django.test import TestCase
from django.test import TransactionTestCase 
from azulab.models.Term import Term
from azulab.Services import TermServices as termService
from azulab.Selectors import TermSelector as termSelector
from django.core.exceptions import ValidationError
from azulab.Test.dumpData.term import dataCreator_term as dataCreator


class AzulabTermModel_test_create_validators(TransactionTestCase):

	def test_index(self):
		TEST_DATA = dataCreator.termCreate_DataCreator()

		for name,value,expected,message,amount in TEST_DATA:		
			exception_expected = expected is not None and issubclass(expected, Exception)
			with self.subTest(name=name, value=value):
				if exception_expected:
					with self.assertRaisesMessage(expected,message):
						term_1 = termService.term_create(term=value[0])
					terms = termSelector.term_list()					
					self.assertEqual(len(terms),amount)					
				else:
					term_1 = termService.term_create(term=value[0])
					terms = termSelector.term_list()
					testTerm=termSelector.term_read(id=term_1.id)
					self.assertEqual(True,termSelector.term_verifyTerm(term=value[0]))
					self.assertEqual(len(terms),amount)
					self.assertEqual(testTerm.term,value[0])
					
                
		
			


		