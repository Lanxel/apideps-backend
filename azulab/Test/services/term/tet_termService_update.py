import datetime
import unittest
from django.test import TestCase
from django.test import TransactionTestCase 
from azulab.models.Term import Term
from azulab.Services import TermServices as termService
from azulab.Selectors import TermSelector as termSelector
from django.core.exceptions import ValidationError
from azulab.Test.dumpData.term import termUpdate_dataCreator as dataCreator

	#(validate_term, '', ValidationError, 'No puede ser vacio el Term', N_fix)

	 #map(int,numbers.split(","))  

class AzulabTermModel_test_Update_validators(TestCase):
	#Fixture crea N = 4 elementos
	fixtures = ['azulab_DB_testdata_termModel.json']	

	def test_index(self):
			 	
		#term_2 = termService.term_create(termT='2019-2')		
		#resp = self.client.get('/terms/')		
		#with self.assertRaisesMessage(ValidationError,'This term already exists'):
		#	term_1 = termService.term_create(termT='2019-5')
		TEST_DATA = dataCreator.termUpdate_DataCreator()

		for name,value,expected,message,result in range(len(TEST_DATA)):
		
			exception_expected = expected is not None and issubclass(expected, Exception)
			with self.subTest(name=name, value=value):
				if exception_expected:
					with self.assertRaisesMessage(expected,message):
						term_1 = termService.term_update(id=value[0],term=value[1])					

				else:
					term_1 = termService.term_update(termT=value)				           
                self.assertEqual(result,termSelector.term_reterm_verifyTermad(term=value[1]))
		
			
