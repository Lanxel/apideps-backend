import unittest
from django.test import TestCase,RequestFactory,Client
from django.test import TransactionTestCase 
from django.core.exceptions import ValidationError
#from django.contrib.auth.models import User, Group, Permission
from django.urls import reverse

from azulab.models import User
from azulab.Selectors import UserSelector as userSel
from azulab.Services import UserServices as userSer
from azulab.Selectors import RoleSelector as roleSel
import csv, os


class test_UserServices(TestCase):
	fixtures= ['azulab_DB_testdata_roleModel.json']
	
	def setUp(self):
		self.user1 = userSer.user_create_one_csv(code='20155800',
                                                 first_name='test1',
                                                 last_name='teacher',
                                                 email='test1@pucp.edu.pe',
                                                 type= 'Alumno')

		self.user2 = userSer.user_create_one_csv(code='20155801',
                                                 first_name='test2',
                                                 last_name='student',
                                                 email='test2@pucp.edu.pe',
                                                 type= 'Docente')

		self.user3 = userSer.user_create_one_csv(code='20155802',
                                                 first_name='test3',
                                                 last_name='Admin',
                                                 email='test3@pucp.edu.pe',
                                                 type= 'Administrador')

		self.user4 = userSer.user_create_one_csv(code='20155803',
                                                 first_name='test4',
                                                 last_name='superman',
                                                 email='test4@pucp.edu.pe',
                                                 type= 'Super Administrador')

	# def test_roles_creados_exitoso(self):
	# 	_roles = ['Alumno','Docente','Administrador','Super Administrador']
	# 	roles = roleSel.role_list()
	# 	self.assertEqual(len(roles),4,'Cantiadd de roles incorrecta')
	# 	for rol in roles:
	# 		with self.subTest(name=f'Test rol {rol.name}'):
	# 			self.assertEqual(True,rol.name in _roles, f'nombre{rol.name} en Roles')
	# 	print('test_roles_creados_exitoso')
	#
	# def test_user_creados_exitoso(self):
	# 	_codes = ['20155800','20155801','20155802','20155803']
	# 	userProfiles = userSel.user_list()
	# 	self.assertEqual(len(userProfiles),4,'Cantidad de UserProfile incorrecta')
	# 	for profile in userProfiles:
	# 		with self.subTest(name=f'Test profile {profile.user}'):
	# 			self.assertEqual(True,profile.code in _codes, f'codigo {profile.code} en perfil')
	# 	print('test_user_creados_exitoso')
	#
	# def test_userRole_creados_exitoso(self):
	# 	_roles = ['Alumno','Docente','Administrador','Super Administrador']
	# 	for rol in _roles:
	# 		userRoles = userSel.user_list_by_role(type=rol)
	# 		self.assertEqual(len(userRoles),1,f'Usuario con Rol {rol} no existe')
	# 	print('test_userRole_creados_exitoso')
	#
	#
	# def test_intentar_crear_usuario_con_errores(self):
	# 	DATA = []
	# 	#            Code, fName, lName, email. type, errorMensaje, existe?
	# 	DATA.append(['20155800','test32','stundent','test32@pupc.edu.pe','Alumno','code', True])
	# 	DATA.append(['20155900','test32','stundent','test32@pupc.eu.pe','Alumno','Email', False])
	# 	DATA.append(['20155100','test32','stundent','test32@pupc.edu.pe','Alumn','Role', False])
	# 	DATA.append(['20155800','test32','stundent','test32@pupc.edu.pe','Docente','code', True])
	# 	DATA.append(['2015580880','test32','stundent','test32@pupc.edu.pe','Docente','code', False])
	#
	# 	for Code, fName, lName, email, rol,  errorMensaje, existe in DATA:
	# 		with self.subTest(name=f'Probando error en {errorMensaje}'):
	# 			try :
	# 				testUser= userSer.user_create_one(code=Code,
	# 										first_name=fName,
	# 										last_name=lName,
	# 										email= email,
	# 										type= rol)
	# 			except Exception:
	# 				pass
	# 			self.assertEqual(existe,userSel.user_verifyCode(code=Code),f'Codigo {Code}')
	# 			self.assertEqual(len(userSel.user_list()),4,'Cantidad de usuarios no coincide')
	#
	# 	print('test_intentar_crear_usuario_con_errores')


	def test_agregar_usuarios_masivamente_exitoso(self):
		_codes = ['20305800','20305801','20305802','20305803','20155800','20155801','20155802','20155803']
		line_count = 4
		cantUser = len(userSel.user_list())
		tmp_file = r'C:\Users\Antony\Documents\PUCP\Software\test_alumnos.csv'
		# Write your test data in a temporary file
		with open(tmp_file, 'r+') as file:		
			# for line in file:
			# 	if line != "\n":
			# 		line_count += 1

					
			error = userSer.user_create_from_csv(file=file,type='Alumno')

			cantNueva = line_count+cantUser
			userProfiles = userSel.user_list()
			# self.assertEqual(len(userProfiles),cantNueva,'Cantidad de UserProfile incorrecta')
			self.assertEquals(len(error),0,'Existen errores en creacion')


			# for profile in users:
			# 	with self.subTest(name=f'Test profile {profile.user}'):
			# 		self.assertEqual(True,profile.code in _codes, f'codigo {profile.code} en perfil')
			#
			# self.assertEqual(len(users),3,'cantaidad creado no coincide')
		#os.remove(tmp_file)	
		print('test_agregar_usuarios_masivamente_exitoso')
	
	# def test_agregar_usuarios_masivamente_con_erroers(self):
	# 	_codes_errores = ['20188800','20188802','201558080']
	# 	usersAntiguos = userSel.user_list()
	# 	cantUserAntigus = len(usersAntiguos)
	# 	line_count = 3
	#
	# 	tmp_file = r'C:/Users/David/Desktop/Tempo/Software/backend-azulab/azulab/Test/test_test/test_alumnos_errores.csv'
	# 	# Write your test data in a temporary file
	# 	with open(tmp_file, 'r+') as file:
	# 		# for line in file:
	# 		# 	if line != "\n":
	# 		# 		line_count += 1
	# 		try:
	# 			users, error = userSer.user_create_from_csv(file=file,type='Alumno')
	# 		except Exception:
	# 			print('Sucedio error en testeo ')
	#
	# 		userProfiles = userSel.user_list()
	# 		cantNueva = cantUserAntigus
	# 		self.assertEqual(cantNueva,cantUserAntigus,'Cantidad de UserProfile incorrecta')
	# 		self.assertEqual(len(error),line_count,'Cantidad de errores no coincide')
	# 		for profile in userProfiles:
	# 			with self.subTest(name=f'Test profile {profile.user}'):
	# 				self.assertEqual(False,profile.code in _codes_errores, f'codigo {profile.code} en perfil')
	# 	#os.remove(tmp_file)
	# 	print('test_agregar_usuarios_masivamente_con_erroers')