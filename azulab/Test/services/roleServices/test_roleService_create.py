import datetime
import unittest
from django.test import TransactionTestCase 
from django.core.exceptions import ValidationError
from django.conf import settings

from azulab.models.Role import Role
from azulab.Services import RoleServices as objService
from azulab.Selectors import RoleSelector as objSelector
from azulab.Test.dumpData.role import serviceRole_Create_dataCreator as roleData

class AzulabRoleModel_test_create_validators(TransactionTestCase ):
	def test_index(self):
		TEST_DATA = roleData.dataCreator()

		for testName, fields, error, msgError, amount, match in TEST_DATA:
			with self.subTest(name=testName, value=fields):
				testObject = None
				if error is not None:
					with self.assertRaisesMessage(error,msgError):
						try:
							testObject = objService.role_create(name=fields[0],permits=fields[1])										
						except Exception:
							raise ValidationError(msgError)

								
				else:
					testObject = objService.role_create(name=fields[0],permits=fields[1])

				listObject = objSelector.role_list()
				self.assertEqual(len(listObject),amount)
				self.assertEqual(match,testObject in listObject)
				
				
