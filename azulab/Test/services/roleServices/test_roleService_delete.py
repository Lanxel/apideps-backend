import datetime
import unittest
from django.test import TransactionTestCase 
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist


from azulab.models.Role import Role
from azulab.Services import RoleServices as objService
from azulab.Selectors import RoleSelector as objSelector
from azulab.Test.dumpData.role import serviceRole_Delete_dataCreator as roleData

class AzulabRoleModel_test_delete_validators(TransactionTestCase ):

	fixtures=['azulab/fixtures/azulab_DB_testdata_roleModel.json']
	def test_index(self):
		TEST_DATA = roleData.dataCreator()

		for testName, fields, error, msgError, amount, match in TEST_DATA:
			with self.subTest(name=testName, value=fields):
				testObject = None
				if error is not None:
					with self.assertRaisesMessage(error,msgError):
						try:
							testObject = objService.role_delete(id=fields[0])										
						except Exception: 
							raise ValidationError(msgError)
				else:
					testObject = objService.role_delete(id=fields[0])


				listObject = objSelector.role_list()
				for obj in listObject:
					self.assertEqual(match,obj.id==fields[0])
					self.assertEqual(match,obj.name==fields[1])
				self.assertEqual(len(listObject),amount)
				self.assertEqual(match,testObject in listObject)