from azulab.Services.UserServices import superuser_add_user
from django.test import TestCase
from django.db import transaction,IntegrityError, DataError
from .models.Role import Role


class TestUserCreate(TestCase):
    def setUp(self):
        Role.objects.create(name='Alumno')

    def test(self):

        data = {"code": 20141735668,
                "first_name": "Antony",
                "last_name": "Palacios Coronado",
                'email': 'apalacios@padp.com',
                'type': 'Alumno'}
        result = superuser_add_user(**data)
        self.assertRaises(result)


#da7a0aa3910cad4e05b8663c959b5e28f40db10a