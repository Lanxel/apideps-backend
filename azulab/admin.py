from django.contrib import admin
from azulab.models.AcademicUnit import AcademicUnit
from azulab.models.AnswerSheet import AnswerSheet
from azulab.models.AnswerDocument import AnswerDocument
from azulab.models.Checkbox import Checkbox
from azulab.models.Comment import Comment
from azulab.models.Course import Course
from azulab.models.Document import Document
from azulab.models.Element import Element
from azulab.models.Laboratory import Laboratory
from azulab.models.Module import Module
from azulab.models.Page import Page
from azulab.models.Question import Question
from azulab.models.RichTextBox import RichTextBox
from azulab.models.Role import Role
from azulab.models.RoleUser import RoleUser
from azulab.models.Schedule import Schedule
from azulab.models.Section import Section
from azulab.models.Term import Term
from azulab.models.Text import Text
from azulab.models.User import UserProfile
from azulab.models.UserSchedule import UserSchedule


admin.site.register(AcademicUnit)
admin.site.register(AnswerSheet)
admin.site.register(AnswerDocument)
admin.site.register(Checkbox)
admin.site.register(Comment)
admin.site.register(Course)
admin.site.register(Document)
admin.site.register(Laboratory)
admin.site.register(Module)
admin.site.register(Page)
admin.site.register(Question)
admin.site.register(RichTextBox)
admin.site.register(Role)
admin.site.register(RoleUser)
admin.site.register(Schedule)
admin.site.register(Section)
admin.site.register(Term)
admin.site.register(Text)
admin.site.register(UserProfile)
admin.site.register(UserSchedule)




