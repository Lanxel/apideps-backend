from google.auth.transport import requests
from google.oauth2 import id_token
from rest_framework.exceptions import ValidationError
from azulab.Selectors import RoleUserSelector as roleUserSel
from azulab.Services.UserServices import user_generate_token


def validate(*,auth_token):
    """
    validate method Queries the Google oAUTH2 api to fetch the user info
    """
    try:
        idinfo = id_token.verify_oauth2_token(
            auth_token, requests.Request())

        if 'accounts.google.com' in idinfo['iss']:
            return idinfo
    except:
        return "The token is either invalid or has expired"

def google_login(*,auth_token):
    user_data = validate(auth_token=auth_token)
    try:
        user_data['sub']
    except ValidationError:
        return {'error':'Token Invalido, Por favor Inicia sesión nuevamente'}

    email = user_data['email']
    userProfile = roleUserSel.roleUser_verifyIfExists(email=email)
    if userProfile is not None:
        token = user_generate_token(userProfile=userProfile)
        return token
    else:
        return {'error':'No estas registrado en el sistema. Por favor contacte a un Administrador'}