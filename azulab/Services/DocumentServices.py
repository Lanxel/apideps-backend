from azulab.models.Document import Document
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
import os

@transaction.atomic
def document_update(*,id, position=None, sectionId=None):
    try:
        with transaction.atomic():
            document = Document.objects.get(pk=id)
            if position is not None:
                document.position = position
            if sectionId is not None:
                document.section_id = sectionId
            document.save()
    except ObjectDoesNotExist:
        return {'error':'Este documento no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return document


@transaction.atomic
def document_upload_replace(*, position, sectionId, idDoc=None, file=None):
    try:
        if idDoc is None:
            document,created = Document.objects.get_or_create(position=position,section_id=sectionId,document=file)
            if not created:
                document.deleted = False
                document.save()
        else:
            document = Document.objects.get(pk=idDoc)
            if document.document:
                os.remove(document.document.path)
            document.document = file
            document.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return document

@transaction.atomic
def document_replace(*,idDoc,file):
    try:
        document = Document.objects.get(pk=idDoc)
        if document.document:
            os.remove(document.document.path)
        document.document = file
        document.save()
    except ObjectDoesNotExist:
        return {'error':'Este documento no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return document


def document_delete(*,idDoc):
    try:
        document = Document.objects.get(pk=idDoc)
        if document.document:
            os.remove(document.document.path)
        document.delete()
    except ObjectDoesNotExist:
        return {'error':'Este documento no existe.'}
    else:
        return True

@transaction.atomic
def document_create_template(*, id, idSection):
    try:
        with transaction.atomic():
            documentNew = Document.objects.get(pk=id)
            documentNew.id = None
            documentNew.section_id = idSection
            documentNew.save()
    except ObjectDoesNotExist:
        return {'error': 'Este documento no existe.'}
    else:
        return documentNew