from azulab.models.AnswerDocument import AnswerDocument
from azulab.Selectors.AnswerSheetSelector import answerSheet_verify_documents
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
import os


@transaction.atomic
def document_upload_replace(*, idQuestion, idAnswerSheet, idAsDoc=None, file=None):
    try:
        if answerSheet_verify_documents(idQuestion=idQuestion, idAnswerSheet=idAnswerSheet):
            if idAsDoc is None:
                document,created = AnswerDocument.objects.get_or_create(answerSheet_id=idAnswerSheet,
                                                                        question_id=idQuestion,document=file)
            else:
                document = AnswerDocument.objects.get(pk=idAsDoc)
                if document.document:
                    os.remove(document.document.path)
                document.document = file
                document.save()
            answerSheet_verify_documents(idQuestion=idQuestion, idAnswerSheet=idAnswerSheet,passed=True)
        else:
            return {'error':'No puede subir más archivos'}
    except ValidationError as e:
        return e.message_dict
    else:
        return document


def document_delete(*,idDoc):
    try:
        document = AnswerDocument.objects.get(pk=idDoc)
        if document.document:
            os.remove(document.document.path)
        answerSheet_verify_documents(idQuestion=document.question_id,idAnswerSheet=document.answerSheet_id,delete=True)
        document.delete()
    except ObjectDoesNotExist:
        return {'error':'Este Documento no existe.'}
    else:
        return True