from azulab.models.Text import Text
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import transaction



@transaction.atomic
def text_create(*, number, statement, maxScore, scoreObtained=0, optional=False, cantDocuments=None,
                type, answer=None, section, position):

    try:
        with transaction.atomic():
            _text,created = Text.objects.get_or_create(number=number, statement=statement,
                                                       maxScore=maxScore, scoreObtained=scoreObtained,
                                                       optional=optional, cantDocuments=cantDocuments,
                                                       type=type, answer=answer,
                                                       section_id=section,
                                                       position=position)
            if not created:
                _text.deleted = False
                _text.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return _text


@transaction.atomic
def text_update(*,id,number=None,statement=None,maxScore=None,scoreObtained=None,optional=False,cantDocuments=None,
                type=None,answer=None, section=None, position=None):
    try:
        with transaction.atomic():
            text = Text.objects.get(pk=id)
            if number is not None:
                text.number = number
            if statement is not None:
                text.statement = statement
            if maxScore is not None:
                text.maxScore = maxScore
            if scoreObtained is not None:
                text.scoreObtained = scoreObtained
            if cantDocuments is not None:
                text.cantDocuments = cantDocuments
            if answer is not None:
                text.answer = answer
            if type is not None:
                text.type = type
            if section is not None:
                text.section_id = section
            if position is not None:
                text.position = position
            text.optional = optional
            text.save()

    except ObjectDoesNotExist:
        return {'error':'Esta pregunta de texto no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return text

@transaction.atomic
def text_delete(*,id):

    try:
        with transaction.atomic():
            text = Text.objects.get(pk=id)
            text.delete()
    except ObjectDoesNotExist:
        return {'error':'Esta pregunta de texto no existe.'}
    else:
        return True


@transaction.atomic
def text_create_template(*,id, idSection):
    try:
        with transaction.atomic():
            textNew = Text.objects.get(pk=id)
            textNew.id = None
            textNew.question_ptr_id = None
            textNew.section_id = idSection
            textNew.save()
    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta de texto no existe.'}
    else:
        return textNew