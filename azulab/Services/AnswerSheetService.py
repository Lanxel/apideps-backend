from django.db import transaction
from azulab.Selectors.ModuleSelector import module_answerSheet
from azulab.models.Module import Module
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.models.AnswerSheet import AnswerSheet
from azulab.Selectors.QuestionSelector import question_get_module
from azulab.models.Laboratory import Laboratory
from azulab.Selectors.AnswerSheetSelector import answerSheet_module_maxGrade
from azulab.models.Checkbox import Checkbox
from django.utils import timezone

@transaction.atomic
def answerSheet_create(*,labId,userScheduleId):
    try:
        with transaction.atomic():
            answerSheet, created = AnswerSheet.objects.get_or_create(laboratory_id=labId,
                                                                     userSchedule_id=userScheduleId)
            if not created:
                answerSheet.deleted = False
                answerSheet.save()
                return answerSheet

            lab = Laboratory.objects.get(pk=labId)
            answers = []
            maxModuleGrades = []
            moduleGrades = []
            modules = lab.modules.filter(deleted=False)
            if modules.exists():
                for module in modules:
                    answer = module_answerSheet(id=module.id)
                    answers.append(answer)
                    maxGrade = answerSheet_module_maxGrade(module=answer)
                    maxModuleGrades.append({'id':module.id,'max':maxGrade})
                    moduleGrades.append({'id':module.id,'grade':0})

            answerSheet.answers = {'answers': answers}
            answerSheet.maxModuleGrade = {'maxModuleGrade': maxModuleGrades}
            answerSheet.moduleGrade = {'moduleGrade': moduleGrades}
            answerSheet.grade = 0
            answerSheet.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return answerSheet


@transaction.atomic
def answerSheet_update(*, answers, user):
    try:
        moduleId = answers['id']
        module = Module.objects.get(pk=moduleId)
        now = timezone.now()
        if module.endDate < now:
            return {'error': 'Este modulo ya finalizó.'}
        laboratory = module.laboratory
        schedule = laboratory.schedule
        userSchedules = schedule.userSchedules.filter(main=False)
        labId = laboratory.id
        scheduleId = schedule.id
        '''
        Ya que esto al momento de crearse solo sera hecho por el alumno,
        buscaremos el userSchedule con rol alumno 
        '''

        userSchedule = userSchedules.filter(schedule_id=scheduleId,roleUser__user__user_id=user.id).first()
        if userSchedule is None:
            return {'error':'Este usuario no existe en el Horario.'}

        answerSheet = AnswerSheet.objects.get(laboratory_id=labId,userSchedule_id=userSchedule.id)
        data = answerSheet.answers['answers']
        dataUpdated = [answers if(('id' in item) and (item['id'] == moduleId)) else item for item in data]

        answerSheet.answers['answers'] = dataUpdated
        answerSheet.save()
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio o usuario no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return answerSheet


@transaction.atomic
def answerSheet_delete(*,labId,userScheduleId):
    try:
        with transaction.atomic():
            answerSheet = AnswerSheet.objects.get(laboratory_id=labId,
                                                  userSchedule_id=userScheduleId)
            answerSheet.deleted = False
            answerSheet.save()

    except ObjectDoesNotExist:
        return {'error':'Esta hoja de respuesta no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return answerSheet


@transaction.atomic
def answerSheet_grading(*, answers, user,idAS):
    try:
        answerSheet = AnswerSheet.objects.get(pk=idAS)
        moduleId = answers['id']
        data = answerSheet.answers['answers']
        moduleGrade = 0
        for item in data:
            if ('id' in item) and (item['id'] == moduleId):
                moduleGrade = 0
                for (page,pageA) in zip(item['pages'], answers['pages']):
                    for (section, sectionA) in zip(page['sections'], pageA['sections']):
                        for (element, elementA) in zip(section['elements'],sectionA['elements']):
                            if elementA['elementType'] in ['text-question','checkbox-question'] and \
                                    elementA['scoreObtained'] is not None:
                                element['scoreObtained'] = float(elementA['scoreObtained'])
                                moduleGrade += elementA['scoreObtained']

        moduleGrades = answerSheet.moduleGrade['moduleGrade']
        maxModuleGrades = answerSheet.maxModuleGrade['maxModuleGrade']
        total = 0
        for item in moduleGrades:
            if item['id'] == moduleId:
                item['grade'] = moduleGrade
            total += float(item['grade'])

        maxim = 0
        for item in maxModuleGrades:
            maxim += float(item['max'])

        answerSheet.moduleGrade['moduleGrade'] = moduleGrades
        answerSheet.ratedBy = user.first_name + ' ' + user.last_name
        if maxim == 0:
            answerSheet.grade = 0
        else:
            answerSheet.grade = round(total*20/maxim)
        answerSheet.state = "Corregido"
        answerSheet.save()

    except ObjectDoesNotExist:
        return {'error':'Esta hoja de respuesta no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return answerSheet


def element_grade_cb(*,element,options,tries):
    data = []
    element['options']['options'] = options
    if element['numTry'] == 0:
        data.append('No tiene mas intentos permitidos')
        data.append(0)
        data.append(False)
        return element,data

    comment = ''
    for real in element['options']['options']:
        if (real['correct'] is True and real['selected'] is False) or (real['correct'] is False and real['selected'] is True):
            if int(element['numTry']) == tries:
                element['scoreObtained'] = float(element['maxScore']) - float(element['discount'])
            else:
                element['scoreObtained'] = float(element['scoreObtained']) - float(element['discount'])
            element['numTry'] = int(element['numTry']) - 1
            if 'commentIncorrect' in real:
                comment = real['commentIncorrect']
            data.append(comment)
            data.append(element['numTry'])
            data.append(False)
            return element, data
    if int(element['numTry']) == tries:
        element['scoreObtained'] = float(element['maxScore'])
    element['numTry'] = 0
    data.append(comment)
    data.append(element['numTry'])
    data.append(True)
    return element,data


@transaction.atomic
def answerSheet_grading_cb(*,idAS,idQuestion,options):
    try:
        with transaction.atomic():
            answerSheet = AnswerSheet.objects.get(pk=idAS)
            answers = answerSheet.answers['answers']
            idModule, idPage, idSection = question_get_module(id=idQuestion)
            tries = Checkbox.objects.get(pk=idQuestion).numTry
            data = ['',0,False]
            for module in answers:
                if module['id'] == idModule:
                    for page in module['pages']:
                        if page['id'] == idPage:
                            for section in page['sections']:
                                if section['id'] == idSection:
                                    for element in section['elements']:
                                        if element['id'] == idQuestion:
                                            element, data = element_grade_cb(element=element,options=options,
                                                                             tries=tries)
                                            break

            answerSheet.answers['answers'] = answers
            answerSheet.save()
    except ObjectDoesNotExist:
        return {'error':'Esta hoja de respuesta no existe.'}
    else:
        return answerSheet,data


@transaction.atomic
def answerSheet_delete_cascade(*,userScheduleId):
    try:
        with transaction.atomic():
            answerSheets = AnswerSheet.objects.filter(userSchedule_id=userScheduleId)
            for anSheet in answerSheets:
                anSheet.deleted = True
                anSheet.save()
    except ObjectDoesNotExist:
        return {'error':'Esta hoja de respuesta no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return