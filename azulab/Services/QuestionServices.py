from ..models.Question import Question
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.models.Text import Text
from azulab.models.Checkbox import Checkbox


@transaction.atomic
def question_create(*,number,statement,maxScore,scoreObtained):
    try:
        with transaction.atomic():
            if not isinstance(maxScore, float):
                return {'error':'El puntaje máximo debe ser decimal.'}
            if not isinstance(number, int):
                return {'error':'El número de pregunta debe ser entero.'}
            if not isinstance(scoreObtained, float):
                return {'error':'El puntaje máximo debe ser decimal.'}


            obj,created = Question.objects.get_or_create(number=number,statement=statement,maxScore=maxScore,scoreObtained=scoreObtained)
            if not created:
                obj.deleted = False
                obj.save()

    except ValidationError as e:
        return e.message_dict
    else:
        return obj

@transaction.atomic
def question_update(*,id,number,statement,maxScore,scoreObtained):
    if not isinstance(maxScore, float):
        return {'error': 'El puntaje máximo debe ser decimal.'}
    if not isinstance(number, int):
        return {'error': 'El número de pregunta debe ser entero.'}
    if not isinstance(scoreObtained, float):
        return {'error': 'El puntaje máximo debe ser decimal.'}
    try:
        with transaction.atomic():
            obj = Question.objects.get(pk=id)
            obj.number = number
            obj.statement = statement
            obj.maxScore = maxScore
            obj.scoreObtained = scoreObtained
            obj.save()
    except ObjectDoesNotExist:
        return {'error':'Esta pregunta no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return obj

@transaction.atomic
def question_delete(*,id):
    try:
        with transaction.atomic():
            obj = Question.objects.get(pk=id)
            obj.deleted = True
            obj.save()
    except ObjectDoesNotExist as e:
        return {'error':'Esta pregunta no existe.'}
    else:
        return obj

@transaction.atomic()
def question_create_template(*,id,idSection):
    try:
        with transaction.atomic():
            questionNew = Question.objects.get(pk=id)
            questionNew.id = None
            questionNew.section_id = idSection
            questionNew.save()

            try:
                response = Text.objects.get(pk=id)
            except ObjectDoesNotExist:
                try:
                    response = Checkbox.objects.get(pk=id)
                except ObjectDoesNotExist:
                    return {'error': 'Esta pregunta no existe.'}
                else:
                    pass
            else:
                pass

            response.question_ptr_id = questionNew.id
            response.save()

    except ObjectDoesNotExist:
        return {'error': 'Esta pregunta no existe.'}
    else:
        return questionNew
