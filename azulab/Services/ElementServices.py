from django.db import transaction
from azulab.Services.RichTextBoxServices import rtb_update, rtb_create
from azulab.Services.TextServices import text_update, text_create
from azulab.Services.CheckboxServices import checkbox_update, checkbox_create
from azulab.Services.DocumentServices import document_update,document_upload_replace
from django.core.exceptions import ValidationError, ObjectDoesNotExist


@transaction.atomic
def elements_update_display(*, idSection, elements):
    try:
        with transaction.atomic():
            for data in elements:
                idElement = data['id']
                ele_type = data['elementType']
                if idElement is not None:
                    if ele_type == 'text':
                        rtb_update(id=idElement, position=data['position'],
                                   sectionId=idSection, content=data['content'])
                    elif ele_type == 'text-question':
                        text_update(id=idElement, number=data['number'], statement=data['statement'],
                                    maxScore=data['maxScore'], optional=data['optional'],
                                    cantDocuments=data['cantDocuments'], type=data['type'],
                                    section=idSection, position=data['position'])
                    elif ele_type == 'checkbox-question':
                        checkbox_update(id=idElement, number=data['number'], statement=data['statement'],
                                        maxScore=data['maxScore'], optional=data['optional'],
                                        cantDocuments=data['cantDocuments'], options=data['options'],
                                        type=data['type'], numTry=data['numTry'],discount=data['discount'],
                                        section=idSection, position=data['position'])
                    elif ele_type == 'document':
                        document_update(id=idElement, position=data['position'], sectionId=idSection)
                else:
                    if ele_type == 'text':
                        rtb = rtb_create(position=data['position'], sectionId=idSection,
                                         content=data['content'])
                        data['id'] = rtb.id
                    elif ele_type == 'text-question':
                        text = text_create(number=data['number'], statement=data['statement'],
                                           maxScore=data['maxScore'], optional=data['optional'],
                                           cantDocuments=data['cantDocuments'], type=data['type'],
                                           section=idSection, position=data['position'])
                        data['id'] = text.id
                    elif ele_type == 'checkbox-question':
                        checkbox = checkbox_create(number=data['number'], statement=data['statement'],
                                                   maxScore=data['maxScore'], optional=data['optional'],
                                                   cantDocuments=data['cantDocuments'], options=data['options'],
                                                   type=data['type'], numTry=data['numTry'],discount=data['discount'],
                                                   section=idSection, position=data['position'])
                        data['id'] = checkbox.id
                    elif ele_type == 'document':
                        document = document_upload_replace(position=data['position'], sectionId=idSection)
                        data['id'] = document.id

    except ValidationError as e:
        return e.message_dict
    else:
        return elements
