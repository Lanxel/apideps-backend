from ..models.Section import Section
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.Services.ElementServices import elements_update_display
from azulab.Services.RichTextBoxServices import rtb_create_template
from azulab.Services.DocumentServices import document_create_template
from azulab.Services.TextServices import text_create_template
from azulab.Services.CheckboxServices import checkbox_create_template
from azulab.Selectors.TextSelector import text_list_section
from azulab.Selectors.CheckboxSelector import checkbox_list_section


@transaction.atomic
def section_create(*,title,position,pageId):
    try:
        with transaction.atomic():
            section, created = Section.objects.get_or_create(title=title,position=position,page_id=pageId)
            if not created:
                section.deleted = False
                section.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return section


def section_update(*,id,title=None,position,pageId):

    try:
        with transaction.atomic():
            section = Section.objects.get(pk=id)
            if title is not None:
                section.title = title
            section.position = position
            section.page_id = pageId
            section.save()
    except ObjectDoesNotExist:
        return {'error': 'Esta sección no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return section

def section_delete(*,id):
    try:
        with transaction.atomic():
            section = Section.objects.get(pk=id)
            section.deleted = True
            section.save()
    except ObjectDoesNotExist:
        return {'error':'Esta sección no existe.'}
    else:
        return section


@transaction.atomic
def section_update_display(*,idPage,sections):
    try:
        with transaction.atomic():
            for data in sections:
                idSection = data['id']
                if idSection is not None:
                    section = Section.objects.get(pk=idSection)
                    section.page_id = idPage
                    section.title = data['title']
                    section.position = data['position']
                else:
                    section = Section.objects.create(page_id=idPage, title=data['title'],
                                                     position=data['position'])
                    idSection = section.id
                    data['id'] = idSection
                data['elements'] = elements_update_display(idSection=idSection, elements=data['elements'])
                section.save()
    except ObjectDoesNotExist:
        return {'error':'Este módulo no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return sections


@transaction.atomic
def section_create_template(*,id,idPage):
    try:
        with transaction.atomic():
            section = Section.objects.get(pk=id)
            sectionNew = Section.objects.get(pk=id)
            sectionNew.id = None
            sectionNew.page_id = idPage
            sectionNew.save()

            for rtb in section.richtextbox_set.filter(deleted=False):
                response = rtb_create_template(id=rtb.id,idSection=sectionNew.id)
                if isinstance(response,dict):
                    return response

            for document in section.document_set.filter(deleted=False):
                response = document_create_template(id=document.id,idSection=sectionNew.id)
                if isinstance(response,dict):
                    return response

            for text in text_list_section(section=section.id):
                response = text_create_template(id=text.id,idSection=sectionNew.id)
                if isinstance(response,dict):
                    return response

            for cb in checkbox_list_section(section=section.id):
                response = checkbox_create_template(id=cb.id,idSection=sectionNew.id)
                if isinstance(response,dict):
                    return response

    except ObjectDoesNotExist:
        return {'error':'Esta sección no existe.'}
    else:
        return sectionNew