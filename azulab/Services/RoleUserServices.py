from azulab.models.RoleUser import RoleUser
from azulab.models.User import UserProfile
from azulab.Services.UserScheduleServices import userSchedule_create,userSchedule_delete_cascade
from django.db import transaction


@transaction.atomic
def roleUser_create(*, user, role):
    roleUser, created = RoleUser.objects.get_or_create(user=user, role=role)
    if role.name == "Super Administrador":
        user.user.is_superuser = True
        user.user.is_staff = True
        user.user.save()
    if not created:
        roleUser.deleted = False
        roleUser.save()
    return roleUser


@transaction.atomic
def roleUser_add_csv(*, code, first_name, last_name, schedule, au, email):
    from azulab.Services.UserServices import user_create_one_csv
    from azulab.Services.UserScheduleServices import userSchedule_verify_student

    userProfile = user_create_one_csv(code=code, first_name=first_name,
                                      last_name=last_name, email=email, type='Alumno')
    if not isinstance(userProfile, UserProfile):
        return userProfile
    roleUser = userProfile.roleUsers.get(role__name='Alumno')
    code,exists,thisSchedule,type = userSchedule_verify_student(idSchedule=schedule.id,idCourse=schedule.course_id,
                                                                idTerm=schedule.term_id,idUser=userProfile.id)
    if type is not None and type == "Docente":
        return {'error': f'El usuario con código {code} es docente del curso.'}
    elif exists and thisSchedule:
        return {'error': f'El alumno con código {code} ya pertenece a este horario.'}
    elif exists and not thisSchedule:
        return {'error': f'El alumno con código {code} ya pertenece a otro horario del curso.'}
    else:
        userSchedule_create(idRoleUser=roleUser.id, idSchedule=schedule.id)
        roleUser.academicUnits.add(au)
    return userProfile


@transaction.atomic
def roleUser_delete_roles(*, userId):
    roles = RoleUser.objects.filter(user_id=userId)
    for role in roles:
        if role.role.name == "Super Administrador":
            userProfile = UserProfile.objects.get(pk=userId)
            userProfile.user.is_staff = False
            userProfile.user.is_superuser = False
            userProfile.user.save()
        role.deleted = True
        role.save()
        userSchedule_delete_cascade(idRoleUser=role.id)
