from ..models.Laboratory import Laboratory
from ..models.Schedule import Schedule
from ..models.UserSchedule import UserSchedule
from django.db import transaction
from azulab.Services.AnswerSheetService import answerSheet_create, answerSheet_delete
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.Services.ModuleServices import module_create_template,module_delete


@transaction.atomic
def laboratory_create(*, schedule, number, title,description=None,url=None,
                      initDate=None, endDate=None, active=False,visible=False):

    try:
        with transaction.atomic():   
            
            lab,created = Laboratory.objects.get_or_create(number=number,
                                                           title=title,
                                                           schedule_id=schedule,
                                                           active=active,
                                                           visible=visible)
            if description is not None:
                lab.description = description
            if url is not None:
                lab.url = url
            if initDate is not None:
                lab.initDate = initDate
            if endDate is not None:
                lab.endDate = endDate
            if not created:
                lab.deleted = False
            lab.save()
            userSchedules = UserSchedule.objects.filter(schedule_id=schedule,main=False,deleted=False)
            for user in userSchedules:
                response = answerSheet_create(labId=lab.id,userScheduleId=user.id)
                if isinstance(response,dict):
                    return response
    except ValidationError as e:
        return e.message_dict
    return lab
    

@transaction.atomic
def laboratory_update(*,id, schedule=None, number=None, title=None,url=None,
                      description=None, initDate=None, endDate=None, active=False,visible=False):
    
    try:
        with transaction.atomic():
            lab = Laboratory.objects.get(pk=id)
            if schedule is not None:
                try:
                    _schedule = Schedule.objects.get(pk=schedule)
                    lab.schedule = _schedule
                except ObjectDoesNotExist:
                    return {'error':'Este horario no existe.'}
            if number is not None:
                lab.number = number
            if title is not None:
                lab.title = title
            if initDate is not None:
                lab.initDate = initDate
            if endDate is not None:
                lab.endDate = endDate
            if description is not None:
                lab.description = description
            if url is not None:
                lab.url = url
            lab.active = active
            lab.visible = visible
            lab.save()
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return lab


@transaction.atomic
def laboratory_delete(*, id):
    try:
        with transaction.atomic():
            lab = Laboratory.objects.get(pk=id)
            lab.deleted = True
            lab.save()
            for module in lab.modules.filter(deleted=False):
                module_delete(id=module.id)

            userSchedules = UserSchedule.objects.filter(schedule_id=lab.schedule_id,main=False,deleted=False)
            for user in userSchedules:
                response = answerSheet_delete(labId=lab.id, userScheduleId=user.id)
                if isinstance(response,dict):
                    pass
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio no existe.'}
    else:
        return lab


@transaction.atomic
def laboratory_template(*,idLab,idList):
    try:
        with transaction.atomic():
            lab = Laboratory.objects.get(pk=idLab)
            for idModule in idList:
                module = module_create_template(id=idModule,idLab=idLab)
                if isinstance(module,dict):
                    return module
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio no existe.'}
    else:
        return lab


@transaction.atomic
def laboratory_update_date(*,id):
    try:
        lab = Laboratory.objects.get(pk=id)
        modules = lab.modules.filter(deleted=False).order_by('initDate')
        if modules.exists():
            lab.initDate = modules.first().initDate
            lab.endDate = modules.last().endDate
            lab.save()
    except ObjectDoesNotExist:
        return {'error':'Este laboratorio no existe.'}
    else:
        return lab