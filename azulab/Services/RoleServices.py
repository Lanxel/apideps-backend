from ..models.Role import Role
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist


@transaction.atomic
def role_create(*, name):
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            role, created = Role.objects.get_or_create(name=name)
            if not created:
                role.deleted = False
                role.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return role


@transaction.atomic
def role_update(*, id, name):
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            role = Role.objects.get(pk=id)
            role.name = name
            role.save()
    except ObjectDoesNotExist:
        return {'error': 'Este rol no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return role


@transaction.atomic
def role_delete(*, id):
    try:
        with transaction.atomic():
            role = Role.objects.get(pk=id)
            role.deleted = True
            role.save()
    except ObjectDoesNotExist:
        return {'error': 'Este rol no existe.'}
    else:
        return role
