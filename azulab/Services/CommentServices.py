from ..models.Comment import Comment
from ..models.Question import Question
from ..models.UserSchedule import UserSchedule
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.utils import timezone


@transaction.atomic
def comment_create(*,content,question,idAnswerSheet,user):
    #nunca va a pasar esto pero bueno xd
    try:
        question = Question.objects.get(pk=question)
    except ObjectDoesNotExist:
        return {'error':'Esta pregunta no existe.'}

    #verificamos si el usuario pertenece al horario
    schedule = question.section.page.module.laboratory.schedule
    userS = UserSchedule.objects.filter(roleUser__user_id=user.id,schedule_id=schedule.id).first()
    if userS is None:
        return {'error':'Este usuario no pertenece al horario.'}

    try:
        with transaction.atomic():
            name = user.first_name + ' ' + user.last_name
            comment = Comment.objects.create(date=timezone.now(),
                                         content=content,
                                         question_id=question.id,
                                         answerSheet_id=idAnswerSheet,
                                         user=name)
    except ValidationError as e:
        return e.message_dict
    else:
        return comment

@transaction.atomic
def comment_update(*,id,content=None):
    try:
        with transaction.atomic():
            obj = Comment.objects.get(pk=id)
            if content is not None:
                obj.content = content
            obj.save()
    except ObjectDoesNotExist:
        return {'error':'Este comentario no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return obj

@transaction.atomic
def comment_delete(*,id):
    try:
        with transaction.atomic():
            obj = Comment.objects.get(pk=id)
            obj.deleted = True
            obj.save()
    except ObjectDoesNotExist:
        return {'error':'Este comentario no existe.'}
    else:
        return obj
