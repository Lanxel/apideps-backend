from ..models.Course import Course
from azulab.models.User import UserProfile
import azulab.Selectors.AcademicUnitSelector as auSel
import azulab.Selectors.ScheduleSelector as scheduleSel
from azulab.Services.ScheduleServices import schedule_delete
from azulab.Services.RoleUserServices import roleUser_add_csv
from ..models.AcademicUnit import AcademicUnit
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
import openpyxl


@transaction.atomic
def course_create(*, code, name, credits=None, academicUnit):
    code = ' '.join(code.split()).upper()
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            if not code.isalnum():
                return {'error': 'El código debe ser una cadena alfanumérica.'}
            if credits is not None and credits <= 0:
                return {'error': 'El valor de créditos debe ser positivo.'}
            try:
                au = AcademicUnit.objects.get(pk=academicUnit)
            except ObjectDoesNotExist:
                return {'error': 'Esta Unidad Académica no existe.'}
            course, created = Course.objects.get_or_create(code=code, name=name, credits=credits, academicUnit=au)
            if not course.deleted and not created:
                return {'error': 'Este curso ya existe'}
            if not created:
                course.deleted = False
                course.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return course


@transaction.atomic
def course_update(*, id, code=None, name=None, credits=None, academicunit):
    code = ' '.join(code.split()).upper()
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            if not code.isalnum():
                return {'error': 'El código debe ser una cadena alfanumérica.'}
            if credits <= 0:
                return {'error': 'El valor de créditos debe ser positivo.'}
            course = Course.objects.get(pk=id)
            try:
                au = AcademicUnit.objects.get(pk=academicunit)
            except ObjectDoesNotExist:
                return {'error': 'Esta Unidad Académica no existe.'}
            if code is not None:
                course.code = code
            if name is not None:
                course.name = name
            if credits is not None:
                course.credits = credits
            course.academicUnit_id = academicunit
            course.save()
    except ObjectDoesNotExist:
        return {'error': 'Este curso no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return course


@transaction.atomic
def course_delete(*, id):
    try:
        with transaction.atomic():
            course = Course.objects.get(pk=id)
            course.deleted = True
            course.save()
            for schedule in course.schedules.filter(deleted=False):
                schedule_delete(id=schedule.id)
    except ObjectDoesNotExist:
        return {'error': 'Este curso no existe.'}
    else:
        return course


def course_add_from_csv(*, courseId, termId, file):
    errors = []
    wb = openpyxl.load_workbook(file)
    worksheet = wb.active
    columns = worksheet[7]
    column_data = []
    for cell in columns:
        column_data.append(cell.value)
    if ((str(column_data[0]) != 'Alumno') or
            (str(column_data[1]) != 'Nombre') or
            (str(column_data[2]) != 'Horario') or
            (str(column_data[3]) != 'Especialidad') or
            (str(column_data[4]) != 'E-mail')):
        return {'error': 'Formato no aceptado. Por favor utilice el formato dado por la universidad'}
    for row in worksheet.iter_rows(min_row=8, max_row=worksheet.max_row):
        row_data = []
        for cell in row:
            row_data.append(cell.value)

        code = str(row_data[0])
        alumno = str(row_data[1])
        first_name = alumno.split(',')[1]
        last_name = alumno.split(',')[0]
        number = str(row_data[2])
        auName = row_data[3]
        email = row_data[4].split(',')[0]
        filedata = {'error': f'No se pudo agregar al usuario con código {code}.'}
        try:
            schedule = scheduleSel.schedule_by_name_course(courseId=courseId, number=number, termId=termId)
        except ObjectDoesNotExist:
            return {'error': 'Este curso no existe.'}

        try:
            academicUnit = auSel.academicunit_read_by_name(name=auName)
        except ObjectDoesNotExist:
            return {'error': 'Esta Unidad Académica no existe.'}

        userProfile = roleUser_add_csv(code=code, first_name=first_name, last_name=last_name,
                                       schedule=schedule, au=academicUnit, email=email)
        if not isinstance(userProfile, UserProfile):
            if 'error' in userProfile:
                errors.append(userProfile)
            else:
                errors.append(filedata)
    return errors


@transaction.atomic
def course_delete_multiple(*, idlist):
    errors = {}
    for id in idlist:
        response = course_delete(id=id)
        if not isinstance(response, Course):
            errors[id] = response
    return errors
