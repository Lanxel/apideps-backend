from azulab.models.UserSchedule import UserSchedule
from azulab.Services.AnswerSheetService import answerSheet_create,answerSheet_delete_cascade
from azulab.models.Schedule import Schedule
from django.db import transaction
from django.core.exceptions import ObjectDoesNotExist, ValidationError


@transaction.atomic
def userSchedule_create(*,idSchedule,idRoleUser):
    try:
        userSchedule, crated = UserSchedule.objects.get_or_create(roleUser_id=idRoleUser,schedule_id=idSchedule)
        if not crated:
            userSchedule.deleted = False
        if userSchedule.roleUser.role_id == 2:
            userSchedule.main = True
        userSchedule.save()
        if userSchedule.roleUser.role_id == 1:
            schedule = Schedule.objects.get(pk=idSchedule)
            labs = schedule.laboratories.filter(deleted=False)
            for lab in labs:
                answerSheet = answerSheet_create(labId=lab.id,userScheduleId=userSchedule.id)
                if isinstance(answerSheet,dict):
                    return answerSheet

    except ValidationError as e:
        return e.message_dict
    else:
        return userSchedule


@transaction.atomic
def userSchedule_delete(*,idSchedule,idRoleUser):
    try:
        with transaction.atomic():
            us = UserSchedule.objects.get(schedule_id=idSchedule,roleUser_id=idRoleUser)
            us.deleted = True
            us.save()
    except ObjectDoesNotExist:
        return {'error':'Este horario o usuario no existe.'}
    else:
        return True


def userSchedule_teachers(*,idSchedule):
    try:
        with transaction.atomic():
            us = UserSchedule.objects.filter(schedule_id=idSchedule, main=True,deleted=False).order_by('roleUser__user__code')
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist
    else:
        return us


def userSchedule_students(*,idSchedule):
    try:
        with transaction.atomic():
            us = UserSchedule.objects.filter(schedule_id=idSchedule,main=False,deleted=False).order_by('roleUser__user__code')
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist
    else:
        return us


def userSchedule_verify_student(*,idSchedule,idCourse,idTerm,idUser):
    userSchedules = UserSchedule.objects.filter(schedule__course_id=idCourse,
                                                schedule__term_id=idTerm,
                                                roleUser__user_id=idUser,
                                                deleted=False)
    if userSchedules.exists():
        userSchedule = userSchedules.first()
        code = userSchedule.roleUser.user.code
        this = userSchedule.schedule_id == idSchedule
        type = userSchedule.roleUser.role.name
        return code,True,this,type
    return None,False,False,None


@transaction.atomic
def userSchedule_delete_cascade(idRoleUser=None,idSchedule=None):
    try:
        with transaction.atomic():
            if idRoleUser is not None:
                uSchedules = UserSchedule.objects.filter(roleUser_id=idRoleUser)
            if idSchedule is not None:
                uSchedules = UserSchedule.objects.filter(schedule_id=idSchedule)
            for us in uSchedules:
                us.deleted = True
                us.save()
                if not us.main:
                    answerSheet_delete_cascade(userScheduleId=us.id)
    except ObjectDoesNotExist:
        return {'error':'Este horario o usuario no existe.'}
    else:
        return
