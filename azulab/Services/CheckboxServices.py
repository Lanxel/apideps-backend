from ..models.Checkbox import Checkbox
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist

@transaction.atomic
def checkbox_create(*, number, statement, maxScore, scoreObtained=0, optional=False, cantDocuments = None,
                    options, type,numTry,discount=1, section, position):
    try:
        with transaction.atomic():
            obj,created = Checkbox.objects.get_or_create(number=number, statement=statement,
                                                         maxScore=maxScore, scoreObtained=scoreObtained,
                                                         optional=optional, cantDocuments=cantDocuments,
                                                         options=options, type=type, numTry=numTry,discount=discount,
                                                         section_id=section, position=position)
            if not created:
                obj.deleted = False
                obj.save()

    except ValidationError as e:
        return e.message_dict
    else:
        return obj

@transaction.atomic
def checkbox_update(*,id,number=None,statement=None,maxScore=None,scoreObtained=None,optional=False,cantDocuments=None,
                    options=None, type=None,numTry=None, discount=None, section=None, position=None):
    try:
        with transaction.atomic():
            obj = Checkbox.objects.get(pk=id)
            if number is not None:
                obj.number = number
            if statement is not None:
                obj.statement = statement
            if maxScore is not None:
                obj.maxScore = maxScore
            if scoreObtained is not None:
                obj.scoreObtained = scoreObtained
            if cantDocuments is not None:
                obj.cantDocuments = cantDocuments
            if options is not None:
                obj.options = options
            if type is not None:
                obj.type = type
            if numTry is not None:
                obj.numTry = numTry
            if discount is not None:
                obj.discount = discount
            if section is not None:
                obj.section_id = section
            if position is not None:
                obj.position = position
            obj.optional = optional
            obj.save()
    except ObjectDoesNotExist:
        return {'error':'Este Checkbox no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return obj

@transaction.atomic
def checkbox_delete(*,id):
    try:
        with transaction.atomic():
            obj = Checkbox.objects.get(pk=id)
            obj.delete()
    except ObjectDoesNotExist:
        return {'error':'Este Checkbox no existe.'}
    else:
        return True


@transaction.atomic
def checkbox_create_template(*,id, idSection):
    try:
        with transaction.atomic():
            cbNew = Checkbox.objects.get(pk=id)
            cbNew.id = None
            cbNew.question_ptr_id=None
            cbNew.section_id = idSection
            cbNew.save()
    except ObjectDoesNotExist:
        return {'error': 'Este Checkbox no existe.'}
    else:
        return cbNew
