from ..models.Term import Term
from azulab.Services.ScheduleServices import schedule_delete
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist

@transaction.atomic
def term_create(*, term, initDate=None, endDate=None):
    term = ' '.join(term.split())
    try:
        with transaction.atomic():
            term, created = Term.objects.get_or_create(term=term)
            if not term.deleted and not created:
                return {'error':'Este ciclo ya existe'}
            if not created:
                term.deleted = False
            if initDate is not None:
                term.initDate = initDate
            if endDate is not None:
                term.endDate = endDate
            term.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return term

@transaction.atomic
def term_update(*, id, term=None,initDate=None,endDate=None):
    term = ' '.join(term.split())
    try:
        with transaction.atomic():
            termObj = Term.objects.get(pk=id)
            if term is not None:
                termObj.term = term
            if initDate is not None:
                termObj.initDate = initDate
            if endDate is not None:
                termObj.endDate = endDate
            termObj.save()
    except ObjectDoesNotExist:
        return {'error': 'Este ciclo no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return termObj

@transaction.atomic
def term_delete(*, id):
    try:
        with transaction.atomic():
            term = Term.objects.get(pk=id)
            term.deleted = True
            term.save()
            for schedule in term.schedules.filter(deleted=False):
                schedule_delete(id=schedule.id)
    except ObjectDoesNotExist as e:
        return {'error':'Este ciclo no existe.'}
    else:
        return term


