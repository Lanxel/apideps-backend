from ..models.Schedule import Schedule
from ..models.Course import Course
from ..models.Term import Term
from ..models.RoleUser import RoleUser
from azulab.Services.LaboratoryServices import laboratory_delete
from ..Services.UserScheduleServices import userSchedule_delete,userSchedule_create, \
    userSchedule_teachers,userSchedule_verify_student,userSchedule_delete_cascade
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist

@transaction.atomic
def schedule_create(*, course, number, term):
    number = ' '.join(number.split()).upper()

    try:
        co = Course.objects.get(pk=course)        
    except ObjectDoesNotExist:
        return {'error':'Este curso no existe.'}
    try:
        te = Term.objects.get(pk=term)       
    except ObjectDoesNotExist:
        return {'error':'Este ciclo no existe.'}

    try:
        with transaction.atomic():
            schedules = Schedule.objects.filter(number=number,course=co,term=te,deleted=False)
            if schedules.exists():
                return {'error': 'Este horario ya existe'}
            schedule = Schedule.objects.create(number=number, course=co, term=te)
    except ValidationError as e:
        return e.message_dict
    else:
        return schedule  
    
@transaction.atomic
def schedule_update(*, id,course=None, number=None, term=None):
    number = ' '.join(number.split()).upper()
    co = None
    tr =None
    if course != None:
        try:
            co = Course.objects.get(pk=course)        
        except ObjectDoesNotExist:
            return {'error':'Este curso no existe.'}

    if term != None:
        try:
            tr = Term.objects.get(pk=term)        
        except ObjectDoesNotExist:
            return {'error':'Este ciclo no existe.'}

    try:
        with transaction.atomic():
            schedule = Schedule.objects.get(pk=id)
            if(number != None):
                schNew = Schedule.objects.filter(number=number, course_id=course, term_id=term,deleted=False)
                if schNew.exists() and schNew.first().id != id:
                    return {'error': f'El horario {number} ya existe'}
                else:
                    schedule.number = number
            if(co != None):
                schedule.course = co
            if(tr != None):
                schedule.term = tr
            schedule.save()

    except ObjectDoesNotExist:
        return {'error':'Este horario no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return schedule

@transaction.atomic
def schedule_delete(*,id):
    try:
        with transaction.atomic():
            schedule = Schedule.objects.get(pk=id)
            schedule.deleted = True
            schedule.save()
            for lab in schedule.laboratories.filter(deleted=False):
                laboratory_delete(id=lab.id)
            userSchedule_delete_cascade(idSchedule=schedule.id)
    except ObjectDoesNotExist:
        return {'error':'Este horario no existe.'}
    else:
        return schedule


@transaction.atomic
def schedule_delete_multiple(*,idlist):
    errors = []
    for id in idlist:
        response = schedule_delete(id=id)
        if not isinstance(response,Schedule):
            response['error'] = f'El horario con id {id} no existe'
            errors.append(response)
    return errors


@transaction.atomic
def schedule_delete_multiple_users(*,idSchedule,idList):
    errors = []
    for id in idList:
        roleUser = RoleUser.objects.get(user_id=id,role_id=1)
        response = userSchedule_delete(idSchedule=idSchedule,idRoleUser=roleUser.id)
        if isinstance(response,dict):
            response['error'] = f'El horario con id {idSchedule} o el usuario con id {id} no existe.'
            errors.append(response)
    return errors

@transaction.atomic
def schedule_update_teacher(*, idSchedule, idList):
    try:
        with transaction.atomic():
            errors = []
            schedule = Schedule.objects.get(pk=idSchedule)
            teachers = userSchedule_teachers(idSchedule=idSchedule)
            for userSch in teachers:
                userSch.main = False
                userSch.deleted = True
                userSch.save()

            for id in idList:
                '''
                Aca se pretende verificar si el docente ya esta en otro horarios
                del mismo curso.
                '''
                code, exists, thisSchedule, type = userSchedule_verify_student(idUser=id,
                                                                               idSchedule=idSchedule,
                                                                               idTerm=schedule.term_id,
                                                                               idCourse=schedule.course_id)
                if type is not None and type == "Alumno":
                    errors.append({'error': f'El usuario con código {code} es alumno del curso.'})
                # elif exists and thisSchedule:
                #     errors.append({'error': f'El alumno con código {code} ya pertenece a este horario.'})
                # elif exists and not thisSchedule:
                #     errors.append({'error': f'El alumno con código {code} ya pertenece a otro horario del curso.'})
                else:
                    role,_ = RoleUser.objects.get_or_create(user_id=id, role_id=2)
                    userSchedule = userSchedule_create(idSchedule=idSchedule, idRoleUser=role.id)
    except ValidationError as e:
        return e.message_dict
    except ObjectDoesNotExist:
        return {'error':'Este usuario no esta registrado como "Docente"'}
    else:
        return errors


@transaction.atomic
def schedule_add_student(*,idSchedule,idList):
    try:
        with transaction.atomic():
            errors = []
            schedule = Schedule.objects.get(pk=idSchedule)
            for id in idList:
                '''
                Aca se pretende verificar si el alumno ya esta en otro horarios
                del mismo curso.
                '''
                code,exists,thisSchedule,type = userSchedule_verify_student(idUser=id,
                                                                            idSchedule=idSchedule,
                                                                            idTerm=schedule.term_id,
                                                                            idCourse=schedule.course_id)
                if type is not None and type == "Docente":
                    errors.append({'error':f'El usuario con código {code} es docente del curso.'})
                elif exists and thisSchedule:
                    errors.append({'error':f'El alumno con código {code} ya pertenece a este horario.'})
                elif exists and not thisSchedule:
                    errors.append({'error': f'El alumno con código {code} ya pertenece a otro horario del curso.'})
                else:
                    role,_ = RoleUser.objects.get_or_create(user_id=id, role_id=1)
                    userSchedule = userSchedule_create(idSchedule=idSchedule, idRoleUser=role.id)
    except ValidationError as e:
        return e.message_dict
    except ObjectDoesNotExist:
        return {'error':'Este usuario no esta registrado como "Alumno"'}
    else:
        return errors
