from ..models.User import UserProfile, User
from django.core.exceptions import ValidationError
import azulab.Selectors.RoleSelector as roleSel
import azulab.Selectors.RoleUserSelector as roleUserSel
import azulab.Services.RoleUserServices as roleUserSer
import azulab.Services.UserScheduleServices as userSchSer
import azulab.Selectors.AcademicUnitSelector as auSel
from rest_framework.authtoken.models import Token
from django.db import transaction,IntegrityError
from django.core.exceptions import ObjectDoesNotExist
import csv
import io


@transaction.atomic
def user_generate_token(*, userProfile):
    user = userProfile.user
    token,created = Token.objects.get_or_create(user=user)
    return token


@transaction.atomic
def user_create_one_csv(*, code, first_name, last_name, email, type):
    code = ''.join(code.split())
    first_name = ' '.join(first_name.split()).title()
    last_name = ' '.join(last_name.split()).title()

    role = roleSel.role_read_by_name(name=type)

    #Usamos un get or create de RoleUser
    userProfile = roleUserSel.roleUser_verify_by_code(code=code,idRole=role.id)

    #Si existe el user, pero sin el role
    with transaction.atomic():
        if userProfile is not None:
            roleUserSer.roleUser_create(user=userProfile,role=role)
            return userProfile

    try:
        with transaction.atomic():
            user,created = User.objects.get_or_create(username=code, first_name=first_name, last_name=last_name)
            user.set_unusable_password()
            if user.is_active and not created:
                return {'error':'Este usuario ya existe'}
            if not created:
                user.is_active = True
            user.email = email
            user.save()
            userProfile = UserProfile(user=user, code=code)
            userProfile.save()
            roleUserSer.roleUser_create(user=userProfile,role=role)
    except ValidationError as e:
        return e.message_dict
    except IntegrityError:
        return {'error': 'Por favor, verifique el código o e-mail.'}
    else:
        user_generate_token(userProfile=userProfile)
        return userProfile


@transaction.atomic
def user_create_from_csv(*,file,type):
    errors = []
    file = file.read().decode('UTF-8')
    io_string = io.StringIO(file)
    next(io_string)
    for row in csv.reader(io_string, delimiter=','):
        filedata = {'code':row[0], 'last_name':row[1], 'first_name':row[2], 'email':row[3]}
        result = user_create_one_csv(code=row[0], last_name=row[1], first_name=row[2], email=row[3], type=type)
        if not isinstance(result, UserProfile):
            errors.append(filedata)
    return errors


@transaction.atomic
def user_update(*,id,code,first_name,last_name,email,roles=None):
    code = ''.join(code.split())
    first_name = ' '.join(first_name.split()).title()
    last_name = ' '.join(last_name.split()).title()

    try:
        with transaction.atomic():
            userProfile = UserProfile.objects.get(pk=id)
            userProfile.user.first_name = first_name
            userProfile.user.last_name = last_name
            userProfile.user.email = email
            userProfile.user.username = code
            userProfile.code = code
            userProfile.user.save()
            userProfile.save()

            if roles is not None:
                #Se borran los anteriores roles que tenia#
                roleUserSer.roleUser_delete_roles(userId=userProfile.id)
                for item in roles:
                    role = roleSel.role_read(id=item['userType'])
                    roleUser = roleUserSer.roleUser_create(user=userProfile, role=role)

                    if role.name not in ["Super Administrador","Administrador"]:
                        idAu = item['academicUnit']
                        idSchedule = item['schedule']
                        au = auSel.academicunit_read(id=idAu)
                        roleUser.academicUnits.add(au)
                        roleUser.save()
                        userSchedule = userSchSer.userSchedule_create(idSchedule=idSchedule,
                                                                      idRoleUser=roleUser.id)
                    elif role.name == "Administrador":
                        idAu = item['academicUnit']
                        au = auSel.academicunit_read(id=idAu)
                        roleUser.academicUnits.add(au)
                        roleUser.save()

    except ObjectDoesNotExist:
        return {'error': 'Este usuario no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return userProfile


@transaction.atomic
def user_delete(*,id):
    try:
        with transaction.atomic():
            userProfile = UserProfile.objects.get(pk=id)
            userProfile.user.is_active = False
            userProfile.user.save()
            #borrar todos sus roles
            roleUserSer.roleUser_delete_roles(userId=id)
            userProfile.save()
    except ObjectDoesNotExist as e:
        return {'error':'Este usuario no existe.'}
    else:
        return userProfile


@transaction.atomic
def user_delete_multiple(*,idlist):
    errors = {}
    for id in idlist:
        response = user_delete(id=id)
        if not isinstance(response,UserProfile):
            errors[id] = response
    return errors


@transaction.atomic
def user_create(*, code, first_name, last_name, email,roles=None):
    code = ''.join(code.split())
    first_name = ' '.join(first_name.split()).title()
    last_name = ' '.join(last_name.split()).title()

    try:
        with transaction.atomic():
            user = User(username=code, first_name=first_name, last_name=last_name, email=email) #django
            user.set_unusable_password()
            user.save()
            userProfile = UserProfile(user=user, code=code)
            userProfile.save()

            if roles is not None:
                for item in roles:
                    role = roleSel.role_read(id=item['userType'])
                    roleUser = roleUserSer.roleUser_create(user=userProfile, role=role)

                    if role.name not in ["Super Administrador", "Administrador"]:
                        idAu = item['academicUnit']
                        idSchedule = item['schedule']
                        au = auSel.academicunit_read(id=idAu)
                        roleUser.academicUnits.add(au)
                        roleUser.save()
                        userSchedule = userSchSer.userSchedule_create(idSchedule=idSchedule,
                                                                      idRoleUser=roleUser.id)
                    elif role.name == "Administrador":
                        idAu = item['academicUnit']
                        au = auSel.academicunit_read(id=idAu)
                        roleUser.academicUnits.add(au)
                        roleUser.save()

    except ValidationError as e:
        return e.message_dict
    except IntegrityError:
        return {'error': 'Por favor, verifique el código o e-mail.'}
    else:
        user_generate_token(userProfile=userProfile)
        return userProfile
