from ..models.AcademicUnit import AcademicUnit
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.Services.CourseServices import course_delete

@transaction.atomic
def academicunit_create(*,code,name):
    code = ' '.join(code.split()).upper()
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            if not code.isalnum():
                return {'error':'El código debe ser una cadena alfanumérica.'}
            au,created = AcademicUnit.objects.get_or_create(code=code,name=name)
            if not au.deleted and not created:
                return {'error':'Esta Unidad Académica ya existe'}
            if not created:
                au.deleted = False
                au.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return au

@transaction.atomic
def academicunit_update(*,id,code,name):
    code = ' '.join(code.split()).upper()
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            au = AcademicUnit.objects.get(pk=id)
            au.code = code
            au.name = name
            au.save()
    except ObjectDoesNotExist:
        return {'error':'Esta Unidad Académica no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return au

@transaction.atomic
def academicunit_delete(*,id):
    try:
        with transaction.atomic():
            au = AcademicUnit.objects.get(pk=id)
            au.deleted = True
            au.save()
            for course in au.courses.filter(deleted=False):
                course_delete(id=course.id)
    except ObjectDoesNotExist:
        return {'error':'Esta Unidad Académica no existe.'}
    else:
        return au
