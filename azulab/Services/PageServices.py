from ..models.Page import Page
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.Services.SectionServices import section_update_display, section_create_template


@transaction.atomic
def page_create(*,number,moduleId):
    try:
        with transaction.atomic():
            page, created = Page.objects.get_or_create(number=number,module_id=moduleId)
            if not created:
                page.deleted = False
                page.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return page


def page_update(*, id, number=None):

    try:
        with transaction.atomic():
            page = Page.objects.get(pk=id)
            if number is not None:
                page.number = number
            page.save()
    except ObjectDoesNotExist:
        return {'error': 'Esta página no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return page

def page_delete(*,id):
    try:
        with transaction.atomic():
            page = Page.objects.get(pk=id)
            page.deleted = True
            page.save()
    except ObjectDoesNotExist:
        return {'error':'Esta página no existe.'}
    else:
        return page


@transaction.atomic
def page_update_display(*,idModule,pages):
    try:
        with transaction.atomic():
            for data in pages:
                idPage = data['id']
                if idPage is not None:
                    page = Page.objects.get(pk=idPage)
                    page.module_id = idModule
                    page.number = data['number']
                else:
                    page = Page(number=data['number'])
                    page.module_id = idModule
                    page.save()
                    idPage = page.id
                    data['id'] = idPage
                data['sections'] = section_update_display(idPage=idPage, sections=data['sections'])
                page.save()
    except ObjectDoesNotExist:
        return {'error':'Esta página no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return pages

@transaction.atomic
def page_create_template(*,id,idModule):
    try:
        with transaction.atomic():
            page = Page.objects.get(pk=id)
            pageNew = Page.objects.get(pk=id)
            pageNew.id = None
            pageNew.module_id = idModule
            pageNew.save()
            for section in page.sections.filter(deleted=False):
                section = section_create_template(id=section.id,idPage=pageNew.id)
                if isinstance(section,dict):
                    return section
    except ObjectDoesNotExist:
        return {'error':'Esta página no existe.'}
    else:
        return pageNew