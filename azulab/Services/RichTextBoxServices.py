from azulab.models.RichTextBox import RichTextBox
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist


@transaction.atomic
def rtb_create(*,position,sectionId,content):
    try:
        with transaction.atomic():
            rtb, created = RichTextBox.objects.get_or_create(position=position,
                                                             section_id=sectionId,
                                                             content=content)
            if not created:
                rtb.deleted = False
                rtb.save()
    except ValidationError as e:
        return e.message_dict
    else:
        return rtb


@transaction.atomic
def rtb_update(*,id,position,sectionId,content):
    try:
        with transaction.atomic():
            rtb= RichTextBox.objects.get(pk=id)
            rtb.position = position
            rtb.section_id = sectionId
            rtb.content = content
            rtb.save()

    except ObjectDoesNotExist:
        return {'error':'Este Rich Text Box no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return rtb


@transaction.atomic
def rtb_delete(*, id):
    try:
        with transaction.atomic():
            rtb = RichTextBox.objects.get(pk=id)
            rtb.delete()
    except ObjectDoesNotExist:
        return {'error': 'Este Rich Text Box no existe.'}
    else:
        return True


@transaction.atomic
def rtb_create_template(*, id, idSection):
    try:
        with transaction.atomic():
            rtbNew = RichTextBox.objects.get(pk=id)
            rtbNew.id = None
            rtbNew.section_id = idSection
            rtbNew.save()
    except ObjectDoesNotExist:
        return {'error': 'Este Rich Text Box no existe.'}
    else:
        return rtbNew