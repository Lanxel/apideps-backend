from ..models.Module import Module
from django.db import transaction
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from azulab.Services.PageServices import page_update_display,page_create_template
from azulab.Selectors.AnswerSheetSelector import answerSheet_get_from_lab,answerSheet_delete_module
from django.utils import timezone



@transaction.atomic
def module_create(*,name,initDate=None,endDate=None,labId):
    name = ' '.join(name.split()).title()
    try:
        with transaction.atomic():
            module, created = Module.objects.get_or_create(name=name,initDate=initDate,
                                                           endDate=endDate,laboratory_id=labId)
            if not created:
                module.deleted = False
                module.save()
            from azulab.Services.LaboratoryServices import laboratory_update_date
            response = laboratory_update_date(id=labId)
            if isinstance(response,dict):
                return response
    except ValidationError as e:
        return e.message_dict
    else:
        return module

@transaction.atomic
def module_update(*,id,name=None,initDate=None,endDate=None,active=False):

    try:
        with transaction.atomic():
            module = Module.objects.get(pk=id)
            if name is not None:
                name = ' '.join(name.split()).title()
                module.name = name
            if initDate is not None:
                module.initDate = initDate
            if endDate is not None:
                module.endDate = endDate
            module.active = active
            module.save()
            from azulab.Services.LaboratoryServices import laboratory_update_date
            response = laboratory_update_date(id=module.laboratory_id)
            if isinstance(response, dict):
                return response
    except ObjectDoesNotExist:
        return {'error': 'Este módulo no existe.'}
    except ValidationError as e:
        return e.message_dict
    else:
        return module

def module_delete(*,id):
    try:
        with transaction.atomic():
            module = Module.objects.get(pk=id)
            module.deleted = True
            module.save()
            answerSheet_delete_module(idModule=module.id,idLab=module.laboratory_id)
    except ObjectDoesNotExist:
        return {'error':'Este módulo no existe.'}
    else:
        return module

def module_max_score(*,data):
    result = 0
    for page in data:
        for section in page['sections']:
            for element in section['elements']:
                if element['elementType'] in ['text-question','checkbox-question']:
                    result += float(element['maxScore'])
    return result


def module_update_statements(*,actual,new):
    try:
        for pageA,pageN in zip(actual['pages'],new['pages']):
            for sectionA,sectionN in zip(pageA['sections'],pageN['sections']):
                for elementA,elementN in zip(sectionA['elements'],sectionN['elements']):
                    if elementA['elementType'] in ['text-question','checkbox-question']:
                        elementA['number'] = elementN['number']
                        elementA['statement'] = elementN['statement']
                        elementA['maxScore'] = float(elementN['maxScore'])
                    elif elementA['elementType'] == 'text':
                        elementA['content'] = elementN['content']
    except Exception:
        return {'error':'Solo se puede modificar los enunciados o puntajes de las preguntas'}
    else:
        return actual



@transaction.atomic
def module_update_display(*,data):
    try:
        with transaction.atomic():
            idModule = data['id']
            module = Module.objects.get(pk=idModule)
            data['pages'] = page_update_display(idModule=idModule,pages=data['pages'])

            #crear answer sheet para todos los alumnos del laboratorio
            labId = module.laboratory_id
            answerSheets = answerSheet_get_from_lab(labId=labId)
            maxScore = module_max_score(data=data['pages'])
            for answerSheet in answerSheets:
                answers = answerSheet.answers['answers']
                maxModuleGrade = answerSheet.maxModuleGrade['maxModuleGrade']
                moduleGrade = answerSheet.moduleGrade['moduleGrade']
                total = len(answers)
                if total == 0:
                    answers.append(data)
                    maxModuleGrade.append({'id':idModule,'max':maxScore})
                    moduleGrade.append({'id':idModule, 'grade': 0})
                else:
                    cont = 0
                    for item in answers:
                        if ('id' in item) and (item['id'] == idModule):
                            now = timezone.now()
                            if module.initDate < now:
                                update = module_update_statements(actual=item,new=data)
                                if 'error' in update:
                                    return update
                                answers[cont] = update
                            else:
                                answers[cont] = data
                            maxModuleGrade[cont] = {'id':idModule,'max':maxScore}
                            moduleGrade[cont] = {'id':idModule, 'grade': 0}
                            break
                        cont += 1
                    if cont == total:
                        answers.append(data)
                        maxModuleGrade.append({'id': idModule, 'max': maxScore})
                        moduleGrade.append({'id': idModule, 'grade': 0})
                answerSheet.answers['answers'] = answers
                answerSheet.maxModuleGrade['maxModuleGrade'] = maxModuleGrade
                answerSheet.moduleGrade['moduleGrade'] = moduleGrade
                answerSheet.save()
    except ObjectDoesNotExist:
        return {'error':'Este módulo no existe.'}
    else:
        return module

@transaction.atomic
def module_create_template(*,id,idLab):
    try:
        with transaction.atomic():
            module = Module.objects.get(pk=id)
            moduleNew = Module.objects.get(pk=id)
            moduleNew.id = None
            moduleNew.initDate = timezone.now()
            moduleNew.endDate = timezone.now() + timezone.timedelta(minutes=5)
            moduleNew.laboratory_id = idLab
            moduleNew.save()
            for page in module.pages.filter(deleted=False):
                page = page_create_template(id=page.id,idModule=moduleNew.id)
                if isinstance(page,dict):
                    return page
    except ObjectDoesNotExist:
        return {'error':'Este módulo no existe.'}
    else:
        return moduleNew