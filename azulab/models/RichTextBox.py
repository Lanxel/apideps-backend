from django.db import models
from django.conf import settings
from .Element import Element
from ckeditor_uploader.fields import RichTextUploadingField


class RichTextBox(Element):
    class Meta:
        app_label = 'azulab'
        db_table = 'RICHTEXTBOX'

    content = RichTextUploadingField()
