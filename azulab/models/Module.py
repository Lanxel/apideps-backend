from django.db import models
from django.conf import settings
from .Laboratory import Laboratory
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _



class Module(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'MODULE'

    name = models.CharField(max_length=50)
    initDate = models.DateTimeField(null=True, blank=True)
    endDate = models.DateTimeField(null=True, blank=True)
    active = models.BooleanField(default=False)
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE, related_name='modules')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'Module {self.name} - {self.laboratory}'

    def clean(self):
        if self.initDate is None and self.endDate is not None:
            raise ValidationError({'dates': _("Ingrese Fecha Inicial")})
        if self.initDate is not None and self.endDate is None:
            raise ValidationError({'dates': _("Ingrese Fecha Final")})
        if self.initDate is not None and self.endDate is not None:
            if self.endDate < self.initDate:
                raise ValidationError({'dates': _("Fecha Inicial debe ser menor que Fecha Final")})

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

