from django.db import models
from django.conf import settings
from .Question import Question


class Checkbox(Question):
    class Meta:
        app_label = 'azulab'
        db_table = 'CHECKBOX'

    options = models.JSONField()
    type = models.CharField(max_length=15)
    numTry = models.IntegerField(default=1, null=True, blank=True)
    discount = models.FloatField(default=1,null=True,blank=True)

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)


