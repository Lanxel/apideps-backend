from django.db import models
from .RoleUser import RoleUser
from .Schedule import Schedule
from .Laboratory import Laboratory


class UserSchedule(models.Model):

    roleUser = models.ForeignKey(RoleUser, on_delete=models.CASCADE, related_name='userSchedules')
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE, related_name='userSchedules')
    laboratory = models.ManyToManyField(Laboratory, related_name='userSchedules', through='AnswerSheet')
    last_login = models.DateTimeField(null=True, blank=True)
    main = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)
    
    def __str__(self):
        return f'{self.roleUser.user.user.first_name} {self.roleUser.user.user.last_name}- {self.schedule}'

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
