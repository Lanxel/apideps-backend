from django.db import models
from django.conf import settings
from .Question import Question
from ckeditor_uploader.fields import RichTextUploadingField


class Text(Question):
    class Meta:
        app_label = 'azulab'
        db_table = 'TEXT'

    type = models.CharField(max_length=5)
    answer = RichTextUploadingField(null=True, blank=True)