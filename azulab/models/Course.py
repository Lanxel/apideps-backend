from django.db  import models
from .AcademicUnit import AcademicUnit
from django.conf import settings


class Course(models.Model):

	class Meta:
		app_label = 'azulab'
		db_table = 'COURSE'

	code = models.CharField(max_length=8, unique=True)
	name = models.CharField(max_length=60)
	credits = models.FloatField(null=True, blank=True)
	academicUnit = models.ForeignKey(AcademicUnit, on_delete=models.CASCADE, related_name='courses')
	deleted = models.BooleanField(default=False)

	def __str__(self):
		return self.name


	def save(self, *args, **kwargs):
		self.full_clean()
		return super().save(*args, **kwargs)