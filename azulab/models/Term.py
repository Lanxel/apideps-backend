from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.conf import settings


class Term(models.Model):
	class Meta:
		app_label = 'azulab'
		db_table = 'TERM'

	term = models.CharField(max_length=6, unique=True)
	initDate = models.DateField(blank=True,null=True)
	endDate = models.DateField(blank=True,null=True)
	deleted = models.BooleanField(default=False)

	def __str__(self):
		return self.term

	def clean(self):
		if self.initDate is None and self.endDate is not None:
			raise ValidationError({'dates': _("Ingrese Fecha Inicial")})
		if self.initDate is not None and self.endDate is None:
			raise ValidationError({'dates': _("Ingrese Fecha Final")})
		if self.initDate is not None and self.endDate is not None:
			if self.endDate < self.initDate:
				raise ValidationError({'dates': _("Fecha Inicial debe ser menor que Fecha Final")})

	def save(self, *args, **kwargs):
		self.full_clean()
		return super().save(*args, **kwargs)

