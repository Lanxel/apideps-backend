from django.db import models
from django.conf import settings
from azulab.models.Question import Question
from azulab.models.AnswerSheet import AnswerSheet
import datetime as dt

def answerSheet_directory_path(instance, filename):
    now = dt.datetime.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    day = now.strftime("%d")
    return 'AnswerSheet_{0}/{1}/{2}/{3}/{4}'.format(instance.answerSheet.id,
                                                    year,month,day,filename)

class AnswerDocument(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'ANSWERDOCUMENT'

    document = models.FileField(upload_to=answerSheet_directory_path,null=True, blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='documents')
    answerSheet = models.ForeignKey(AnswerSheet, on_delete=models.CASCADE, related_name='documents',default=None)