from django.db import models
from django.conf import settings


class AcademicUnit(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'ACADEMICUNIT'

    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=75, unique=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
