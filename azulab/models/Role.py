from django.db import models
from django.conf import settings


class Role(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'ROLE'

    name = models.CharField(max_length=21, unique=True)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
