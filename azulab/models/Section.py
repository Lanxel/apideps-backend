from django.db import models
from django.conf import settings
from .Page import Page


class Section(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'SECTION'

    title = models.CharField(max_length=50)
    page = models.ForeignKey(Page, on_delete=models.CASCADE, related_name='sections')
    position = models.IntegerField()
    random = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
