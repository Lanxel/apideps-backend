from django.db import models
from django.conf import settings
from .Question import Question
from .AnswerSheet import AnswerSheet

class Comment(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'COMMENT'

    date = models.DateTimeField()
    content = models.CharField(max_length=140)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='commments')
    answerSheet = models.ForeignKey(AnswerSheet, on_delete=models.CASCADE, related_name='comments',default=None)
    user = models.CharField(max_length=100)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return self.content

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)