from django.db import models
from django.conf import settings
from .Module import Module
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


class Page(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'PAGE'

    number = models.IntegerField()
    module = models.ForeignKey(Module, on_delete=models.CASCADE,related_name='pages')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'Page {self.number}'

    def clean(self):
        if self.number < 1:
            raise ValidationError({'number':_("Número de página debe ser mayor o igual a 1")})

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
