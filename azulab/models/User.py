from django.contrib.auth.models import User
from django.db import models
from .Role import Role
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.conf import settings
import datetime


class UserProfile(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'USER'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    code = models.CharField(max_length=8, unique=True)
    roles = models.ManyToManyField(Role, related_name='users', through='RoleUser')

    def __str__(self):
        return (f'{self.user.last_name} {self.user.first_name}')

    def clean(self):
        if len(self.code) != 8:
            raise ValidationError({'code':_("Código PUCP Inválido (Deben ser 8 caracteres)")})

        if not self.code.isnumeric():
            raise ValidationError({'code':_("Código PUCP Inválido (Solo se aceptan números)")})

        domain = self.user.email.split('@')[1]
        if domain not in ['pucp.edu.pe', 'pucp.pe']:
            raise ValidationError({'email': _("Email no es del dominio PUCP.")})

        if self.user.date_joined is None:
            self.user.date_joined = datetime.date.today()

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)


