from django.db import models
from .Laboratory import Laboratory
from .UserSchedule import UserSchedule
from django.contrib.postgres.fields import ArrayField


class AnswerSheet(models.Model):

    userSchedule = models.ForeignKey(UserSchedule, on_delete=models.CASCADE, related_name='answerSheets')
    laboratory = models.ForeignKey(Laboratory, on_delete=models.CASCADE, related_name='answerSheets')
    grade = models.FloatField(null=True,blank=True,default=0)
    moduleGrade = models.JSONField(null=True,blank=True)
    maxModuleGrade = models.JSONField(null=True,blank=True)
    answers = models.JSONField(null=True,blank=True)
    ratedBy = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=15, null=True, blank=True)
    deleted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)