from django.db import models
from .Schedule import Schedule
from .AcademicUnit import AcademicUnit
from .User import UserProfile
from .Role import Role


class RoleUser(models.Model):

    user = models.ForeignKey(UserProfile, on_delete=models.CASCADE, related_name='roleUsers')
    role = models.ForeignKey(Role, on_delete=models.CASCADE, related_name='roleUsers')
    schedules = models.ManyToManyField(Schedule, related_name='roleUsers', through='UserSchedule')
    academicUnits = models.ManyToManyField(AcademicUnit, related_name='roleUsers')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.role.name}  - {self.user.user.first_name} {self.user.user.last_name}"

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
