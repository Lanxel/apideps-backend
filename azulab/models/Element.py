from django.db import models
from .Section import Section
from django.conf import settings


class Element(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'ELEMENT'
        abstract = True

    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    position = models.IntegerField()
    deleted = models.BooleanField(default=False)


    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
