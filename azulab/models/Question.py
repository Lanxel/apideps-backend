from django.db import models
from django.conf import settings
from .Element import Element
from ckeditor_uploader.fields import RichTextUploadingField


class Question(Element):
    class Meta:
        app_label = 'azulab'
        db_table = 'QUESTION'

    number = models.IntegerField()
    statement = RichTextUploadingField()
    maxScore = models.FloatField()
    scoreObtained = models.FloatField(null=True, blank=True,default=0)
    optional = models.BooleanField(default=False)
    cantDocuments = models.IntegerField(null=True, blank=True,default=1)

    def __str__(self):
        return f'Question #{self.number}'

