from django.db import models
from django.conf import settings
from .Element import Element
import datetime as dt

def user_directory_path(instance, filename):
    now = dt.datetime.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    day = now.strftime("%d")
    return 'lab_{0}/{1}/{2}/{3}/{4}/{5}'.format(instance.section.page.module.laboratory.id,
                                                instance.section.page.module.id,
                                                year,month,day,filename)


class Document(Element):
    class Meta:
        app_label = 'azulab'
        db_table = 'DOCUMENT'
    document = models.FileField(upload_to=user_directory_path, null=True, blank=True)