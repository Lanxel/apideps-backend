from django.db import models
from django.conf import settings

from .Course import Course
from .Term import Term


class Schedule(models.Model):
    class Meta:
        app_label = 'azulab'
        db_table = 'SCHEDULE'

    number = models.CharField(max_length=7)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='schedules')
    term = models.ForeignKey(Term, on_delete=models.CASCADE, related_name='schedules')
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.number} - {self.course.name} - {self.term.term}'

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)
