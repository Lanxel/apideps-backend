from rest_framework.permissions import BasePermission
from azulab.models.RoleUser import RoleUser
from azulab.models.Role import Role
from azulab.models.User import UserProfile
from azulab.models.UserSchedule import UserSchedule
from django.core.exceptions import ObjectDoesNotExist


class IsSuperUser(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        return user.is_superuser

class IsAdminUser(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user.is_anonymous:
            return False
        userProfile = UserProfile.objects.get(user=user)
        role = Role.objects.get(name='Administrador')
        return RoleUser.objects.filter(user=userProfile, role=role,deleted=False).exists()

class IsTeacherUser(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user.is_anonymous:
            return False
        userProfile = UserProfile.objects.get(user=user)
        role = Role.objects.get(name='Docente')
        return RoleUser.objects.filter(user=userProfile,role=role,deleted=False).exists()

class IsStudentUser(BasePermission):

    def has_permission(self, request, view):
        user = request.user
        if user.is_anonymous:
            return False
        userProfile = UserProfile.objects.get(user=user)
        role = Role.objects.get(name='Alumno')
        return RoleUser.objects.filter(user=userProfile, role=role,deleted=False).exists()


class UserBelongsToSchedule(BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_anonymous:
            return False
        try:
            userShedule = UserSchedule.objects.get(schedule_id=obj.id,roleUser__user__user_id=user.id)
        except ObjectDoesNotExist:
            return False
        else:
            return True


class IsOwnerComment(BasePermission):
    message = 'Usted no es el dueño de este comentario'
    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_anonymous:
            return False
        name = obj.user
        return name == user.first_name + ' ' + user.last_name

class UserOwnsAnswerSheet(BasePermission):
    def has_object_permission(self, request, view, obj):
        user = request.user
        if user.is_anonymous:
            return False
        userAS = obj.userSchedule.roleUser.user.user
        return userAS == user

class ModuleIsActive(BasePermission):
    message = 'Modulo aun no esta activo'
    def has_object_permission(self, request, view, obj):
        return obj.active