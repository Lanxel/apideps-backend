from azulab.models.Module import Module
from rest_framework.views import APIView
from azulab.utils import ApiErrorsMixin
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser,IsAdminUser,IsTeacherUser,ModuleIsActive
from rest_framework.authentication import TokenAuthentication
import azulab.Services.ModuleServices as moduleSer
import azulab.Selectors.ModuleSelector as moduleSel
from azulab.API.PageAPI import PageListApi,PageSectionsListApi
from django.utils import timezone
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)

class ModuleListApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        currentModule = serializers.SerializerMethodField()
        class Meta:
            model = Module
            fields = ['id','name','initDate','endDate','active','deleted','laboratory','currentModule']

        def get_currentModule(self,obj):
            min = timezone.now() - timezone.timedelta(days=5)
            max = timezone.now()
            if obj.initDate is not None:
                return min < obj.initDate < max
            return False


class ModuleCreateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser, IsAuthenticated]
    class ModuleCreateInputSerializer(serializers.Serializer):
        name = serializers.CharField(max_length=50)
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)
        labId = serializers.IntegerField()


    @swagger_auto_schema(manual_parameters=[token], request_body=ModuleCreateInputSerializer)
    def post(self, request):
        serializer = self.ModuleCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = moduleSer.module_create(**serializer.validated_data)
        if not isinstance(response, Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = ModuleListApi.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class ModuleReadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Module
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = moduleSel.module_read(id=id)
        if not isinstance(response, Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ModuleUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class ModuleUpdateInputSerializer(serializers.Serializer):
        name = serializers.CharField(required=False)
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)
        active = serializers.BooleanField(required=False, default=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Module
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token],request_body=ModuleUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.ModuleUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = moduleSer.module_update(id=id, **serializer.validated_data)

        if not isinstance(response,Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class ModuleDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Module
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = moduleSer.module_delete(id=id)
        if not isinstance(response, Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class ModulePagesListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        pages = serializers.SerializerMethodField()
        class Meta:
            model = Module
            fields = ['id', 'name', 'initDate', 'endDate', 'pages']

        def get_pages(self, obj):
            serializer = PageSectionsListApi.OutputSerializer(obj.pages.filter(deleted=False).order_by("number"),
                                                              many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idModule):
        response = moduleSel.module_read(id=idModule)
        if not isinstance(response, Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ModuleUpdateDisplayApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class ModuleUpdateDisplayInputSerializer(serializers.Serializer):
        data = serializers.JSONField()

    @swagger_auto_schema(manual_parameters=[token], request_body=ModuleUpdateDisplayInputSerializer)
    def post(self,request):
        data = self.ModuleUpdateDisplayInputSerializer(data=request.data)
        data.is_valid(raise_exception=True)
        response = moduleSer.module_update_display(**data.validated_data)
        if not isinstance(response,Module):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)