from ..models.RoleUser import RoleUser
from ..API.RoleAPI import RoleReadApi
from rest_framework.views import APIView
from rest_framework import serializers
from azulab.utils import ApiErrorsMixin
from ..API.UserAPI import UserListSimpleApi
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class RoleUserListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        user = UserListSimpleApi.OutputSerializer()
        class Meta:
            model = RoleUser
            fields = ['user']

class RoleUserDetailApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = RoleUser
            fields = ['id','role','schedules','academicUnits']
            depth = 2
