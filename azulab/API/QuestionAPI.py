from ..models.Checkbox import Checkbox
from ..models.Text import Text
from ..Services import CheckboxServices as checkboxService
from ..Selectors import CheckboxSelector as checkboxSelector
from ..Services import TextServices as textService
from ..Selectors import TextSelector as textSelector
from ..Selectors import QuestionSelector as questionSelector
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsTeacherUser, IsSuperUser
from rest_framework.authentication import TokenAuthentication
from azulab.utils import ApiErrorsMixin
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from django.core import serializers as ss_1

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search Question", type=openapi.TYPE_STRING)


class QuestionListSectionApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputCheckboxSerializer(serializers.ModelSerializer):
        elementType = serializers.SerializerMethodField('get_elementType')

        class Meta:
            model = Checkbox
            exclude = ['deleted','section']

        def get_elementType(self, obj):
            elementType = "checkbox-question"
            return elementType

    class OutputTextSerializer(serializers.ModelSerializer):
        elementType = serializers.SerializerMethodField('get_elementType')

        class Meta:
            model = Text
            exclude = ['deleted','section']

        def get_elementType(self, obj):
            elementType = "text-question"
            return elementType


    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request,idSection):
        objC, objT = questionSelector.question_list_section(section=idSection)
        serializerC = self.OutputCheckboxSerializer(objC,many=True)
        serializerT = self.OutputTextSerializer(objT, many=True)
        return Response(data=serializerC.data + serializerT.data, status=status.HTTP_200_OK)

class QuestionCreateCheckboxApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class CheckboxCreateInputSerializer(serializers.Serializer):
        #Atributos de Element
        section = serializers.IntegerField()
        position = serializers.IntegerField()

        #Atributos de Question
        number = serializers.IntegerField()
        statement = serializers.CharField()
        maxScore = serializers.FloatField()
        scoreObtained = serializers.FloatField(default=0,required=False)
        optional = serializers.BooleanField(default=False,required=False)
        cantDocuments = serializers.IntegerField(max_value=20,required=False)

        #Atributos  de Chekbox
        options = serializers.JSONField() 
        type = serializers.CharField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Checkbox
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=CheckboxCreateInputSerializer)
    def post(self, request):
        serializer = self.CheckboxCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = checkboxService.checkbox_create(**serializer.validated_data)
        if not isinstance(response,Checkbox):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

class QuestionUpdateCheckboxApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Checkbox
            fields = '__all__'

    class CheckboxUpdateInputSerializer(serializers.Serializer):
        #Atributos de Element
        section = serializers.IntegerField(required=False)
        position = serializers.IntegerField(required=False)

        #Atributos de Question
        number = serializers.IntegerField(required=False)
        statement = serializers.CharField(required=False)
        maxScore = serializers.FloatField(required=False)
        scoreObtained = serializers.FloatField(default=0, required=False)
        optional = serializers.BooleanField(default=False, required=False)
        cantDocuments = serializers.IntegerField(max_value=20, required=False)

        #Atributos  de Checkbox
        options = serializers.JSONField(required=False)
        type = serializers.CharField(required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=CheckboxUpdateInputSerializer)
    def post(self, request,id):
        serializer = self.CheckboxUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = checkboxService.checkbox_update(id=id, **serializer.validated_data)
        if not isinstance(response,Checkbox):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class QuestionDeleteCheckboxApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Checkbox
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = checkboxService.checkbox_delete(id=id)
        if isinstance(response, dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        # serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK, data={'response': response})

class QuestionCreateTextApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class TextCreateInputSerializer(serializers.Serializer):
        #Atributos de Element
        section = serializers.IntegerField()
        position = serializers.IntegerField()

        #Atributos de Question
        number = serializers.IntegerField()
        statement = serializers.CharField()
        maxScore = serializers.FloatField()
        scoreObtained = serializers.FloatField(default=0, required=False)
        optional = serializers.BooleanField(default=False, required=False)
        cantDocuments = serializers.IntegerField(max_value=20, required=False)

        #Atributos  de Text
        answer = serializers.CharField(required=False)
        type = serializers.CharField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Text
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=TextCreateInputSerializer)
    def post(self, request):
        serializer = self.TextCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = textService.text_create(**serializer.validated_data)
        if not isinstance(response,Text):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

class QuestionUpdateTextApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Text
            fields = '__all__'

    class TextUpdateInputSerializer(serializers.Serializer):
        #Atributos de Element
        section = serializers.IntegerField(required=False)
        position = serializers.IntegerField(required=False)

        #Atributos de Question
        number = serializers.IntegerField(required=False)
        statement = serializers.CharField(required=False)
        maxScore = serializers.FloatField(required=False)
        scoreObtained = serializers.FloatField(default=0, required=False)
        optional = serializers.BooleanField(default=False, required=False)
        cantDocuments = serializers.IntegerField(max_value=20, required=False)
        #Atributos  de Text
        answer = serializers.CharField(required=False)
        type = serializers.CharField(required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=TextUpdateInputSerializer)
    def post(self, request,id):
        serializer = self.TextUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = textService.text_update(id=id, **serializer.validated_data)
        if not isinstance(response,Text):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class QuestionDeleteTextApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Text
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = textService.text_delete(id=id)
        if isinstance(response, dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        #serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data={'response':response})
