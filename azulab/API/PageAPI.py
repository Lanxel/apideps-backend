from azulab.models.Page import Page
import azulab.Selectors.PageSelector as pageSel
import azulab.Services.PageServices as pageSer
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.utils import ApiErrorsMixin
from azulab.Permissions import IsSuperUser,IsTeacherUser
from rest_framework.authentication import TokenAuthentication
from azulab.API.SectionAPI import SectionListApi
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)

class PageListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Page
            fields = "__all__"


class PageCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class PageCreateInputSerializer(serializers.Serializer):
        number = serializers.IntegerField()
        moduleId = serializers.IntegerField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Page
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=PageCreateInputSerializer)
    def post(self, request):
        serializer = self.PageCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = pageSer.page_create(**serializer.validated_data)
        if not isinstance(response,Page):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class PageReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Page
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idPage):
        response = pageSel.page_read(id=idPage)
        if not isinstance(response,Page):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class PageUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class PageUpdateInputSerializer(serializers.Serializer):
        number = serializers.IntegerField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Page
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token],request_body=PageUpdateInputSerializer)
    def post(self, request, idPage):
        serializer = self.PageUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = pageSer.page_update(id=idPage, **serializer.validated_data)
        if not isinstance(response,Page):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class PageDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Page
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, idPage):
        response = pageSer.page_delete(id=idPage)
        if not isinstance(response, Page):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class PageSectionsListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        sections = serializers.SerializerMethodField()
        class Meta:
            model = Page
            fields = ['id', 'number','sections']

        def get_sections(self, obj):
            serializer = SectionListApi.OutputSerializer(obj.sections.filter(deleted=False).order_by("position"),
                                                        many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idPage):
        response = pageSel.page_read(id=idPage)
        if not isinstance(response, Page):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
