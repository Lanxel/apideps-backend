from ..models.Role import Role
from ..Services import RoleServices as roleService
from ..Selectors import RoleSelector as roleSelector
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.utils import ApiErrorsMixin
from azulab.Permissions import IsSuperUser,IsAdminUser
from rest_framework.authentication import TokenAuthentication
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)

class RoleListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Role
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request):
        roles = roleSelector.role_list()
        serializer = self.OutputSerializer(roles,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class RoleCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class RoleCreateInputSerializer(serializers.Serializer):
        name = serializers.CharField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Role
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=RoleCreateInputSerializer)
    def post(self, request):
        serializer = self.RoleCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = roleService.role_create(**serializer.validated_data)
        if not isinstance(response,Role):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class RoleReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Role
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = roleSelector.role_read(id=id)
        if not isinstance(response,Role):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class RoleUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class RoleUpdateInputSerializer(serializers.Serializer):
        name = serializers.CharField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Role
            fields = '__all__'



    @swagger_auto_schema(manual_parameters=[token],request_body=RoleUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.RoleUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = roleService.role_update(id=id, **serializer.validated_data)

        if not isinstance(response,Role):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class RoleDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Role
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = roleService.role_delete(id=id)
        if not isinstance(response, Role):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)