from rest_framework.response import Response
from azulab.utils import ApiErrorsMixin
from rest_framework.views import APIView
from rest_framework import serializers
from azulab.Services.GoogleServices import google_login
from rest_framework import status
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework.permissions import AllowAny
from azulab.API.UserAPI import UserTokenDetailApi
from rest_framework.authtoken.models import Token

class GoogleLoginApi(ApiErrorsMixin, APIView):
    authentication_classes = []
    permission_classes = [AllowAny]
    class SocialLoginSerializer(serializers.Serializer):
        auth_token = serializers.CharField()

    @swagger_auto_schema(request_body=SocialLoginSerializer)
    def post(self, request):
        serializer = self.SocialLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = google_login(**serializer.validated_data)
        if not isinstance(response,Token):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = UserTokenDetailApi.OutputSerializer(response)
        return Response(data=serializer.data,status=status.HTTP_202_ACCEPTED)






