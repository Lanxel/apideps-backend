from azulab.models.Laboratory import Laboratory
from azulab.models.Module import Module
from azulab.models.Schedule import Schedule
from azulab.API.ModuleAPI import ModuleListApi,ModulePagesListApi
from azulab.utils import ApiErrorsMixin
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser,IsAdminUser,IsTeacherUser,IsStudentUser,UserBelongsToSchedule
import azulab.Selectors.LaboratorySelector as labSelector
import azulab.Services.LaboratoryServices as labService
from django.utils import timezone
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search", type=openapi.TYPE_STRING)
questions = openapi.Parameter('questions', openapi.IN_QUERY, description="Grades by Question", type=openapi.TYPE_BOOLEAN)

class LaboratoryListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Laboratory
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request):
        labs = labSelector.laboratory_list()
        serializer = self.OutputSerializer(labs, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryCreateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class LaboratoryCreateInputSerializer(serializers.Serializer):
        number = serializers.IntegerField(default=1)
        title = serializers.CharField()
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)
        description = serializers.CharField(required=False)
        url = serializers.URLField(required=False)
        schedule = serializers.IntegerField()
        active = serializers.BooleanField(required=False)
        visible = serializers.BooleanField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Laboratory
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=LaboratoryCreateInputSerializer)
    def post(self, request):
        serializer = self.LaboratoryCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = labService.laboratory_create(**serializer.validated_data)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class LaboratoryReadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        currentLab = serializers.SerializerMethodField()
        class Meta:
            model = Laboratory
            fields = ['id','number','title','description','url','initDate','endDate','currentLab']

        def get_currentLab(self,obj):
            min = timezone.now() - timezone.timedelta(days=5)
            max = timezone.now()
            if obj.initDate is not None:
                return min < obj.initDate < max
            return False


    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = labSelector.laboratory_read(id=id)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Laboratory
            exclude = ['deleted']

    class LaboratoryUpdateInputSerializer(serializers.Serializer):
        number = serializers.IntegerField(default=1,required=False)
        title = serializers.CharField(required=False)
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)
        description = serializers.CharField(required=False)
        schedule = serializers.IntegerField(required=False)
        active = serializers.BooleanField(required=False)
        visible = serializers.BooleanField(required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=LaboratoryUpdateInputSerializer)
    def post(self, request,id):
        serializer = self.LaboratoryUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = labService.laboratory_update(id=id, **serializer.validated_data)
        if not isinstance(response,Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class LaboratoryDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Laboratory
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = labService.laboratory_delete(id=id)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class LaboratoryModulesList(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        modules = serializers.SerializerMethodField()
        class Meta:
            model = Laboratory
            fields = ['id','number','title','description','url','initDate','endDate','modules']

        def get_modules(self,obj):
            serializer = ModuleListApi.OutputSerializer(obj.modules.filter(deleted=False).order_by("initDate"),
                                                        many=True)
            return serializer.data


    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,idLab):
        response = labSelector.laboratory_read(id=idLab)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryDisplayApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        modules = serializers.SerializerMethodField()
        class Meta:
            model = Laboratory
            fields = ['id','number','title','description','url','schedule','modules']

        def get_modules(self,obj):
            serializer = ModulePagesListApi.OutputSerializer(obj.modules.filter(deleted=False).order_by("initDate"),
                                                             many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idLab):
        response = labSelector.laboratory_read(id=idLab)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryTemplateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class LaboratoryTemplateInputSerializer(serializers.Serializer):
        idList = serializers.ListField(child=serializers.IntegerField())

    @swagger_auto_schema(manual_parameters=[token],request_body=LaboratoryTemplateInputSerializer)
    def post(self, request, idLab):
        serializer = self.LaboratoryTemplateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = labService.laboratory_template(idLab=idLab,**serializer.validated_data)
        if not isinstance(response, Laboratory):
            return Response(data=response,status=status.HTTP_400_BAD_REQUEST)
        serializer = LaboratoryModulesList.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class LaboratoryGradeApi(ApiErrorsMixin, APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentUser, IsAuthenticated, UserBelongsToSchedule]

    class OutputSerializer(serializers.ModelSerializer):
        finalGrade = serializers.SerializerMethodField()
        totalGrade = serializers.SerializerMethodField()
        class Meta:
            model = Laboratory
            fields = ['id','number','title','finalGrade','totalGrade']

        def get_totalGrade(self,obj):
            return 20

        def get_finalGrade(self,obj):
            from azulab.Selectors.AnswerSheetSelector import answerSheet_total_grade
            user = self.context['user']
            finalGrade = answerSheet_total_grade(idLab=obj.id, user=user)
            return finalGrade

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idSchedule):
        from azulab.Selectors.ScheduleSelector import schedule_read
        response = schedule_read(id=idSchedule)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        context = {'user':self.request.user}
        self.check_object_permissions(request=self.request,obj=response)
        serializer = self.OutputSerializer(response.laboratories.filter(deleted=False).order_by('number'),
                                           many=True,context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryModuleGradeApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentUser, IsAuthenticated, UserBelongsToSchedule]

    class OutputSerializer(serializers.ModelSerializer):
        finalGrade = serializers.SerializerMethodField()
        totalGrade = serializers.SerializerMethodField()
        class Meta:
            model = Module
            fields = ['id','name','finalGrade','totalGrade']

        def get_finalGrade(self,obj):
            answerSheet = self.context['answerSheet']
            moduleGrade = answerSheet.moduleGrade['moduleGrade']
            maxModuleGrade = answerSheet.maxModuleGrade['maxModuleGrade']
            totalGrade = 0
            for module in maxModuleGrade:
                if module['id'] == obj.id:
                    totalGrade = module['max']
            if totalGrade == 0:
                return 0

            finalGrade = 0
            for module in moduleGrade:
                if module['id'] == obj.id:
                    finalGrade = module['grade']
            if finalGrade is None:
                return 0
            else:
                return finalGrade*20/totalGrade

        def get_totalGrade(self,obj):
            return 20

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idLab):
        from azulab.Selectors.AnswerSheetSelector import answerSheet_get
        response = labSelector.laboratory_read(id=idLab)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        self.check_object_permissions(request=self.request,obj=response.schedule)
        answerSheet = answerSheet_get(labId=idLab, userId=self.request.user.userprofile.id)
        if isinstance(answerSheet,dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        context = {'answerSheet':answerSheet}
        serializer = self.OutputSerializer(response.modules.filter(deleted=False),many=True,context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryLiveApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        currentModule = serializers.SerializerMethodField()
        users = serializers.SerializerMethodField()
        modules = serializers.SerializerMethodField()
        class Meta:
            model = Laboratory
            fields = ['id','number','title','description','initDate','endDate','currentModule','modules','users']

        def get_modules(self,obj):
            serializer = ModuleListApi.OutputSerializer(obj.modules.filter(deleted=False).order_by("initDate"),
                                                        many=True)
            return serializer.data

        def get_currentModule(self,obj):
            module = labSelector.laboratory_current_module(id=obj.id)
            if module is not None:
                serializer = ModuleListApi.OutputSerializer(module)
                self.context['idModule'] = module.id
                return serializer.data

        def get_users(self,obj):
            from azulab.Selectors.AnswerSheetSelector import answerSheet_lab_progress
            if 'idModule' in self.context:
                search = self.context['search']
                data = answerSheet_lab_progress(idLab=obj.id,idModule=self.context['idModule'],search=search)
                return data

    @swagger_auto_schema(manual_parameters=[token,search])
    def get(self, request, idLab):
        response = labSelector.laboratory_read(id=idLab)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        context = {'search':request.GET.get('search')}
        serializer = self.OutputSerializer(response,context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class LaboratoryStatisticsApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsTeacherUser | IsStudentUser, IsAuthenticated,UserBelongsToSchedule]

    @swagger_auto_schema(manual_parameters=[token,questions])
    def get(self, request, idLab,idModule):
        from azulab.Selectors.AnswerSheetSelector import answerSheet_get_all_module_grades,\
            answerSheet_get_all_module_grades_by_question
        response = labSelector.laboratory_read(id=idLab)
        if not isinstance(response, Laboratory):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        self.check_object_permissions(request=self.request, obj=response.schedule)
        questions = self.request.GET.get('questions')
        questions = False if questions.lower() == 'false' else True
        if questions is False:
            response = answerSheet_get_all_module_grades(idLab=idLab,idModule=idModule)
        else:
            response = answerSheet_get_all_module_grades_by_question(idLab=idLab, idModule=idModule)
        return Response(data=response, status=status.HTTP_200_OK)