from ..models.User import UserProfile,User
from ..Services import UserServices as userService
from ..Selectors import UserSelector as userSelector
import azulab.Selectors.RoleUserSelector as roleUserSel
import azulab.API.RoleAPI as roleApi
import azulab.API.AcademicUnitAPI as auApi
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from azulab.utils import ApiErrorsMixin
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser,IsAdminUser
from django.core.validators import FileExtensionValidator
from rest_framework.parsers import MultiPartParser
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


type = openapi.Parameter('type', openapi.IN_QUERY, description="Role of User to Create", type=openapi.TYPE_STRING)
token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Authoritazion Token", type=openapi.TYPE_STRING)
idAu = openapi.Parameter('idAu', openapi.IN_QUERY, description="Academic Unit id to filter", type=openapi.TYPE_INTEGER)
filep = openapi.Parameter('file',openapi.IN_FORM,description='CSV File',type=openapi.TYPE_FILE)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search", type=openapi.TYPE_STRING)

class UserTokenDetailApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Token
            fields = ('key',)


class UserGetExtraDetailApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        auth_token = UserTokenDetailApi.OutputSerializer()
        class Meta:
            model = User
            fields = ('first_name','last_name','email','auth_token')


class UserGetDetailApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = User
            fields = ('first_name','last_name','email',)


class UserListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        academicUnit = serializers.SerializerMethodField()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user','academicUnit')

        def get_academicUnit(self,obj):
            type = self.context['type']
            au = roleUserSel.roleUser_get_academicUnit(user=obj, type=type)
            serializer = auApi.AcademicUnitReadApi.OutputSerializer(au)
            return serializer.data


    @swagger_auto_schema(manual_parameters=[token,search,type,idAu])
    def get(self,request):
        context = {'type':request.GET.get('type')}
        users = userSelector.user_list(search=request.GET.get('search'),
                                       type=request.GET.get('type'),
                                       idAu=request.GET.get('idAu'))
        serializer = self.OutputSerializer(users,context=context, many=True)
        return Response(data=serializer.data,status=status.HTTP_200_OK)


class UserReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user')

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = userSelector.user_read(id=id)
        if not isinstance(response,UserProfile):
            return Response(data=response,status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data,status=status.HTTP_200_OK)


class UserUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser,IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user')

    class UserUpdateInputSerializer(serializers.Serializer):
        code = serializers.CharField(max_length=8, required=False)
        first_name = serializers.CharField(max_length=100, required=False)
        last_name = serializers.CharField(max_length=100, required=False)
        email = serializers.EmailField(required=False)
        roles = serializers.ListField(child=serializers.JSONField(), required=False)

    @swagger_auto_schema(manual_parameters=[token], request_body=UserUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.UserUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = userService.user_update(id=id, **serializer.validated_data)
        if not isinstance(response, UserProfile):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class UserDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser,IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = UserProfile
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = userService.user_delete(id=id)
        if not isinstance(response,UserProfile):
            return Response(data=response,status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class UserCreateOneApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user')

    class UserCreateOneInputSerializer(serializers.Serializer):
        first_name = serializers.CharField(max_length=100)
        last_name = serializers.CharField(max_length=100)
        code = serializers.CharField(max_length=8)
        email = serializers.EmailField()

    @swagger_auto_schema(manual_parameters=[type,token], request_body=UserCreateOneInputSerializer)
    def post(self, request):
        serializer = self.UserCreateOneInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = userService.user_create_one_csv(**serializer.validated_data, type=request.GET.get("type"))
        if not isinstance(response, UserProfile):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class UserCreateFromCSV(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    parser_classes = [MultiPartParser]
    class InputSerializer(serializers.Serializer):
        file = serializers.FileField(validators=[FileExtensionValidator(allowed_extensions=['csv'])])

    @swagger_auto_schema(manual_parameters=[type, token,filep])
    def post(self,request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        errors = userService.user_create_from_csv(file=self.request.FILES['file'], type=request.GET.get("type"))
        if len(errors) == 0:
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(errors)


class UserLanding(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        roles = serializers.SerializerMethodField()
        class Meta:
            model = UserProfile
            fields = ('id','code','user','roles')

        def get_roles(self, obj):
            roles = roleUserSel.roleUser_get_current_roles(userId=obj.id)
            serializer = roleApi.RoleListApi.OutputSerializer(roles, many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request):
        user = self.request.user.userprofile
        serializer = self.OutputSerializer(user)
        return Response(serializer.data)


class UserVerifyApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    class UserVerifyInputSerializer(serializers.Serializer):
        code = serializers.CharField(max_length=8,required=False)
        email = serializers.EmailField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = UserProfile
            fields = ('id',)

    @swagger_auto_schema(manual_parameters=[token], request_body=UserVerifyInputSerializer)
    def post(self,request):
        serializer = self.UserVerifyInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = userSelector.user_verify(**serializer.validated_data)
        if not isinstance(response,UserProfile):
            return Response({'id': None}, status=status.HTTP_200_OK)
        serializer = self.OutputSerializer(response)
        return Response(serializer.data, status=status.HTTP_200_OK)


class UserDeleteMultipleApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class UserDeleteMultipleInputSerializer(serializers.Serializer):
        idlist = serializers.ListField(child=serializers.IntegerField(),required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=UserDeleteMultipleInputSerializer)
    def delete(self, request):
        serializer = self.UserDeleteMultipleInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = userService.user_delete_multiple(**serializer.validated_data)
        if len(response)!=0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class UserCreateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user','roles')

    class UserCreateInputSerializer(serializers.Serializer):
        first_name = serializers.CharField(max_length=100)
        last_name = serializers.CharField(max_length=100)
        code = serializers.CharField(max_length=8)
        email = serializers.EmailField()
        roles = serializers.ListField(child=serializers.JSONField(), required=False,
                                      help_text='[{"userType":idRole,'
                                                '"academicUnit":idAu,'
                                                '"schedule":idSchedule}]')

    @swagger_auto_schema(manual_parameters=[token], request_body=UserCreateInputSerializer)
    def post(self, request):
        serializer = self.UserCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = userService.user_create(**serializer.validated_data)
        if not isinstance(response, UserProfile):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class UserListSimpleApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        user = UserGetDetailApi.OutputSerializer()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'user')


class UserReadRolesApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        first_name = serializers.SerializerMethodField()
        last_name = serializers.SerializerMethodField()
        email= serializers.SerializerMethodField()
        roles = serializers.SerializerMethodField()
        class Meta:
            model = UserProfile
            fields = ('id', 'code', 'first_name', 'last_name','email','roles')

        def get_first_name(self, obj):
            data = obj.user.first_name
            return data

        def get_last_name(self, obj):
            data = obj.user.last_name
            return data

        def get_email(self, obj):
            data = obj.user.email
            return data

        def get_roles(self,obj):
            return userSelector.user_get_roles(user=obj)

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = userSelector.user_read(id=id)
        if not isinstance(response, UserProfile):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)
