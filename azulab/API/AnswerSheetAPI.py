from azulab.models.AnswerSheet import AnswerSheet
from azulab.utils import ApiErrorsMixin
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import UserOwnsAnswerSheet
from azulab.Permissions import IsStudentUser, IsTeacherUser, UserOwnsAnswerSheet,IsSuperUser
import azulab.Services.AnswerSheetService as asSer
import azulab.Selectors.AnswerSheetSelector as asSel
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
idModule = openapi.Parameter('idModule', openapi.IN_QUERY, description="Module ID", type=openapi.TYPE_INTEGER)


class AnswerSheetCreateUpdateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser |IsStudentUser | IsTeacherUser, IsAuthenticated]

    class AnswerSheetCreateUpdateInputSerializer(serializers.Serializer):
        answers = serializers.JSONField(help_text='Mismo JSON que se manda en modules/id/pages/ pero con'
                                                  'las respuestas del alumno')

    @swagger_auto_schema(manual_parameters=[token], request_body=AnswerSheetCreateUpdateInputSerializer)
    def post(self, request):
        user = self.request.user
        serializer = self.AnswerSheetCreateUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = asSer.answerSheet_update(**serializer.validated_data, user=user)
        if not isinstance(response, AnswerSheet):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_201_CREATED)


class AnswerSheetReadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser |IsTeacherUser| IsStudentUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AnswerSheet
            fields = ['id']

    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,idLab,idUser):
        response = asSel.answerSheet_get(labId=idLab,userId=idUser)
        if not isinstance(response,AnswerSheet):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data,status=status.HTTP_200_OK)

class AnswerSheetGetModuleApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser | (IsStudentUser & UserOwnsAnswerSheet), IsAuthenticated]

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idAnswerSheet,idModule):
        answerSheet = asSel.answerSheet_read(id=idAnswerSheet)
        self.check_object_permissions(request=self.request,obj=answerSheet)
        response = asSel.answerSheet_get_module(id=idAnswerSheet,idModule=idModule)
        if response is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        return Response(data=response,status=status.HTTP_200_OK)


class AnswerSheetGradingApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class AnswerSheetGradingInputSerializer(serializers.Serializer):
        answers = serializers.JSONField(help_text='Mismo JSON que se manda en modules/id/pages/ pero con'
                                                  'las respuestas del alumno')

    @swagger_auto_schema(manual_parameters=[token], request_body=AnswerSheetGradingInputSerializer)
    def post(self, request, id):
        user = self.request.user
        serializer = self.AnswerSheetGradingInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = asSer.answerSheet_grading(**serializer.validated_data, user=user,idAS=id)
        if not isinstance(response, AnswerSheet):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class AnswerSheetGradesApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        code = serializers.SerializerMethodField()
        moduleGrades = serializers.SerializerMethodField()
        name = serializers.SerializerMethodField()
        class Meta:
            model = AnswerSheet
            fields = ['id','code','name','moduleGrades','grade','ratedBy']

        def get_code(self,obj):
            user = obj.userSchedule.roleUser.user
            return user.code

        def get_name(self,obj):
            user = obj.userSchedule.roleUser.user.user
            return user.first_name + ' ' + user.last_name

        def get_moduleGrades(self,obj):
            data = []
            for moduleGrade in obj.moduleGrade['moduleGrade']:
                idModule = moduleGrade['id']
                for maxGrade in obj.maxModuleGrade['maxModuleGrade']:
                    if maxGrade['id'] == idModule:
                        data.append({'id':idModule,'grade':str(moduleGrade['grade']) + '/' + str(maxGrade['max'])})
                        break
            return data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idLab):
        response = asSel.answerSheet_get_from_lab(labId=idLab)
        serializer = self.OutputSerializer(response,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class AnswerSheetGradeCBApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsStudentUser, IsAuthenticated, UserOwnsAnswerSheet]

    class AnswerSheetGradeCBInputSerializer(serializers.Serializer):
        idQuestion = serializers.IntegerField()
        options = serializers.ListField(child=serializers.JSONField())

    @swagger_auto_schema(manual_parameters=[token], request_body=AnswerSheetGradeCBInputSerializer)
    def post(self, request, idAnswerSheet):
        answerSheet = asSel.answerSheet_read(id=idAnswerSheet)
        self.check_object_permissions(request=self.request,obj=answerSheet)
        serializer = self.AnswerSheetGradeCBInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response,data = asSer.answerSheet_grading_cb(**serializer.validated_data,idAS=idAnswerSheet)
        if not isinstance(response, AnswerSheet):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(data={'comment':data[0],'triesLeft':data[1],'correct':data[2]},status=status.HTTP_200_OK)