from azulab.models.AnswerDocument import AnswerDocument
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsStudentUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser
import azulab.Services.AnswerDocumentServices as asdocSer
import azulab.Selectors.AnswerDocumentSelector as asdocSel
from azulab.utils import ApiErrorsMixin
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
file_uploaded = openapi.Parameter('file',openapi.IN_FORM,description='File to Upload',type=openapi.TYPE_FILE)

class AnswerDocumentUploadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentUser, IsAuthenticated]
    parser_classes = [MultiPartParser]
    class AnswerDocumentUploadInputSerializer(serializers.Serializer):
        idQuestion = serializers.IntegerField()
        idAnswerSheet = serializers.IntegerField()
        file = serializers.FileField()
        idAsDoc = serializers.IntegerField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        document_name = serializers.SerializerMethodField()
        class Meta:
            model = AnswerDocument
            fields = ['id', 'question', 'answerSheet', 'document_name']

        def get_document_name(self,obj):
            file_name = obj.document.path.split('/')[-1]
            return file_name


    @swagger_auto_schema(manual_parameters=[token, file_uploaded], request_body=AnswerDocumentUploadInputSerializer)
    def post(self, request):
        serializer = self.AnswerDocumentUploadInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = asdocSer.document_upload_replace(**serializer.validated_data)
        if not isinstance(response,AnswerDocument):
            return Response(response,status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class AnswerDocumentDownloadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser]

    class OutputSerializer(serializers.ModelSerializer):
        document_name = serializers.SerializerMethodField()
        elementType = serializers.SerializerMethodField()
        class Meta:
            model = AnswerDocument
            fields = ['id','document_name', 'elementType']

        def get_document_name(self,obj):
            file_name = obj.document.path.split('/')[-1]
            return file_name

        def get_elementType(self,obj):
            elementType = "document"
            return elementType

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idAsDoc):
        response = asdocSel.document_download(id=idAsDoc)
        if isinstance(response, dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return response


class AnswerDocumentDeleteApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsStudentUser, IsAuthenticated]

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self,request,idAsDoc):
        response = asdocSer.document_delete(idDoc=idAsDoc)
        if isinstance(response, dict):
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK,data={'response':response})