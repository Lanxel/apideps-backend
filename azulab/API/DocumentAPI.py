from azulab.models.Document import Document
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsTeacherUser,IsSuperUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser
import azulab.Services.DocumentServices as docSer
import azulab.Selectors.DocumentSelector as docSel
from azulab.utils import ApiErrorsMixin
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
file_uploaded = openapi.Parameter('file',openapi.IN_FORM,description='File to Upload',type=openapi.TYPE_FILE)


class DocumentUploadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    parser_classes = [MultiPartParser]
    class DocumentUploadInputSerializer(serializers.Serializer):
        position = serializers.IntegerField()
        sectionId = serializers.IntegerField()
        file = serializers.FileField()
        idDoc = serializers.IntegerField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        document_name = serializers.SerializerMethodField()
        class Meta:
            model = Document
            fields = ['id', 'position', 'section', 'document_name']

        def get_document_name(self,obj):
            file_name = obj.document.path.split('/')[-1]
            return file_name


    @swagger_auto_schema(manual_parameters=[token, file_uploaded], request_body=DocumentUploadInputSerializer)
    def post(self, request):
        serializer = self.DocumentUploadInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = docSer.document_upload_replace(**serializer.validated_data)
        if not isinstance(response,Document):
            return Response(response,status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class DocumentDownloadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    parser_classes = [MultiPartParser]

    class OutputSerializer(serializers.ModelSerializer):
        document_name = serializers.SerializerMethodField()
        elementType = serializers.SerializerMethodField('get_elementType')
        class Meta:
            model = Document
            fields = ['id','position','document_name', 'elementType']

        def get_document_name(self,obj):
            if obj.document:
                file_name = obj.document.path.split('/')[-1]
                return file_name
            else:
                return ''

        def get_elementType(self, obj):
            elementType = "document"
            return elementType

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idDoc):
        response = docSel.document_download(id=idDoc)
        if isinstance(response, dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return response


class DocumentReplaceFileApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    parser_classes = [MultiPartParser]

    class DocumentReplaceFileInputSerializer(serializers.Serializer):
        file = serializers.FileField()

    @swagger_auto_schema(manual_parameters=[token, file_uploaded], request_body=DocumentReplaceFileInputSerializer)
    def post(self, request,idDoc):
        serializer = self.DocumentReplaceFileInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = docSer.document_replace(idDoc=idDoc,**serializer.validated_data)
        if not isinstance(response, Document):
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_201_CREATED)


class DocumentDeleteApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self,request,idDoc):
        response = docSer.document_delete(idDoc=idDoc)
        if isinstance(response, dict):
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_201_CREATED,data={'response':response})