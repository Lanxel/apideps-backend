from ..models.AcademicUnit import AcademicUnit
from azulab.API.CourseAPI import CourseReadApi
from ..Services import AcademicUnitServices as auService
from ..Selectors import AcademicUnitSelector as auSelector
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser,IsAdminUser
from rest_framework.authentication import TokenAuthentication
from azulab.utils import ApiErrorsMixin
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search by Name", type=openapi.TYPE_STRING)


class AcademicUnitListApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AcademicUnit
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token,search])
    def get(self, request):
        aus = auSelector.academicunit_list(search=request.GET.get('search'))
        serializer = self.OutputSerializer(aus,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class AcademicUnitCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]
    class AcademicUnitCreateInputSerializer(serializers.Serializer):
        code = serializers.CharField()
        name = serializers.CharField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AcademicUnit
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=AcademicUnitCreateInputSerializer)
    def post(self, request):
        serializer = self.AcademicUnitCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = auService.academicunit_create(**serializer.validated_data)
        if not isinstance(response,AcademicUnit):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class AcademicUnitReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AcademicUnit
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = auSelector.academicunit_read(id=id)
        if not isinstance(response,AcademicUnit):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class AcademicUnitUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AcademicUnit
            fields = '__all__'

    class AcademicUnitUpdateInputSerializer(serializers.Serializer):
        code = serializers.CharField(required=False)
        name = serializers.CharField(required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=AcademicUnitUpdateInputSerializer)
    def post(self, request,id):
        serializer = self.AcademicUnitUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = auService.academicunit_update(id=id, **serializer.validated_data)
        if not isinstance(response,AcademicUnit):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class AcademicUnitDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = AcademicUnit
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = auService.academicunit_delete(id=id)
        if not isinstance(response, AcademicUnit):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class AcademicUnitCourseListApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        courses = serializers.SerializerMethodField()
        class Meta:
            model = AcademicUnit
            fields = ['id','code','name','courses']

        def get_courses(self,obj):
            courses = obj.courses.filter(deleted=False).order_by("name")
            serializer = CourseReadApi.OutputSerializer(courses,many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,id):
        response = auSelector.academicunit_read(id=id)
        if not isinstance(response,AcademicUnit):
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)