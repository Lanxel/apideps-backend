from ..models.Schedule import Schedule
from azulab.models.RoleUser import RoleUser
import azulab.Selectors.ScheduleSelector as scheduleSel
import azulab.Services.ScheduleServices as scheduleSer
import azulab.Selectors.RoleUserSelector as roleUserSel
from ..models.Laboratory import Laboratory
from ..Selectors import ScheduleSelector as scheduleSelector
from django.utils import timezone
from azulab.API.RoleUserAPI import RoleUserListApi
from ..API.CourseAPI import CourseBasicDetailApi
from ..API.LaboratoryAPI import LaboratoryReadApi
from azulab.utils import ApiErrorsMixin
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser, IsAdminUser, IsTeacherUser,IsStudentUser, UserBelongsToSchedule
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

course = openapi.Parameter('courseId', openapi.IN_QUERY, description="Course Id", type=openapi.TYPE_STRING)
term = openapi.Parameter('termId', openapi.IN_QUERY, description="Term Id", type=openapi.TYPE_STRING)
token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
typeq = openapi.Parameter('type', openapi.IN_QUERY, description="Type of User", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search", type=openapi.TYPE_STRING)

class ScheduleListCoursesApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        cant = serializers.SerializerMethodField()
        roleUser = serializers.SerializerMethodField()
        class Meta:
            model = Schedule
            fields = ['id','number','cant','roleUser']

        def get_cant(self,obj):
            students = scheduleSel.schedule_count(schedule=obj)
            return students

        def get_roleUser(self,obj):
            roleUsers = scheduleSel.schedule_teacher(schedule=obj)
            if roleUsers is not None:
                serializer = RoleUserListApi.OutputSerializer(roleUsers,many=True)
                return serializer.data

    @swagger_auto_schema(manual_parameters=[token,course,term])
    def get(self,request): 
        try:
            schedules = scheduleSel.schedule_list(codeCourse=request.GET.get('courseId'),
                                                  term=request.GET.get('termId'))
            serializer = self.OutputSerializer(schedules,many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
            
        except Exception:
            return Response(data=schedules, status=status.HTTP_400_BAD_REQUEST)

class ScheduleReadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Schedule
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = scheduleSel.schedule_read(id=id)
        if not isinstance(response,Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ScheduleCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]
    class ScheduleCreateInputSerializer(serializers.Serializer):
        course = serializers.IntegerField()
        number = serializers.CharField()
        term = serializers.IntegerField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Schedule
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=ScheduleCreateInputSerializer)
    def post(self, request):
        serializer = self.ScheduleCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_create(**serializer.validated_data)
        if not isinstance(response,Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

class ScheduleUserDetailApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        course = CourseBasicDetailApi.OutputSerializer()
        class Meta:
            model = Schedule
            fields = ('id','number','course')

class ScheduleUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Schedule
            fields = '__all__'

    class SchUpdateInputSerializer(serializers.Serializer):
        course = serializers.IntegerField()
        number = serializers.CharField()
        term = serializers.IntegerField()

    @swagger_auto_schema(manual_parameters=[token],request_body=SchUpdateInputSerializer)
    def post(self, request,id):

        serializer = self.SchUpdateInputSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_update(id=id, **serializer.validated_data)
        if not isinstance(response,Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class ScheduleDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Schedule
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = scheduleSer.schedule_delete(id=id)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class ScheduleDeleteMultipleApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class ScheduleDeleteMultipleInputSerializer(serializers.Serializer):
        idlist = serializers.ListField(child=serializers.IntegerField(),required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=ScheduleDeleteMultipleInputSerializer)
    def delete(self, request):
        serializer = self.ScheduleDeleteMultipleInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_delete_multiple(**serializer.validated_data)
        if len(response) != 0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class ScheduleListOfUserApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsStudentUser | IsAdminUser | IsTeacherUser,IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        roleUsers = serializers.SerializerMethodField()
        course = CourseBasicDetailApi.OutputSerializer()
        term = serializers.SerializerMethodField()
        class Meta:
            model = Schedule
            fields = ('id','number','course','term','roleUsers')

        def get_roleUsers(self,obj):
            type = self.context['type']
            search = self.context['search']
            roleUsers = roleUserSel.roleuser_list(idSchedule=obj.id,search=search,type=type)
            serializer = RoleUserListApi.OutputSerializer(roleUsers,many=True)
            return serializer.data

        def get_term(self,obj):
            term = obj.term
            return {'id':term.id,'term':term.term}

    @swagger_auto_schema(manual_parameters=[token,typeq,search])
    def get(self,request,id):
        response = scheduleSel.schedule_read(id=id)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        context = {'type':request.GET.get('type'),'search':request.GET.get('search')}
        serializer = self.OutputSerializer(response,context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ScheduleStudentsListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser | IsStudentUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        users = serializers.SerializerMethodField()
        course = CourseBasicDetailApi.OutputSerializer()
        class Meta:
            model = Schedule
            fields = ['id','number','course','term','users']
            depth = 1

        def get_users(self,obj):
            type = self.context['type']
            users = scheduleSel.schedule_users(idSchedule=obj.id,type=type)
            serializer = RoleUserListApi.OutputSerializer(users,many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token,typeq])
    def get(self,request,id):
        response = scheduleSel.schedule_read(id=id)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        context = {'type': request.GET.get('type')}
        serializer = self.OutputSerializer(response, context=context)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ScheduleLabListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser | IsStudentUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        laboratories = serializers.SerializerMethodField()
        course = CourseBasicDetailApi.OutputSerializer()
        class Meta:
            model = Schedule
            fields = ['id','number','course','term','laboratories']
            depth = 1

        def get_laboratories(self,obj):
            labs = obj.laboratories.filter(deleted=False).order_by("number")
            serializer = LaboratoryReadApi.OutputSerializer(labs,many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,id):
        response = scheduleSel.schedule_read(id=id)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class ScheduleListNextLabApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Laboratory
            fields = ['initDate']

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        schedule = scheduleSelector.schedule_read(id=id)
        if not isinstance(schedule, Schedule):
            return Response(data=schedule, status=status.HTTP_400_BAD_REQUEST)
        labs = schedule.laboratories.all()
        currentDate = timezone.now()
        nextLab = None
        for lab in labs:
            if lab.initDate is not None:
                if lab.initDate > currentDate:
                    nextLab = lab
        if not isinstance(nextLab, Laboratory):
            return Response(data={"error":"There is not a next laboratory"}, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(nextLab)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ScheduleDeleteUsersApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class ScheduleDeleteUsersInputSerializer(serializers.Serializer):
        idList = serializers.ListField(child=serializers.IntegerField(),required=False,
                                       help_text='Lista de ids de los alumnos a eliminar')

    @swagger_auto_schema(manual_parameters=[token], request_body=ScheduleDeleteUsersInputSerializer)
    def delete(self, request,id):
        serializer = self.ScheduleDeleteUsersInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_delete_multiple_users(idSchedule=id,**serializer.validated_data)
        if len(response) != 0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

class ScheduleTeacherApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        teacher = serializers.SerializerMethodField()
        class Meta:
            model = Schedule
            fields = ['id','number','teacher']

        def get_teacher(self,obj):
            roleUser = scheduleSel.schedule_teacher(schedule=obj)
            if isinstance(roleUser, list):
                serializer = RoleUserListApi.OutputSerializer(roleUser,many=True)
                return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = scheduleSel.schedule_read(id=id)
        if not isinstance(response, Schedule):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class ScheduleUpdateTeacherApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser, IsAuthenticated]
    class ScheduleUpdateTeacherInputSerializer(serializers.Serializer):
        idList = serializers.ListField(child=serializers.IntegerField(),required=False,
                                       help_text='Lista de ids de los profesores')

    @swagger_auto_schema(manual_parameters=[token], request_body=ScheduleUpdateTeacherInputSerializer)
    def post(self, request, id):
        serializer = self.ScheduleUpdateTeacherInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_update_teacher(idSchedule=id, **serializer.validated_data)
        if isinstance(response, dict) or len(response) > 0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class ScheduleAddStudentApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser, IsAuthenticated]
    class ScheduleAddStudentInputSerializer(serializers.Serializer):
        idList = serializers.ListField(child=serializers.IntegerField(),required=False,
                                       help_text='Lista de ids de los alumnos')

    @swagger_auto_schema(manual_parameters=[token], request_body=ScheduleAddStudentInputSerializer)
    def post(self, request, id):
        serializer = self.ScheduleAddStudentInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = scheduleSer.schedule_add_student(idSchedule=id, **serializer.validated_data)
        if isinstance(response, dict) or len(response) > 0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)