from azulab.models.Section import Section
from azulab.models.RichTextBox import RichTextBox
from azulab.models.Text import Text
from azulab.models.Checkbox import Checkbox
from azulab.API.RichTextBoxAPI import RTBReadApi
from azulab.API.DocumentAPI import DocumentDownloadApi
from azulab.API.QuestionAPI import QuestionListSectionApi
from azulab.models.Document import Document
import azulab.Selectors.SectionSelector as sectionSel
import azulab.Services.SectionServices as sectionSer
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.utils import ApiErrorsMixin
from azulab.Permissions import IsSuperUser,IsTeacherUser
from rest_framework.authentication import TokenAuthentication
from azulab.Selectors.ElementSelector import element_list_section
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)

class SectionListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        elements = serializers.SerializerMethodField()
        class Meta:
            model = Section
            fields = ['id','title','position','elements','random']

        def get_elements(self,obj):
            elements = element_list_section(sectionId=obj.id)
            data = []
            for item in elements:
                if isinstance(item,RichTextBox):
                    serializer = RTBReadApi.OutputSerializer(item)
                    data.append(serializer.data)
                if isinstance(item,Document):
                    serializer = DocumentDownloadApi.OutputSerializer(item)
                    data.append(serializer.data)
                if isinstance(item,Text):
                    serializer = QuestionListSectionApi.OutputTextSerializer(item)
                    data.append(serializer.data)
                if isinstance(item,Checkbox):
                    serializer = QuestionListSectionApi.OutputCheckboxSerializer(item)
                    data.append(serializer.data)
            return data



class SectionCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class SectionCreateInputSerializer(serializers.Serializer):
        title = serializers.CharField(max_length=50)
        position = serializers.IntegerField()
        pageId = serializers.IntegerField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Section
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=SectionCreateInputSerializer)
    def post(self, request):
        serializer = self.SectionCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = sectionSer.section_create(**serializer.validated_data)
        if not isinstance(response,Section):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class SectionReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Section
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idSection):
        response = sectionSel.section_read(id=idSection)
        if not isinstance(response,Section):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class SectionUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class SectionUpdateInputSerializer(serializers.Serializer):
        title = serializers.CharField(max_length=50,required=False)
        position = serializers.IntegerField()
        pageId = serializers.IntegerField()


    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Section
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token],request_body=SectionUpdateInputSerializer)
    def post(self, request, idSection):
        serializer = self.SectionUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = sectionSer.section_update(id=idSection, **serializer.validated_data)

        if not isinstance(response,Section):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)


class SectionDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Section
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, idSection):
        response = sectionSer.section_delete(id=idSection)
        if not isinstance(response, Section):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

