from ..models.Comment import Comment
from ..Services import CommentServices as commentService
from ..Selectors import CommentSelector as commentSelector
from rest_framework.views import APIView
from azulab.utils import ApiErrorsMixin
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsTeacherUser, IsStudentUser, IsOwnerComment,IsSuperUser
from rest_framework.authentication import TokenAuthentication
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)



class CommentLisQuestiontApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAuthenticated]

    class CommentListInputSerializer(serializers.Serializer):
        idList = serializers.ListField(child=serializers.IntegerField())
        idAnswerSheet = serializers.IntegerField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Comment
            fields = ['id','question','content','date','user']


    @swagger_auto_schema(manual_parameters=[token], request_body=CommentListInputSerializer)
    def post(self, request):
        serializer = self.CommentListInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        comments = commentSelector.comment_list_question(**serializer.validated_data)
        serializer = self.OutputSerializer(comments,many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CommentCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser | IsStudentUser, IsAuthenticated]
    class CommentCreateInputSerializer(serializers.Serializer):
        content = serializers.CharField()
        question = serializers.IntegerField()
        idAnswerSheet = serializers.IntegerField()
        
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Comment
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=CommentCreateInputSerializer)
    def post(self, request):
        user = request.user
        serializer = self.CommentCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = commentService.comment_create(**serializer.validated_data,user=user)
        if not isinstance(response, Comment):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class CommentUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAuthenticated,IsOwnerComment]
    class CommentUpdateInputSerializer(serializers.Serializer):
        content = serializers.CharField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Comment
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=CommentUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.CommentUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        comment = commentSelector.comment_read(id=id)
        self.check_object_permissions(request=self.request,obj=comment)
        response = commentService.comment_update(id=id, **serializer.validated_data)
        if not isinstance(response, Comment):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class CommentDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAuthenticated,IsOwnerComment]

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        comment = commentSelector.comment_read(id=id)
        if isinstance(comment,Comment):
            self.check_object_permissions(request=self.request, obj=comment)
        response = commentService.comment_delete(id=id)
        if not isinstance(response, Comment):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

