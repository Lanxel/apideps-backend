from azulab.models.RichTextBox import RichTextBox
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsTeacherUser,IsSuperUser
from rest_framework.authentication import TokenAuthentication
from azulab.utils import ApiErrorsMixin
import azulab.Services.RichTextBoxServices as rtbSer
import azulab.Selectors.RichTextBoxSelector as rtbSel
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)

class RTBCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class RTBCreateInputSerializer(serializers.Serializer):
        position = serializers.IntegerField()
        sectionId = serializers.IntegerField()
        content = serializers.CharField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = RichTextBox
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=RTBCreateInputSerializer)
    def post(self, request):
        serializer = self.RTBCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = rtbSer.rtb_create(**serializer.validated_data)
        if not isinstance(response, RichTextBox):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)

class RTBReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        elementType = serializers.SerializerMethodField('get_elementType')
        class Meta:
            model = RichTextBox
            fields = ['id','position','content', 'elementType']

        def get_elementType(self, obj):
            elementType = "text"
            return elementType
    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idRTB):
        response = rtbSel.rtb_read(id=idRTB)
        if not isinstance(response,RichTextBox):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class RTBUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser,IsAuthenticated]

    class RTBUpdateInputSerializer(serializers.Serializer):
        number = serializers.IntegerField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = RichTextBox
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token],request_body=RTBUpdateInputSerializer)
    def post(self, request, idRTB):
        serializer = self.PageUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = rtbSer.rtb_update(id=idRTB, **serializer.validated_data)
        if not isinstance(response,RichTextBox):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data=serializer.data)

class RTBDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsTeacherUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = RichTextBox
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, idRTB):
        response = rtbSer.rtb_delete(id=idRTB)
        if isinstance(response,dict):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        #serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK,data={'response': response})
