from ..models.Course import Course
from azulab.models.Term import Term
from ..Services import CourseServices as courseService
from ..Selectors import CourseSelector as courseSelector
from azulab.Selectors.ScheduleSelector import schedule_terms,schedule_list
from ..Selectors import TermSelector as termSel
from rest_framework.views import APIView
from azulab.utils import ApiErrorsMixin
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser,IsAdminUser,IsTeacherUser,IsStudentUser
from rest_framework.authentication import TokenAuthentication
from rest_framework.parsers import MultiPartParser
from django.core.validators import FileExtensionValidator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search by name", type=openapi.TYPE_STRING)
filep = openapi.Parameter('file',openapi.IN_FORM,description='Excel File',type=openapi.TYPE_FILE)
idAu = openapi.Parameter('idAu', openapi.IN_QUERY, description="Academic Unit id to filter", type=openapi.TYPE_INTEGER)
idTerm = openapi.Parameter('idTerm', openapi.IN_QUERY, description="Term id to filter", type=openapi.TYPE_INTEGER)


class CourseListApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):

        idSchedule = serializers.SerializerMethodField()
        academicUnit = serializers.SerializerMethodField()
        class Meta:
            model = Course
            fields = ['id','code','name','credits','academicUnit','idSchedule']


        def get_idSchedule(self,obj):
            schedule, _ = schedule_terms(idCourse=obj.id)
            if schedule is not None:
                data = schedule.id
                return data
            else:
                return None

        def get_academicUnit(self,obj):
            return {'id':obj.academicUnit.id, 'name':obj.academicUnit.name}

    @swagger_auto_schema(manual_parameters=[token, search,idTerm,idAu])
    def get(self, request):
        courses = courseSelector.course_list(search=request.GET.get('search'),
                                             idTerm=request.GET.get('idTerm'),
                                             idAu=request.GET.get('idAu'))
        if isinstance(courses,dict):
            return Response(data=courses, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(courses, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class CourseCreateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    class CourseCreateInputSerializer(serializers.Serializer):
        code = serializers.CharField()
        name = serializers.CharField()
        credits = serializers.FloatField(required=False)
        academicUnit = serializers.IntegerField()
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Course
            fields = ('id','code','name','credits')

    @swagger_auto_schema(manual_parameters=[token], request_body=CourseCreateInputSerializer)
    def post(self, request):
        serializer = self.CourseCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = courseService.course_create(**serializer.validated_data)
        if not isinstance(response, Course):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data = serializer.data)


class CourseReadApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Course
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = courseSelector.course_read(id=id)
        if not isinstance(response, Course):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

class CourseUpdateApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser,IsAuthenticated]
    class CourseUpdateInputSerializer(serializers.Serializer):
        code = serializers.CharField(required=False)
        name = serializers.CharField(required=False)
        credits = serializers.FloatField(required=False)
        academicunit = serializers.IntegerField()

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Course
            fields = ('id','code','name','credits')

    @swagger_auto_schema(manual_parameters=[token], request_body=CourseUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.CourseUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = courseService.course_update(id=id, **serializer.validated_data)
        if not isinstance(response, Course):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

class CourseDeleteApi(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Course
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = courseService.course_delete(id=id)
        if not isinstance(response, Course):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

class CourseBasicDetailApi(ApiErrorsMixin, APIView):
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Course
            fields = ('id','code', 'name',)


class CourseAddStudentsFromCsvApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser, IsAuthenticated]
    parser_classes = [MultiPartParser]
    class CourseStudentsCSVInputSerializer(serializers.Serializer):
        file = serializers.FileField(validators=[FileExtensionValidator(allowed_extensions=['xls','xlsx'])])

    @swagger_auto_schema(manual_parameters=[token, filep])
    def post(self,request,idCourse, idTerm):
        serializer = self.CourseStudentsCSVInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = self.request.FILES['file']
        response = courseService.course_add_from_csv(courseId=idCourse,termId=idTerm,file=file)
        if isinstance(response, dict) or len(response) != 0 :
            return Response(response,status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_201_CREATED)

class CourseListOfTerms(ApiErrorsMixin,APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = ['id','term']

    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,id):
        terms = termSel.term_courses_terms(idCourse=id)
        serializer = self.OutputSerializer(terms,many=True)
        return Response(data=serializer.data,status=status.HTTP_200_OK)

class CourseDeleteMultipleApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class CourseDeleteMultipleInputSerializer(serializers.Serializer):
        idlist = serializers.ListField(child=serializers.IntegerField(),required=False)

    @swagger_auto_schema(manual_parameters=[token],request_body=CourseDeleteMultipleInputSerializer)
    def delete(self, request):
        serializer = self.CourseDeleteMultipleInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = courseService.course_delete_multiple(**serializer.validated_data)
        if len(response) != 0:
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)


class CourseAdminViewApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser | IsAdminUser | IsTeacherUser | IsStudentUser, IsAuthenticated]
    class OutputSerializer(serializers.ModelSerializer):

        currentSchedule = serializers.SerializerMethodField()
        currentTerm = serializers.SerializerMethodField()
        terms = serializers.SerializerMethodField()
        class Meta:
            model = Course
            fields = ['id','name','code','currentSchedule','currentTerm','terms']

        def get_currentTerm(self,obj):
            _, term = schedule_terms(idCourse=obj.id)
            if term is not None:
                data = {'id': term.id, 'term': term.term}
                return data
            else:
                return {'error':'This Course does not have schedules'}

        def get_currentSchedule(self,obj):
            schedule, _ = schedule_terms(idCourse=obj.id)
            if schedule is not None:
                data = {'id': schedule.id, 'number': schedule.number}
                return data
            else:
                return {'error':'This Course does not have schedules'}

        def get_terms(self,obj):
            return termSel.term_schedules(idCourse=obj.id)



    @swagger_auto_schema(manual_parameters=[token])
    def get(self,request,id):
        response = courseSelector.course_read(id=id)
        if not isinstance(response, Course):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)