from ..models.Term import Term
from ..Services import TermServices as termSer
from ..Selectors import TermSelector as termSel
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework import status
from azulab.utils import ApiErrorsMixin
from . import ScheduleAPI as scheduleApi
from azulab.models.Role import Role
from ..Selectors.ScheduleSelector import schedule_list_by_user
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from azulab.API.ScheduleAPI import ScheduleUserDetailApi
from rest_framework.permissions import IsAuthenticated
from azulab.Permissions import IsSuperUser, IsAdminUser, IsStudentUser, IsTeacherUser
from rest_framework.authentication import TokenAuthentication
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

token = openapi.Parameter('Authorization', openapi.IN_HEADER, description="Token", type=openapi.TYPE_STRING)
search = openapi.Parameter('search', openapi.IN_QUERY, description="Search Filter", type=openapi.TYPE_STRING)
typeq = openapi.Parameter('type', openapi.IN_QUERY, description="Type of User", type=openapi.TYPE_STRING)


class TermListApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token, search])
    def get(self, request):
        terms = termSel.term_list(search=request.GET.get('search'))
        serializer = self.OutputSerializer(terms, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class TermCreateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class TermCreateInputSerializer(serializers.Serializer):
        term = serializers.CharField()
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = "__all__"

    @swagger_auto_schema(manual_parameters=[token], request_body=TermCreateInputSerializer)
    def post(self, request):
        serializer = self.TermCreateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = termSer.term_create(**serializer.validated_data)
        if not isinstance(response, Term):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class TermReadApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, id):
        response = termSel.term_read(id=id)
        if not isinstance(response, Term):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class TermUpdateApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class TermUpdateInputSerializer(serializers.Serializer):
        term = serializers.CharField(required=False)
        initDate = serializers.DateTimeField(required=False)
        endDate = serializers.DateTimeField(required=False)

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token], request_body=TermUpdateInputSerializer)
    def post(self, request, id):
        serializer = self.TermUpdateInputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = termSer.term_update(id=id, **serializer.validated_data)
        if not isinstance(response, Term):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)


class TermDeleteApi(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsSuperUser, IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        class Meta:
            model = Term
            fields = '__all__'

    @swagger_auto_schema(manual_parameters=[token])
    def delete(self, request, id):
        response = termSer.term_delete(id=id)
        if not isinstance(response, Term):
            return Response(data=response, status=status.HTTP_400_BAD_REQUEST)
        serializer = self.OutputSerializer(response)
        return Response(status=status.HTTP_200_OK, data=serializer.data)


class TermCoursesListOfStudent(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        schedules = serializers.SerializerMethodField()  # por default busca get_<nombre de variable>

        class Meta:
            model = Term
            fields = ('term', 'schedules')

        def get_schedules(self, obj):
            request = self.context['request']
            type = self.context['type']
            user = request.user.userprofile
            schedules = schedule_list_by_user(user=user, term=obj, type=type)
            if "error" in schedules:
                return []
            serializer = ScheduleUserDetailApi.OutputSerializer(schedules, many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token, typeq])
    def get(self, request):
        terms = termSel.term_list()
        context = {'request': self.request, 'type': request.GET.get("type")}
        serializer = self.OutputSerializer(terms, context=context, many=True)
        return Response(serializer.data)


class TermCurrentCoursesListOfStudent(ApiErrorsMixin, APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    class OutputSerializer(serializers.ModelSerializer):
        schedules = serializers.SerializerMethodField()

        class Meta:
            model = Term
            fields = ('term', 'schedules')

        def get_schedules(self, obj):
            type = self.context['type']
            user = self.context['user']
            schedules = schedule_list_by_user(user=user, term=obj, type=type)
            if "error" in schedules:
                return []
            serializer = ScheduleUserDetailApi.OutputSerializer(schedules, many=True)
            return serializer.data

    @swagger_auto_schema(manual_parameters=[token])
    def get(self, request, idRole):
        user = self.request.user.userprofile
        terms = termSel.term_current()
        tipo = Role.objects.get(pk=idRole).name
        context = {'user': user, 'type': tipo}
        serializer = self.OutputSerializer(terms, context=context)
        return Response(serializer.data)
